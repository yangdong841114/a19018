﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

public class CustomerConfig
{
    static CustomerConfig()
    {
        //易宝支付提供的测试账号
        //网银2015收银台
        //商户密钥
        //merchantAccount = "10012442782";
        //merchantKey = "mP42238826nuW64r7yh26DGK34o2L2m81L25RG32lD7Lo1058A7iJ28at6QS";


        //2013网关
       // merchantAccount = "10000457067";//
       //  merchantKey = "U26po59182dV8d7654bo24o5z369408u4sQ3To9j6QuopAbo3gwj4h33mro4";

       // merchantAccount = "10014993374";
       // merchantKey = "4n74775r78PoP3jKN4GL1rH442b34434WUALJdlaDyP8T94XTqo2n177Ck7e";

        merchantAccount = ConfigurationManager.AppSettings["Account"]; //"10014993374";
        merchantKey = ConfigurationManager.AppSettings["MerCert"]; //"4n74775r78PoP3jKN4GL1rH442b34434WUALJdlaDyP8T94XTqo2n177Ck7e";

        //移动收银台

         mAccount = "10014993374";
         //商户私钥
         merchantPrivateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAJlMCoXLj3xGF0tPd7CxL5GSl9gpJAOlfg9xsXvS3JVqw7ws4/YoI9rSC3zm4q6kifcSq966ropiYZHtGrWDda/aFfxIAZ8/S3y+/v7k3E+9Xkr/IGaXeaAlKebQ92o+vT065mfdZSFYZ1afFpjT7qNtUBticQcFmR9SGURMyLn5AgMBAAECgYAr7QEa44Bi8Mad+tBmHe9ufB3sf6VlMheUH1TnET3JxsT9d7N+hnP3sLY//Py5ZEZwOLZs5Ps8hgJFSTadCmU/IFAn/ubIdPIIr5tRcRRazd2rxbErcbDEiUjw9azr8b/8QBU2Cz/RLMmeCdxZ69+SZRa89HLojw3YAyKRAAJOMQJBAP6+rg/14Rn1/veeoDFPi+PHlnPv9reH9MdEWr7iacTfrOxwZxuqJ8LQHQhq4cLGrojAQXzG15+d0a1cLXyXi+UCQQCaDWaiLQqc4dAmVrC8HRrZE+whjb92NZwEIqu63VG+CpPPHaaPPQjRI6O1h2NcqIvD+qHIxCsJW9GXIDGXwByFAkAkkbrrD6EA2Q+A/ISCj6jFSiGPAyOntRyufmGIhXgpivKH/prOnd6XWMjSLIW8MEld2DuObf0GLOGswU7GU/sBAkEAgmLnREpVGjVyBJEJIx6LM8JN8JRVwcpcE4/4w79AyVWQhxN0fvXMuZ6tmB5QNgOd0DEYr6GBP3mMapnf/62lNQJAGcFXcowgKuqqflQriSxiITHMYAZWKAuqlK21l5Gx9lCq8kEgSz/JHTkBZImj2h2V5U8+VrvZ3QWA6ZhGsyl3Og==";
         //商户公钥
         merchantPublickey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCZTAqFy498RhdLT3ewsS+RkpfYKSQDpX4PcbF70tyVasO8LOP2KCPa0gt85uKupIn3Eqveuq6KYmGR7Rq1g3Wv2hX8SAGfP0t8vv7+5NxPvV5K/yBml3mgJSnm0PdqPr09OuZn3WUhWGdWnxaY0+6jbVAbYnEHBZkfUhlETMi5+QIDAQAB";
         //易宝公钥
         yeepayPublicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC+ipjQi+MGdhLgio/1JT1AIqmIMZP+hWtpqJDcCtrVQxnM+dixD/gEq0Me5ICsvL7auN99aRvechbWvF3supiM+2OM0CMQYvxi29bsig+dUAVO1vJFu4jYNaxjrX57ih15lWxOTTBjxSM5C7/x/Dy2X9nIZr9jDZIkTWTAcPXCnwIDAQAB";


    }
    /// <summary>
    /// 商户编号
    /// </summary>
    public static string merchantAccount { get; set; }
    /// <summary>
    /// 商户密钥
    /// </summary>
    public static string merchantKey { get; private set; }

    public static string mAccount { get; set; }

    /// <summary>
    /// 商户私钥
    /// </summary>
    public static string merchantPrivateKey { get; private set; }
    /// <summary>
    /// 商户公钥
    /// </summary>
    public static string merchantPublickey { get; private set; }
    /// <summary>
    /// 易宝公钥
    /// </summary>
    public static string yeepayPublicKey { get; private set; }
}
