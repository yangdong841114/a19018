﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Net;
using System.IO;

namespace Library
{
    public class Netease
    {
        private static string appKey = "0d403fd697e73532495b9354fb647e1d"; //0d403fd697e73532495b9354fb647e1d
        private static string appSecret = "246c8263e48d";  //246c8263e48d
        private static string nonce = "12345";


        /// <summary>
        /// 设置回调用的SignKey
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        public static CodeJson SetSignKey(string signKey)
        {
            String url = "https://vcloud.163.com/app/callback/setSignKey";

            string jsondata = "{\"signKey\":\"" + signKey + "\"}";

            TimeSpan ts = DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1);
            string curTime = System.Convert.ToInt32(ts.TotalSeconds).ToString();
            string checkSum = getCheckSum(appSecret, nonce, curTime);
            IDictionary<object, String> headers = new Dictionary<object, String>();
            headers["AppKey"] = appKey;
            headers["Nonce"] = nonce;
            headers["CurTime"] = curTime;
            headers["CheckSum"] = checkSum;
            headers["ContentType"] = "application/json;charset=utf-8";
            string result = HttpPost(url, jsondata, headers, "application/json;charset=utf-8");
            return Newtonsoft.Json.JsonConvert.DeserializeObject<CodeJson>(result);

        }

        /// <summary>
        /// 设置直播间状态回调地址
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        public static CodeJson SetLiveRoomStatusCallBack(string callback)
        {
            String url = "https://vcloud.163.com/app/chstatus/setcallback";

            string jsondata = "{\"chStatusClk\":\"" + callback + "\"}";

            TimeSpan ts = DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1);
            string curTime = System.Convert.ToInt32(ts.TotalSeconds).ToString();
            string checkSum = getCheckSum(appSecret, nonce, curTime);
            IDictionary<object, String> headers = new Dictionary<object, String>();
            headers["AppKey"] = appKey;
            headers["Nonce"] = nonce;
            headers["CurTime"] = curTime;
            headers["CheckSum"] = checkSum;
            headers["ContentType"] = "application/json;charset=utf-8";
            string result = HttpPost(url, jsondata, headers, "application/json;charset=utf-8");
            return Newtonsoft.Json.JsonConvert.DeserializeObject<CodeJson>(result);

        }
       
        /// <summary>
        /// 创建 云信ID
        /// curl -X POST -H "AppKey: go9dnk49bkd9jd9vmel1kglw0803mgq3" -H "Nonce: 4tgggergigwow323t23t" -H "CurTime: 1443592222" -H "CheckSum: 9e9db3b6c9abb2e1962cf3e6f7316fcc55583f86" -H "Content-Type: application/x-www-form-urlencoded" -d 'accid=zhangsan&name=zhangsan' 'https://api.netease.im/nimserver/user/create.action'
        /// </summary>
        /// <param name="accid"></param>
        /// <param name="username"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public static Account_res user_create(string accid,string username, string token)
        {
            String url = "https://api.netease.im/nimserver/user/create.action";
            url += "?accid=" + accid + "&name=" + username+"&token=" + token;
            //TO开发者：accid填要创建的id

            TimeSpan ts = DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1);
            string curTime = System.Convert.ToInt32(ts.TotalSeconds).ToString();
            string checkSum = getCheckSum(appSecret,nonce,curTime);
            IDictionary<object, String> headers = new Dictionary<object, String>();
            headers["AppKey"] = appKey;
            headers["Nonce"] = nonce;
            headers["CurTime"] = curTime;
            headers["CheckSum"] = checkSum;
            headers["ContentType"] = "application/x-www-form-urlencoded;charset=utf-8";
            string json = HttpPost(url, null, headers);
            Account_res jo = Newtonsoft.Json.JsonConvert.DeserializeObject<Account_res>(json);

            return jo;
        }

        /// <summary>
        /// 更新网易云通信密码
        /// </summary>
        /// <param name="accid">本地帐号</param>
        /// <param name="token">新密码</param>
        /// <returns></returns>
        public static CodeJson UpdateUserToken(string accid, string token)
        {
            var url = "https://api.netease.im/nimserver/user/update.action";
            url += "?accid=" + accid + "&token=" + token;//
            var headers = CreateChannelPostHeaders();
            var result = HttpPost(url, null, headers, "application/json;charset=utf-8");
            return Newtonsoft.Json.JsonConvert.DeserializeObject<CodeJson>(result);
        }

        public static string upload(string filebase64string)
        {
            String url = "https://api.netease.im/nimserver/msg/upload.action";
            string data = "content=" + filebase64string;

            TimeSpan ts = DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1);
            string curTime = System.Convert.ToInt32(ts.TotalSeconds).ToString();
            string checkSum = getCheckSum(appSecret, nonce, curTime);
            IDictionary<object, String> headers = new Dictionary<object, String>();
            headers["AppKey"] = appKey;
            headers["Nonce"] = nonce;
            headers["CurTime"] = curTime;
            headers["CheckSum"] = checkSum;
            headers["ContentType"] = "application/x-www-form-urlencoded;charset=utf-8";
            string json = HttpPost(url, data, headers);
            return json;

        }

        /// <summary>
        /// 创建 聊天室
        /// curl -X POST -H "CheckSum: fc040248923c881f2fe7cc39602b79565230155c" -H "AppKey: f1234540c12345673123456847aaaaaa" -H "Nonce: 1" -H "CurTime: 1451217360" -H "Content-Type: application/x-www-form-urlencoded" -d 'name=mychatroom&announcement=&broadcasturl=xxxxxx&creator=zhangsan' 'https://api.netease.im/nimserver/chatroom/create.action'
        /// </summary>
        /// <param name="creator">聊天室属主的账号accid</param>
        /// <param name="name">聊天室名称，长度限制128个字符</param>
        /// <param name="broadcasturl">直播地址，长度限制1024个字符</param>
        /// <returns></returns>
        public static Chatroom_res chatroom_create(string creator, string name, string broadcasturl)
        {
            String url = "https://api.netease.im/nimserver/chatroom/create.action";
            url += "?name=" + name + "&announcement=&broadcasturl=" + broadcasturl + "&creator=" + creator;

            TimeSpan ts = DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1);
            string curTime = System.Convert.ToInt32(ts.TotalSeconds).ToString();
            string checkSum = getCheckSum(appSecret, nonce, curTime);
            IDictionary<object, String> headers = new Dictionary<object, String>();
            headers["AppKey"] = appKey;
            headers["Nonce"] = nonce;
            headers["CurTime"] = curTime;
            headers["CheckSum"] = checkSum;
            headers["ContentType"] = "application/x-www-form-urlencoded;charset=utf-8";
            string json = HttpPost(url, null, headers);

            Chatroom_res jo = new Chatroom_res();
            jo.chatroom = new Chatroom_Info_res();
            jo = Newtonsoft.Json.JsonConvert.DeserializeObject<Chatroom_res>(json);

            return jo;

        }

        public static string getChatroomAddr(int roomid, string accid, int clienttype = 1)
        {
            String url = "https://api.netease.im/nimserver/chatroom/requestAddr.action";
            url += "?roomid=" + roomid + "&accid=" + accid + "&clienttype=1";
            //string jsondata = "{\"roomid\":" + roomid + "," +
            //      "\"accid\":\"" + accid + "\",\"clienttype\":1}";

            // roomid=66&accidzhangsan&clienttype=1

            TimeSpan ts = DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1);
            string curTime = System.Convert.ToInt32(ts.TotalSeconds).ToString();
            string checkSum = getCheckSum(appSecret, nonce, curTime);
            IDictionary<object, String> headers = new Dictionary<object, String>();
            headers["AppKey"] = appKey;
            headers["Nonce"] = nonce;
            headers["CurTime"] = curTime;
            headers["CheckSum"] = checkSum;
            headers["ContentType"] = "application/x-www-form-urlencoded";
            return HttpPost(url, null, headers);

        }

        /// <summary>
        /// 创建一个直播频道
        /// curl -X POST -H "Content-Type: application/json" -H "AppKey: 29781bbc4db54742a3ebcxxxxxxxxxxx" -H "Nonce: 12345" -H "CurTime: 1469171950571" -H "CheckSum: 4ba6ca70c685eb900917e423eadaxxxxxxxxxxxxx" -d '{"name":"channel_name", "type":0}' https://vcloud.163.com/app/channel/create
        /// </summary>
        /// <param name="name">频道名称（最大长度64个字符，只支持中文、字母、数字和下划线）</param>
        /// <param name="type">频道类型（0:rtmp）</param>
        public static Channel_res channel_create(string name, int type)
        {
            String url = "https://vcloud.163.com/app/channel/create";
            string jsondata = "{\"name\":\"" + name + "\"," +
                  "\"type\":" + type.ToString() + "}";

           // url += "?name=" + name + "&creator=" + type.ToString() ;

            TimeSpan ts = DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1);
            string curTime = System.Convert.ToInt32(ts.TotalSeconds).ToString();
            string checkSum = getCheckSum(appSecret, nonce, curTime);
            IDictionary<object, String> headers = new Dictionary<object, String>();
            headers["AppKey"] = appKey;
            headers["Nonce"] = nonce;
            headers["CurTime"] = curTime;
            headers["CheckSum"] = checkSum;
            headers["ContentType"] = "application/json;charset=utf-8";
            string json = HttpPost(url, jsondata, headers, "application/json;charset=utf-8");
            Channel_res jo = new Channel_res();
            jo.ret = new Channel_Ret_res();
            jo = Newtonsoft.Json.JsonConvert.DeserializeObject<Channel_res>(json);

            return jo;
            
        }
        /// <summary>
        /// 设置频道录制状态
        /// </summary>
        /// <param name="cid">频道ID，32位字符串</param>
        /// <param name="needRecord">1-开启录制； 0-关闭录制</param>
        /// <param name="format">1-flv； 0-mp4</param>
        /// <param name="duration">录制切片时长(分钟)，5~120分钟</param>
        /// <param name="filename">录制后文件名（只支持中文、字母和数字），格式为filename_YYYYMMDD-HHmmssYYYYMMDD-HHmmss, 文件名录制起始时间（年月日时分秒) -录制结束时间（年月日时分秒)</param>
        /// <returns></returns>
        public static ChannelJson ChannelSetRecord(string cid,int needRecord,int format,string duration,string filename)
        {
            var url = "https://vcloud.163.com/app/channel/setAlwaysRecord";
            var  para = "{\"cid\":\"" + cid + "\"," + "\"needRecord\":" + needRecord + ",\"format\":"+ format + ",\"duration\":\""+ duration + "\",\"filename\":\""+ filename + "\"}";
            var headers = CreateChannelPostHeaders();
            var result = HttpPost(url, para, headers, "application/json;charset=utf-8");
            return Newtonsoft.Json.JsonConvert.DeserializeObject<ChannelJson>(result);
        }

        /// <summary>
        /// 设置Post数据时的所需的头部信息
        /// </summary>
        /// <returns></returns>
        public static IDictionary<object, string> CreateChannelPostHeaders()
        {
            var ts = DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1);
            var curTime = System.Convert.ToInt32(ts.TotalSeconds).ToString();
            var checkSum = getCheckSum(appSecret, nonce, curTime);
            var  headers = new Dictionary<object, String>();
            headers["AppKey"] = appKey;
            headers["Nonce"] = nonce;
            headers["CurTime"] = curTime;
            headers["CheckSum"] = checkSum;
            headers["ContentType"] = "application/json;charset=utf-8";
            return headers;
        }


        public static string HttpPost(string url, string pdata, IDictionary<object, string> headers = null, string contentType=null)
        {
            System.Net.WebRequest request = HttpWebRequest.Create(url);
            request.Method = "POST";

            if (headers != null)
            {
                foreach (var v in headers)
                {
                    if (v.Key is HttpRequestHeader)
                        request.Headers[(HttpRequestHeader)v.Key] = v.Value;
                    else
                        request.Headers[v.Key.ToString()] = v.Value;
                }
            }

            if (pdata != null)
            {
                if(contentType != null)
                    request.ContentType = contentType;
                byte[] data = Encoding.UTF8.GetBytes(pdata);
                request.ContentLength = data.Length;

                Stream myRequestStream = request.GetRequestStream();

                myRequestStream.Write(data, 0, data.Length);
                myRequestStream.Close();

            }
            HttpWebResponse response = null;
            Stream dataStream = null;
            StreamReader reader = null;
            try
            {
                // Get the response.
                response = (HttpWebResponse)request.GetResponse();
                // Display the status.
                //Console.WriteLine(response.StatusDescription);
                // Get the stream containing content returned by the server.
                dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                reader = new StreamReader(dataStream);
                // Read the content.
                return reader.ReadToEnd();
                // Display the content.
                //Console.WriteLine(responseFromServer);
                // Cleanup the streams and the response.
                
            }
            catch (Exception e)
            {
                string ee = e.Message;
                //Console.WriteLine(e.Message);
                //Console.WriteLine(response.StatusDescription);
            }
            finally
            {
                if (reader != null && dataStream != null && response != null)
                {
                    reader.Close();
                    dataStream.Close();
                    response.Close();
                }
               
            }
            return "";
        }
        // 计算并获取CheckSum
        public static String getCheckSum(String appSecret, String nonce, String curTime)
        {
            byte[] data = Encoding.Default.GetBytes(appSecret + nonce + curTime);
            byte[] result;

            SHA1 sha = new SHA1CryptoServiceProvider();
            // This is one implementation of the abstract class SHA1.
            result = sha.ComputeHash(data);

            return getFormattedText(result);
        }

        // 计算并获取md5值
        public static String getMD5(String requestBody)
        {
            if (requestBody == null)
                return null;

            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5 md5Hasher = MD5.Create();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(requestBody));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return getFormattedText(Encoding.Default.GetBytes(sBuilder.ToString()));
        }

        private static String getFormattedText(byte[] bytes)
        {
            char[] HEX_DIGITS = { '0', '1', '2', '3', '4', '5',
            '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
            int len = bytes.Length;
            StringBuilder buf = new StringBuilder(len * 2);
            for (int j = 0; j < len; j++)
            {
                buf.Append(HEX_DIGITS[(bytes[j] >> 4) & 0x0f]);
                buf.Append(HEX_DIGITS[bytes[j] & 0x0f]);
            }
            return buf.ToString();
        }
    }

    public class Channel_res
    {
        public Channel_res() { }
        /// <summary>
        /// 状态码
        /// </summary>
        public int code { get; set; }
        
        public Channel_Ret_res ret{get;set;}

        /// <summary>
        /// 错误信息
        /// </summary>
        public string msg { get; set; }
    }
    public class Channel_Ret_res
    {
        public Channel_Ret_res() { }
        /// <summary>
        /// 频道ID，32位字符串
        /// </summary>
        public string cid { get; set; }

        /// <summary>
        /// 创建频道的时间戳
        /// </summary>
        public string ctime { get; set; }

        /// <summary>
        /// 频道名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 推流地址
        /// </summary>
        public string pushUrl { get; set; }

        /// <summary>
        /// http拉流地址
        /// </summary>
        public string httpPullUrl { get; set; }

        /// <summary>
        /// hls拉流地址
        /// </summary>
        public string hlsPullUrl { get; set; }

        /// <summary>
        /// rtmp拉流地址
        /// </summary>
        public string rtmpPullUrl { get; set; }
    }

    public class Chatroom_res
    {
        public Chatroom_res() { }

        public int code { get; set; }
        public Chatroom_Info_res chatroom { get; set; }
    }
    public class Chatroom_Info_res
    {
        public Chatroom_Info_res() { }

        public int roomid { get; set; }

        public string valid { get; set; }

        public string announcement { get; set; }

        public string name { get; set; }
        public string broadcasturl { get; set; }
        public string ext { get; set; }
        public string creator { get; set; }
    }


    public class Account_res
    {
        public Account_res() { }

        public int code { get; set; }

        public Account_Info_res info { get; set; }

    }
    public class Account_Info_res
    {
        public Account_Info_res() { }

        public string accid { get; set; }

        public string token { get; set; }

        public string name { get; set; }

    }

    public class ChatroomAddr
    {
        public int code { get; set; }

        public List<string> addr { get; set; }

    }

    public class ChannelJson
    {
        /// <summary>
        /// 状态码
        /// </summary>
        public int code { get; set; }
        /// <summary>
        /// 提示信息
        /// </summary>
        public string msg { get; set; }
    }

    public class CodeJson
    {
        /// <summary>
        /// 状态码
        /// </summary>
        public int code { get; set; }
    }
}
