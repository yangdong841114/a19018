﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;


namespace Library
{
   public  class Cookie
    {
       public Cookie()    {}

       /// 
       /// Cookies赋值
       /// 
       /// 主键
       /// 键值
       /// 有效天数
       /// 
       //public static bool setCookie(string strName, string strValue, int strDay)
       //{
       //    try
       //    {
       //        HttpCookie Cookie = new HttpCookie(strName);
       //        //Cookie.Domain = ".xxx.com";//当要跨域名访问的时候,给cookie指定域名即可,格式为.xxx.com
       //        Cookie.Expires = DateTime.Now.AddDays(strDay);
       //        strValue=HttpUtility.UrlEncode(strValue, Encoding.GetEncoding("UTF-8"));
       //        Cookie.Value = strValue;
       //        System.Web.HttpContext.Current.Response.Cookies.Add(Cookie);
       //        return true;
       //    }
       //    catch
       //    {
       //        return false;
       //    }
       //}

       public static bool setCookie(string strName, string strValue, string Seconds)
       {
           try
           {
               HttpCookie Cookie = new HttpCookie(strName);
               //Cookie.Domain = ".xxx.com";//当要跨域名访问的时候,给cookie指定域名即可,格式为.xxx.com
               if (Seconds != "0")
               {
                   Cookie.Expires = DateTime.Now.AddSeconds(int.Parse(Seconds));
               }
               
               strValue = HttpUtility.UrlEncode(strValue, Encoding.GetEncoding("UTF-8"));
               Cookie.Value = strValue;
               System.Web.HttpContext.Current.Response.Cookies.Add(Cookie);
               return true;
           }
           catch
           {
               return false;
           }
       }
   
       /// 
       /// 读取Cookies
       /// 
       /// 主键
       /// 

       public static string getCookie(string strName)
       {
           HttpCookie Cookie = System.Web.HttpContext.Current.Request.Cookies[strName];
           if (Cookie != null && Cookie.Value!="")
           {
               return HttpUtility.UrlDecode(Cookie.Value, Encoding.GetEncoding("UTF-8"));
              // return Cookie.Value;
           }
           else
           {
               return null;
           }
       }

       /// 
       /// 删除Cookies
       /// 
       /// 主键
       /// 
       public static bool delCookie(string strName)
       {
           try
           {
               HttpCookie Cookie = new HttpCookie(strName);
               //Cookie.Domain = ".xxx.com";//当要跨域名访问的时候,给cookie指定域名即可,格式为.xxx.com
               Cookie.Value = null;
               Cookie.Expires = DateTime.Now.AddYears(-10); 
               System.Web.HttpContext.Current.Response.Cookies.Add(Cookie);
               return true;
           }
           catch
           {
               return false;
           }
       }



    }
}
