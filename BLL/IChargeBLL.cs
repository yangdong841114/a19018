﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 会员充值业务逻辑接口
    /// </summary>
    public interface IChargeBLL : IBaseBLL<Charge>
    {

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <returns></returns>
        PageResult<Charge> GetListPage(Charge model);

        /// <summary>
        /// 保存会员充值申请
        /// </summary>
        /// <param name="model">保存数据</param>
        /// <param name="current">当前会员</param>
        /// <returns></returns>
        int SaveCharge(Charge model, Member current);

        /// <summary>
        /// 保存移动支付充值记录
        /// </summary>
        /// <param name="model"></param>
        /// <param name="current"></param>
        /// <returns></returns>
        int SavePayCharge(Charge model, Member current);

        /// <summary>
        /// 取消充值申请
        /// </summary>
        /// <param name="id">充值申请ID</param>
        /// <param name="current">当前会员</param>
        /// <returns></returns>
        int UpdateCancel(int id, Member current);


        /// <summary>
        /// 审核充值申请
        /// </summary>
        /// <param name="id">充值申请ID</param>
        /// <param name="current">当前登录用户</param>
        /// <returns></returns>
        int UpdateAudit(int id, Member current);

        /// <summary>
        /// 导出EXCEL查询
        /// </summary>
        /// <param name="model">查询参数</param>
        /// <returns></returns>
        DataTable GetExcelListPage(Charge model);

        /// <summary>
        /// 导出转账excel
        /// </summary>
        /// <returns></returns>
        DataTable GetChargeExcel();
    }
}
