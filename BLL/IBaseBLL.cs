﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public interface IBaseBLL<T>
    {
        /// <summary>
        /// 保存记录
        /// </summary>
        /// <param name="dto">对象</param>
        /// <returns>自增生成的ID</returns>
        object SaveByIdentity(DtoData dto);

        /// <summary>
        /// 保存记录
        /// </summary>
        /// <param name="dto">对象</param>
        /// <returns>执行行数</returns>
        int Save(DtoData dto);

        /// <summary>
        /// 根据更新一条记录 
        /// </summary>
        /// <param name="dto">要保存的对象，必须有id字段为主键</param>
        /// <returns>执行的行数</returns>
        int Update(DtoData dto);

        /// <summary>
        /// 根据ID删除记录
        /// </summary>
        /// <param name="table">表名</param>
        /// <param name="id">ID</param>
        /// <returns>执行的行数</returns>
        int Delte(string table,object id);

        /// <summary>
        /// 根据SQL条件查询记录条数
        /// </summary>
        /// <param name="sql">查询语句</param>
        /// <param name="param">参数列表</param>
        /// <returns>记录数</returns>
        int GetCount(string sql, List<DbParameterItem> param);

        /// <summary>
        /// 根据SQL查询记录条数
        /// </summary>
        /// <param name="sql">查询语句</param>
        /// <returns>记录数</returns>
        int GetCount(string sql);

        /// <summary>
        /// 根据SQL条件查询单个记录
        /// </summary>
        /// <param name="sql">sql</param>
        /// <param name="param">参数列表</param>
        /// <returns>DataRow行数据</returns>
        T GetOne(string sql, List<DbParameterItem> param);

        /// <summary>
        /// 根据SQL查询单个记录
        /// </summary>
        /// <param name="sql">sql</param>
        /// <returns>DataRow行数据</returns>
        T GetOne(string sql);

        /// <summary>
        /// 根据SQL条件查询列表
        /// </summary>
        /// <param name="sql">查询语句</param>
        /// <param name="param">参数列表</param>
        /// <returns>列表</returns>
        List<T> GetList(string sql, List<DbParameterItem> param);


        /// <summary>
        /// 根据SQL查询列表
        /// </summary>
        /// <param name="sql">查询语句</param>
        /// <returns>列表</returns>
        List<T> GetList(string sql);
    }
}
