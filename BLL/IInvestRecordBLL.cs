﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace BLL
{
    /// <summary>
    /// 投资记录业务逻辑接口
    /// </summary>
    public interface IInvestRecordBLL : IBaseBLL<InvestRecord>
    {

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <returns></returns>
        PageResult<InvestRecord> GetListPage(InvestRecord model);

        PageResult<SumInvestRecord> GetListPageGroupBy(InvestRecord model);
        PageResult<InvestRecord> GetListPageSj(InvestRecord model);
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="model">保存数据</param>
        /// <returns></returns>
        int SaveInvestRecord(InvestRecord model);

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model">修改数据</param>
        /// <param name="current">当前会员</param>
        /// <returns></returns>
        int UpdateInvestRecord(InvestRecord model, Member current);

        /// <summary>
        /// 获取银行实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        InvestRecord GetModel(int id);
        string OCCgateway(string onlineOrtest, string orderID);
        void AutoCurrencyToOCC();
        void AutoCurrencyToOCCHASH();
        void CurrencyToOcc(string occSignCode, string occUrl, string type, string date, string path);
        void CurrencyToOcc(string occSignCode, string occUrl, string type, string date);
        void CurrencyToOcc(int id, string occSignCode, string occUrl, Member current);
        void CallbackOcc(string data);

        DataTable GetExcelListPageInvestYx(InvestRecord model);
        DataTable GetExcelListPageInvestSj(InvestRecord model);
    }
}
