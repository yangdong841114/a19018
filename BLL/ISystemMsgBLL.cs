﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 操作日志业务逻辑接口
    /// </summary>
    public interface ISystemMsgBLL
    {
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="toUid"></param>
        /// <returns></returns>
        List<SystemMsg> GetList(int toUid);

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="modle"></param>
        /// <returns></returns>
        int Save(SystemMsg modle);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        int Delete(string url,int toUid);

        //获取主页数据
        Dictionary<string, List<SystemMsg>> GetMainData();
    }
}
