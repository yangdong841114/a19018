﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 投资记录业务逻辑接口
    /// </summary>
    public interface IInvestRecordKtBLL : IBaseBLL<InvestRecordKt>
    {

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <returns></returns>
        PageResult<InvestRecordKt> GetListPage(InvestRecordKt model);

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="model">保存数据</param>
        /// <param name="current">当前会员</param>
        /// <returns></returns>
        int SaveInvestRecordKt(InvestRecordKt model, Member current);

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model">修改数据</param>
        /// <param name="current">当前会员</param>
        /// <returns></returns>
        int UpdateInvestRecordKt(InvestRecordKt model, Member current);

        /// <summary>
        /// 获取银行实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        InvestRecordKt GetModel(int id);
        System.Data.DataTable GetExcelListPageInvestKt(InvestRecordKt model);
        

    }
}
