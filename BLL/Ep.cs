﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace BLL
{
    public class Ep
    {
         private static readonly DAL.Ep dal = new DAL.Ep();
         public Ep() { }

         /// <summary>
         /// 删除一条数据
         /// </summary>
         /// <param name="id">id</param>
         /// <returns>操作结果int,影响的行数</returns>
         public bool Delete(int id)
         {
             return dal.Delete(id) > 0;
         }

       /// <summary>
       /// 增加一条数据
       /// </summary>
       /// <param name="model">实体对象</param>
       /// <returns>操作结果int,影响的行数</returns>
         public string Add(string str)
       {
           return dal.Add(str);
       }





       ///<summary>
       ///根据条件获得分页数据列表
       ///</summary>
       /// <param name="strWhere">条件</param>
       /// <param name="StartIndex">起始记录索引</param>
       ///<param name="PageSize">分页大小</param>
       ///<returns>数据集DataTable</returns>
       public DataTable GetListByWhere(string strWhere, int StartIndex, int PageSize)
       {
           return dal.GetListByWhere(strWhere, StartIndex, PageSize);
       }



       ///<summary>
       ///根据条件获得分页数据列表
       ///</summary>
       /// <param name="strWhere">条件</param>
       /// <param name="StartIndex">起始记录索引</param>
       ///<param name="PageSize">分页大小</param>
       ///<returns>数据集DataTable</returns>
       public DataTable GetListByWhere1(string strWhere, int StartIndex, int PageSize)
       {
           return dal.GetListByWhere1(strWhere, StartIndex, PageSize);
       }


      
       ///<summary>
       ///获得数据条数
       ///</summary>
       ///<param name="strWhere">条件</param>
       ///<returns>数据条数</returns>
       public int GetListCountByWhere(string strWhere)
       {
           return dal.GetListCountByWhere(strWhere);
       }


       ///<summary>
       ///获得数据条数
       ///</summary>
       ///<param name="strWhere">条件</param>
       ///<returns>数据条数</returns>
       public int GetListCountByWhere1(string strWhere)
       {
           return dal.GetListCountByWhere1(strWhere);
       }
    }
}
