﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class InvestBackRecordBLL : BaseBLL<InvestBackRecord>, IInvestBackRecordBLL
    {
        public IParamSetBLL psBLL { get; set; }
        private System.Type type = typeof(InvestBackRecord);

        public IBaseDao dao { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new InvestBackRecord GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param,true);
            if (row == null) return null;
            InvestBackRecord mb = (InvestBackRecord)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new InvestBackRecord GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            InvestBackRecord mb = (InvestBackRecord)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<InvestBackRecord> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<InvestBackRecord> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count>0)
            {
                list = new List<InvestBackRecord>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((InvestBackRecord)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<InvestBackRecord> GetList(string sql)
        {
            List<InvestBackRecord> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<InvestBackRecord>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((InvestBackRecord)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="model">保存数据</param>
        /// <returns></returns>
        public void SaveModel(InvestBackRecord model)
        {
            dao.SaveByIdentity(model);
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model">修改数据</param>
        /// <returns></returns>
        public int UpdateModel(InvestBackRecord model)
        {
            return dao.Update(model);
        }

    }
}
