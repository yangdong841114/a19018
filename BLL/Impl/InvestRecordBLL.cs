﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;
using Newtonsoft.Json;

namespace BLL.Impl
{
    public class InvestRecordBLL : BaseBLL<InvestRecord>, IInvestRecordBLL
    {
        public IParamSetBLL psBLL { get; set; }
        private System.Type type = typeof(InvestRecord);

        public IMemberBLL memberBLL { get; set; }

        public IBaseSetBLL setBLL { get; set; }

        public IBaseDao dao { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new InvestRecord GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param, true);
            if (row == null) return null;
            InvestRecord mb = (InvestRecord)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new InvestRecord GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            InvestRecord mb = (InvestRecord)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<InvestRecord> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<InvestRecord> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<InvestRecord>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((InvestRecord)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<InvestRecord> GetList(string sql)
        {
            List<InvestRecord> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<InvestRecord>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((InvestRecord)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public PageResult<InvestRecord> GetListPage(InvestRecord model)
        {
            PageResult<InvestRecord> page = new PageResult<InvestRecord>();
            string sql = "select *,row_number() over(order by m.id desc) rownumber from InvestRecord m where 1=1 ";
            string countSql = "select count(1) from InvestRecord m where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    param.Add(new DbParameterItem("m.uid", ConstUtil.EQ, model.uid));
                }
                if (!ValidateUtils.CheckIntZero(model.isAcitve))
                {
                    if (model.isAcitve == -1) model.isAcitve = 0;
                    param.Add(new DbParameterItem("m.isAcitve", ConstUtil.EQ, model.isAcitve));
                }
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.userName))
                {
                    param.Add(new DbParameterItem("m.userName", ConstUtil.LIKE, model.userName));
                }
                if (!ValidateUtils.CheckNull(model.fromAddress))
                {
                    param.Add(new DbParameterItem("m.fromAddress", ConstUtil.LIKE, model.fromAddress));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);
            List<DbParameterItem> param_nopage = param;

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<InvestRecord> list = new List<InvestRecord>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((InvestRecord)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;

            //汇总已审未审
            dt = dao.GetList(sql, param_nopage, true);
            double all_ethValue = 0;
            double all_ethValueTz = 0;
            double all_epointsETHToTCS = 0;
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    InvestRecord model_row = (InvestRecord)ReflectionUtil.GetModel(type, row);
                    all_ethValue += model_row.ethValue.Value;
                    all_ethValueTz += model_row.ethValueTz.Value;
                    all_epointsETHToTCS += model_row.epointsETHToTCS.Value;
                }
            }
            InvestRecord foot_model = new InvestRecord();
            list = new List<InvestRecord>();
            foot_model.ethValue = all_ethValue;
            foot_model.ethValueTz = all_ethValueTz;
            foot_model.epointsETHToTCS = all_epointsETHToTCS;
            foot_model.fromAddress = "合计";
            foot_model.HASH = "";
            list.Add(foot_model);
            page.footer = list;
            return page;
        }


        public PageResult<SumInvestRecord> GetListPageGroupBy(InvestRecord model)
        {
            PageResult<SumInvestRecord> page = new PageResult<SumInvestRecord>();
            string sql = "select t.*,row_number() over(order by ethValue desc) rownumber from (select userName,SUM(ethValue)as ethValue  from InvestRecord m where isAcitve =1 group by userName) t";
            string countSql = "select COUNT(1) from (select userName from InvestRecord m where isAcitve =1 group by userName)  t";
            List<DbParameterItem> param = new List<DbParameterItem>();

            //查询记录条数
            page.total = dao.GetCount(countSql);
            List<DbParameterItem> param_nopage = param;

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            //sql += " group by userName";
            DataTable dt = dao.GetPageList(sql, param);
            List<SumInvestRecord> list = new List<SumInvestRecord>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((SumInvestRecord)ReflectionUtil.GetModel(typeof(SumInvestRecord), row));
                }
            }
            page.rows = list;
            return page;
        }

        public PageResult<InvestRecord> GetListPageSj(InvestRecord model)
        {
            PageResult<InvestRecord> page = new PageResult<InvestRecord>();
            string sql = @"select ir.*,ir.addTime as sjTime,isnull(jj.all_epointsETH,0) as srETH,isnull(xf.all_epointsETH,0) as xfETH,isnull(jj.all_epointsETH,0)-isnull(xf.all_epointsETH,0) as szceETH,isnull(xf.all_epointsTCS,0) as xfTCS,row_number() over(order by ir.addTime desc) rownumber  from 
(
select uid,userId,userName,cast(addTime as date) as addTime,
sum(case when isAcitve = 1 then ethValueTz else 0 end) as tzbjETH,
sum(case when isAcitve = 1 then epointsETHToTCS  else 0 end) as xhTCS
from InvestRecord group by uid,userId,userName,cast(addTime as date)
) ir

left join 
(
select SUM(epoints) as all_epoints,SUM(epointsETH) as all_epointsETH,SUM(epointsETHToTCS) as all_epointsTCS,uid from Currency where 1=1 group by uid
) jj
on ir.uid=jj.uid

left join 
(
select SUM(epointsETH) as all_epointsETH,SUM(epointsETHToTCS) as all_epointsTCS, uid from CurrencyXf where 1=1 group by uid
) xf
on ir.uid=xf.uid where 1=1 ";
            string countSql = @"select count(1)  from 
(
select uid,userId,userName,cast(addTime as date) as addTime,
sum(case when isAcitve = 1 then ethValueTz else 0 end) as tzbjETH,
sum(case when isAcitve = 1 then epointsETHToTCS  else 0 end) as xhTCS
from InvestRecord group by uid,userId,userName,cast(addTime as date)
) ir

left join 
(
select SUM(epoints) as all_epoints,SUM(epointsETH) as all_epointsETH,SUM(epointsETHToTCS) as all_epointsTCS,uid from Currency where 1=1 group by uid
) jj
on ir.uid=jj.uid

left join 
(
select SUM(epointsETH) as all_epointsETH,SUM(epointsETHToTCS) as all_epointsTCS, uid from CurrencyXf where 1=1 group by uid
) xf
on ir.uid=xf.uid where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("ir.userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.userName))
                {
                    param.Add(new DbParameterItem("ir.userName", ConstUtil.LIKE, model.userName));
                }

                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("ir.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("ir.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);
            List<DbParameterItem> param_nopage = param;

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<InvestRecord> list = new List<InvestRecord>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((InvestRecord)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;

            //汇总已审未审
            dt = dao.GetList(sql, param_nopage, true);
            double all_srETH = 0;
            double all_xfETH = 0;
            double all_szceETH = 0;
            double all_tzbjETH = 0;
            double all_xfTCS = 0;
            double all_xhTCS = 0;
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    InvestRecord model_row = (InvestRecord)ReflectionUtil.GetModel(type, row);
                    all_srETH += model_row.srETH.Value;
                    all_xfETH += model_row.xfETH.Value;
                    all_szceETH += model_row.szceETH.Value;
                    all_tzbjETH += model_row.tzbjETH.Value;
                    all_xfTCS += model_row.xfTCS.Value;
                    all_xhTCS += model_row.xhTCS.Value;
                }
            }
            InvestRecord foot_model = new InvestRecord();
            list = new List<InvestRecord>();
            foot_model.srETH = all_srETH;
            foot_model.xfETH = all_xfETH;
            foot_model.szceETH = all_szceETH;
            foot_model.tzbjETH = all_tzbjETH;
            foot_model.xfTCS = all_xfTCS;
            foot_model.xhTCS = all_xhTCS;
            foot_model.userName = "合计";
            foot_model.HASH = "";
            list.Add(foot_model);
            page.footer = list;
            return page;
        }

        public string OCCgateway(string onlineOrtest, string orderID)
        {
            string poc_back = "没有调用";
            string is_success = "y";
            string sql_update = "";
            string id = "0";
            string address = "";
            string value = "";
            DataTable dt = new DataTable();
            //在线环境才调用接口去提现
            if (onlineOrtest == "online")
            {
                if (orderID.IndexOf("A19018OCCXH_v1v_") != -1)
                {
                    dt = dao.GetList("select * from InvestRecord where id=" + orderID.Replace("A19018OCCXH_v1v_", ""));
                    address = dt.Rows[0]["userId"].ToString();
                    value = dt.Rows[0]["epointsETHToTCS"].ToString();
                }
                if (orderID.IndexOf("A19018XFETH_v1v_") != -1)
                {
                    dt = dao.GetList("select * from CurrencyXf where id=" + orderID.Replace("A19018XFETH_v1v_", ""));
                    address = dt.Rows[0]["userId"].ToString();
                    value = dt.Rows[0]["epointsETH"].ToString();
                }
                if (orderID.IndexOf("A19018XFOCC_v1v_") != -1)
                {
                    dt = dao.GetList("select * from CurrencyXf where id=" + orderID.Replace("A19018XFOCC_v1v_", ""));
                    address = dt.Rows[0]["userId"].ToString();
                    value = dt.Rows[0]["epointsETHToOCC"].ToString();
                }
                if (orderID.IndexOf("A19018KTOCC_v1v_") != -1)
                {
                    dt = dao.GetList("select * from InvestRecordKt where id=" + orderID.Replace("A19018KTOCC_v1v_", ""));
                    address = dt.Rows[0]["userId"].ToString();
                    value = dt.Rows[0]["TcsKt"].ToString();
                }
                if (orderID.IndexOf("A19018KTETH_v1v_") != -1)
                {
                    dt = dao.GetList("select * from InvestRecordKt where id=" + orderID.Replace("A19018KTETH_v1v_", ""));
                    address = dt.Rows[0]["userId"].ToString();
                    value = dt.Rows[0]["ethValueKb"].ToString();
                }

                poc_back = Common.JhInterface.pocGateway(orderID, "A19018", "自动生成", address, value, "OCC", "0");
                if (poc_back.IndexOf("success:") == -1 && poc_back.IndexOf("code\\u0027:\\u00270000") == -1 && poc_back.IndexOf("code\\u0027:\\u00272001") == -1) { poc_back = "POC兑现网关失败-" + poc_back + "-"; is_success = "n"; }
                string txhash = "";
                if (poc_back.IndexOf("txhash") != -1)
                    txhash = poc_back.Substring(poc_back.IndexOf("txhash"));
                if (txhash.Length > 100)
                {
                    txhash = txhash.Substring(0, 95);
                    poc_back += " hash_is_" + txhash;
                }
                poc_back = DateTime.Now.ToString() + poc_back.Replace("\u0027", "'");



                if (orderID.IndexOf("A19018OCCXH_v1v_") != -1)
                {
                    id = orderID.Replace("A19018OCCXH_v1v_", "");
                    sql_update = "update InvestRecord set XhLog='" + poc_back + "' ";
                    if (is_success == "y") sql_update += ",isXh=1 ";
                    sql_update += " where id=" + id;
                }
                if (orderID.IndexOf("A19018XFETH_v1v_") != -1)
                {
                    id = orderID.Replace("A19018XFETH_v1v_", "");
                    sql_update = "update CurrencyXf set XfLogETH='" + poc_back + "' ";
                    if (is_success == "y") sql_update += ",isxfETH=1 ";
                    sql_update += " where id=" + id;
                }
                if (orderID.IndexOf("A19018XFOCC_v1v_") != -1)
                {
                    id = orderID.Replace("A19018XFTCS_v1v_", "");
                    sql_update = "update CurrencyXf set XfLogTCS='" + poc_back + "' ";
                    if (is_success == "y") sql_update += ",isxfTCS=1 ";
                    sql_update += " where id=" + id;
                }
                if (orderID.IndexOf("A19018KTOCC_v1v_") != -1)
                {
                    id = orderID.Replace("A19018KTOCC_v1v_", "");
                    sql_update = "update InvestRecordKt set KtLog='" + poc_back + "' ";
                    if (is_success == "y") sql_update += ",isSend=1 ";
                    sql_update += " where id=" + id;
                }
                if (orderID.IndexOf("A19018KTETH_v1v_") != -1)
                {
                    id = orderID.Replace("A19018KTETH_v1v_", "");
                    sql_update = "update InvestRecordKt set KtLog='" + poc_back + "' ";
                    if (is_success == "y") sql_update += ",isSend=1 ";
                    sql_update += " where id=" + id;
                }
                dao.ExecuteBySql(sql_update);
            }
            return poc_back;
        }

        public string TCSgatewayNoSelectReturnUpdateSql(string onlineOrtest, string orderID, string input_address, string input_value)
        {
            string poc_back = "没有调用";
            string is_success = "y";
            string sql_update = "";
            string id = "0";
            string address = input_address;
            string value = input_value;
            DataTable dt = new DataTable();
            //在线环境才调用接口去提现
            if (onlineOrtest == "online")
            {
                //if (orderID.IndexOf("A19018TCSXH_v1v_") != -1)
                //{
                //    dt = dao.GetList("select * from InvestRecord where id=" + orderID.Replace("A19018TCSXH_v1v_", ""));
                //    address = dt.Rows[0]["userId"].ToString();
                //    value = dt.Rows[0]["epointsETHToTCS"].ToString();
                //}
                //if (orderID.IndexOf("A19018XFETH_v1v_") != -1)
                //{
                //    dt = dao.GetList("select * from CurrencyXf where id=" + orderID.Replace("A19018XFETH_v1v_", ""));
                //    address = dt.Rows[0]["userId"].ToString();
                //    value = dt.Rows[0]["epointsETH"].ToString();
                //}
                //if (orderID.IndexOf("A19018XFTCS_v1v_") != -1)
                //{
                //    dt = dao.GetList("select * from CurrencyXf where id=" + orderID.Replace("A19018XFTCS_v1v_", ""));
                //    address = dt.Rows[0]["userId"].ToString();
                //    value = dt.Rows[0]["epointsETHToTCS"].ToString();
                //}
                //if (orderID.IndexOf("A19018KTTCS_v1v_") != -1)
                //{
                //    dt = dao.GetList("select * from InvestRecordKt where id=" + orderID.Replace("A19018KTTCS_v1v_", ""));
                //    address = dt.Rows[0]["userId"].ToString();
                //    value = dt.Rows[0]["TcsKt"].ToString();
                //}
                //if (orderID.IndexOf("A19018KTETH_v1v_") != -1)
                //{
                //    dt = dao.GetList("select * from InvestRecordKt where id=" + orderID.Replace("A19018KTETH_v1v_", ""));
                //    address = dt.Rows[0]["userId"].ToString();
                //    value = dt.Rows[0]["ethValueKb"].ToString();
                //}

                poc_back = Common.JhInterface.pocGateway(orderID, "A19018", "自动生成", address, value, "OCC", "0");
                if (poc_back.IndexOf("success:") == -1 && poc_back.IndexOf("code\\u0027:\\u00270000") == -1 && poc_back.IndexOf("code\\u0027:\\u00272001") == -1) { poc_back = "POC兑现网关失败-" + poc_back + "-"; is_success = "n"; }
                string txhash = "";
                if (poc_back.IndexOf("txhash") != -1)
                    txhash = poc_back.Substring(poc_back.IndexOf("txhash"));
                if (txhash.Length > 100)
                {
                    txhash = txhash.Substring(0, 95);
                    poc_back += " hash_is_" + txhash;
                }
                poc_back = DateTime.Now.ToString() + poc_back.Replace("\u0027", "'");

                if (orderID.IndexOf("A19018OCCXH_v1v_") != -1)
                {
                    id = orderID.Replace("A19018OCCXH_v1v_", "");
                    sql_update = "update InvestRecord set XhLog='" + poc_back + "' ";
                    if (is_success == "y") sql_update += ",isXh=1 ";
                    sql_update += " where id=" + id;
                }
                if (orderID.IndexOf("A19018XFETH_v1v_") != -1)
                {
                    id = orderID.Replace("A19018XFETH_v1v_", "");
                    sql_update = "update CurrencyXf set XfLogETH='" + poc_back + "' ";
                    if (is_success == "y") sql_update += ",isxfETH=1 ";
                    sql_update += " where id=" + id;
                }
                if (orderID.IndexOf("A19018XFOCC_v1v_") != -1)
                {
                    id = orderID.Replace("A19018XFOCC_v1v_", "");
                    sql_update = "update CurrencyXf set XfLogTCS='" + poc_back + "' ";
                    if (is_success == "y") sql_update += ",isxfTCS=1 ";
                    sql_update += " where id=" + id;
                }
                if (orderID.IndexOf("A19018KTOCC_v1v_") != -1)
                {
                    id = orderID.Replace("A19018KTOCC_v1v_", "");
                    sql_update = "update InvestRecordKt set KtLog='" + poc_back + "' ";
                    if (is_success == "y") sql_update += ",isSend=1 ";
                    sql_update += " where id=" + id;
                }
                if (orderID.IndexOf("A19018KTETH_v1v_") != -1)
                {
                    id = orderID.Replace("A19018KTETH_v1v_", "");
                    sql_update = "update InvestRecordKt set KtLog='" + poc_back + "' ";
                    if (is_success == "y") sql_update += ",isSend=1 ";
                    sql_update += " where id=" + id;
                }
                //dao.ExecuteBySql(sql_update);
            }
            return sql_update + ";";
        }
        public void CallbackOcc(string model)
        {
            OccResponse data = JsonConvert.DeserializeObject<OccResponse>(model);
            if (data.code == "0001") { throw new ValidateException("系统升级中，请迟些再试"); }
            else if (data.code == "0002") { throw new ValidateException("数据与签名不匹配"); }
            else if (data.code == "0003") { throw new ValidateException("JSON数据结构错误"); }
            else if (data.code == "1001") { throw new ValidateException("提交参数错误"); }
            else if (data.code == "1002") { throw new ValidateException("数据验证失败"); }
            else
            {
                StringBuilder historyHashSql = new StringBuilder();
                historyHashSql.Append("select OrderId from HistoryHash");
                var hashModel = dao.GetList(historyHashSql.ToString());
                var hashData = JsonConvert.SerializeObject(hashModel);
                var arrHash = JsonConvert.DeserializeObject<List<HistoryHash>>(hashData).Select(p => p.OrderId).ToArray();
                var historySql = "insert into HistoryHash values ";
                var newData = data.result;
                newData.ToList().ForEach(r =>
                {
                    r.id = Convert.ToInt32(r.orderId.Replace("A19018OCCXH_v1v_", "").Replace("A19018XFOCC_v1v_", "").Replace("A19018KTOCC_v1v_", "").Replace("A19018XFETH_v1v_", "").Replace("A19018KTETH_v1v_", ""));
                });
                string updateSql = "";
                var ethOrOccList = newData.Where(p => !arrHash.Contains(p.id));
                if (ethOrOccList.Any())
                {
                    ethOrOccList.ToList().ForEach(r =>
                    {
                        historySql += "(" + r.id + "),";
                    });
                    ethOrOccList.Where(p => p.orderId.Contains("A19018OCCXH_v1v_")).ToList().ForEach(r =>
                    {
                        var poc_back = r.code != "0000" ? DateTime.Now.ToString() + "下发失败-" + r.result.txhash : "hash_is:" + r.result.txhash;
                        updateSql += string.Format("update InvestRecord set XhLog = '{0}' , isXh = 1 where id = {1};", poc_back, r.id);
                    });
                    ethOrOccList.Where(p => p.orderId.Contains("A19018XFOCC_v1v_")).ToList().ForEach(r =>
                    {
                        var poc_back = r.code != "0000" ? DateTime.Now.ToString() + "下发失败-" + r.result.txhash : "hash_is:" + r.result.txhash;
                        updateSql += string.Format("update CurrencyXf set XfLogTCS = '{0}' , isxfTCS = 1 where id = {1};", poc_back, r.id);
                    });
                    ethOrOccList.Where(p => p.orderId.Contains("A19018KTOCC_v1v_")).ToList().ForEach(r =>
                    {
                        var poc_back = r.code != "0000" ? DateTime.Now.ToString() + "下发失败-" + r.result.txhash : "hash_is:" + r.result.txhash;
                        updateSql += string.Format("update InvestRecordKt set KtLog = '{0}' , isSend = 1 where id = {1};", poc_back, r.id);
                    });
                    ethOrOccList.Where(p => p.orderId.Contains("A19018XFETH_v1v_")).ToList().ForEach(r =>
                    {
                        var poc_back = r.code != "0000" ? DateTime.Now.ToString() + "下发失败-" + r.result.txhash : "hash_is:" + r.result.txhash;
                        updateSql += string.Format("update CurrencyXf set XfLogETH = '{0}' , isxfETH = 1 where id = {1};", poc_back, r.id);
                    });
                    ethOrOccList.Where(p => p.orderId.Contains("A19018KTETH_v1v_")).ToList().ForEach(r =>
                    {
                        var poc_back = r.code != "0000" ? DateTime.Now.ToString() + "下发失败-" + r.result.txhash : "hash_is:" + r.result.txhash;
                        updateSql += string.Format("update InvestRecordKt set KtLog = '{0}' , isSend = 1 where id = {1};", poc_back, r.id);
                    });
                    if (updateSql != "")
                    {
                        dao.ExecuteBySql(updateSql);
                    }
                    StringBuilder tbSql = new StringBuilder(string.Format("delete from SumCurrency;insert into SumCurrency select addDate,sum(case when isxfETH = 1 then epointsETH else 0 end) as XfzsYxfETH, sum(case when isxfETH != 1 then epointsETH else 0 end) as XfzsDxfETH, sum(case when isxfTCS = 1 then epointsETHToTCS else 0 end) as XfzsYxfTCS, sum(case when isxfTCS != 1 then epointsETHToTCS else 0 end) as XfzsDxfTCS, sum(epointsETHToTCS) as XfzsTCS, sum(epointsTCS) as epointsTCS, SUM(fee1) fee1,SUM(epointsETH) XfzsETH,SUM(fee3) fee3,1,1 from CurrencyXf group by addDate order by addDate update[A19018].[dbo].[SumCurrency] set isOcc = 0 where XfzsDxfTCS > 0 update[A19018].[dbo].[SumCurrency] set isEth = 0 where XfzsDxfETH > 0", data));
                    if (historySql != "insert into HistoryHash values ") dao.ExecuteBySql(historySql.TrimEnd(','));
                    dao.ExecuteBySql(tbSql.ToString());
                }
            }
        }
        public void CurrencyToOcc(string occSignCode, string occUrl, string type, string date, string path)
        {
            try
            {
                StringBuilder strSql = new StringBuilder();
                StringBuilder updateStatusSql = new StringBuilder();
                if (type == "OCC")
                {
                    strSql.AppendFormat("select 'OCC' as [type],'A19018OCCXH_v1v_' + cast(id as varchar(10)) as orderId,userId as [address],epointsETHToTCS as [value] from InvestRecord where isAcitve = 1 and isXh = 0 and CONVERT(varchar(10),addTime,21) = '{0}' union select 'OCC' as [type], 'A19018XFOCC_v1v_' + cast(id as varchar(10)) as orderId, userId as [address], epointsETHToTCS as [value] from CurrencyXf where isxfTCS = 0 and addDate = '{0}' union select  'OCC' as [type], 'A19018KTOCC_v1v_' + cast(id as varchar(10)) as orderId, userId as [address], TcsKt as [value] from InvestRecordKt where isSend = 0 and ethOrtcs = 'OCC' and CONVERT(varchar(10),addTime,21) = '{0}';", date);
                    //updateStatusSql.AppendFormat("update SumCurrency set isOcc = 1 where addDate = '{0}'", date);
                }
                else
                {
                    strSql.AppendFormat("select 'ETH' as [type],'A19018XFETH_v1v_' + cast(id as varchar(10)) as orderId,userId as [address],epointsETH as [value] from CurrencyXf where isxfETH = 0 and addDate = '{0}' union select  'ETH' as [type], 'A19018KTETH_v1v_' + cast(id as varchar(10)) as orderId, userId as [address], ethValueKb as [value] from InvestRecordKt where isSend = 0 and ethOrtcs = 'ETH' and CONVERT(varchar(10),addTime,21) = '{0}';", date);
                    //updateStatusSql.AppendFormat("update SumCurrency set isEth = 1 where addDate = '{0}'", date);
                }
                var dt = dao.GetList(strSql.ToString());
                if (dt.Rows.Count > 0)//重新请求取HASH
                {
                    var pageSize = 50;
                    var length = dt.Rows.Count % pageSize > 0 ? dt.Rows.Count / pageSize + 1 : dt.Rows.Count / pageSize;
                    for (int i = 1; i <= length; i++)
                    {
                        var skip = (i - 1) * pageSize;
                        var skipDt = dt.AsEnumerable().Skip(skip).Take(pageSize).CopyToDataTable();
                        var result = Common.JhInterface.pocGateway(skipDt, occSignCode, occUrl);
                        if (result == "操作超时") { throw new ValidateException("操作超时"); }
                        //if (path != "")
                        //{
                        //    var filePath = path + "_" + i + ".txt";
                        //    if (!System.IO.File.Exists(filePath)) using (System.IO.File.Create(filePath)) { }
                        //    System.IO.File.AppendAllText(filePath, result);
                        //}
                        OccResponse data = JsonConvert.DeserializeObject<OccResponse>(result);
                        if (data.code == "0001") { throw new ValidateException("系统升级中，请迟些再试"); }
                        else if (data.code == "0002") { throw new ValidateException("数据与签名不匹配"); }
                        else if (data.code == "0003") { throw new ValidateException("JSON数据结构错误"); }
                        else if (data.code == "1001") { throw new ValidateException("提交参数错误"); }
                        else if (data.code == "1002") { throw new ValidateException("数据验证失败"); }
                        else
                        {
                            var ethOrOccList = data.result;
                            ethOrOccList.ToList().ForEach(r =>
                            {
                                r.id = Convert.ToInt32(r.orderId.Replace("A19018OCCXH_v1v_", "").Replace("A19018XFOCC_v1v_", "").Replace("A19018KTOCC_v1v_", "").Replace("A19018XFETH_v1v_", "").Replace("A19018KTETH_v1v_", ""));
                            });
                            var ethOrOccSuccessList = ethOrOccList.Where(p => p.code == "0000" && p.result.msg == "ok");
                            var ethOrOccUnSuccessList = ethOrOccList.Where(p => p.code != "0000" || p.result.msg != "ok");
                            StringBuilder updateSql = new StringBuilder();
                            string occXhLogIds = "", occXfLogTCSIds = "", occKtLogIds = "", ethXfLogETHIds = "", ethKtLogIds = "", ids = "";
                            ethOrOccSuccessList.Where(p => p.orderId.Contains("A19018OCCXH_v1v_")).Select(p => p.id).ToList().ForEach(r => { occXhLogIds += r.ToString() + ","; });
                            updateSql.Append(occXhLogIds == "" ? "" : string.Format("update InvestRecord set XhLog = 'Pending...' where id in ({0});", occXhLogIds.TrimEnd(',')));
                            ethOrOccSuccessList.Where(p => p.orderId.Contains("A19018XFOCC_v1v_")).Select(p => p.id).ToList().ForEach(r => { occXfLogTCSIds += r.ToString() + ","; });
                            updateSql.Append(occXfLogTCSIds == "" ? "" : string.Format("update CurrencyXf set XfLogTCS = 'Pending...' where id in ({0});", occXfLogTCSIds.TrimEnd(',')));
                            ethOrOccSuccessList.Where(p => p.orderId.Contains("A19018KTOCC_v1v_")).Select(p => p.id).ToList().ForEach(r => { occKtLogIds += r.ToString() + ","; });
                            updateSql.Append(occKtLogIds == "" ? "" : string.Format("update InvestRecordKt set KtLog = 'Pending...' where id in ({0});", occKtLogIds.TrimEnd(',')));
                            ethOrOccSuccessList.Where(p => p.orderId.Contains("A19018XFETH_v1v_")).Select(p => p.id).ToList().ForEach(r => { ethXfLogETHIds += r.ToString() + ","; });
                            updateSql.Append(ethXfLogETHIds == "" ? "" : string.Format("update CurrencyXf set XfLogETH = 'Pending...' where id in ({0});", ethXfLogETHIds.TrimEnd(',')));
                            ethOrOccSuccessList.Where(p => p.orderId.Contains("A19018KTETH_v1v_")).Select(p => p.id).ToList().ForEach(r => { ethKtLogIds += r.ToString() + ","; });
                            updateSql.Append(ethKtLogIds == "" ? "" : string.Format("update InvestRecordKt set KtLog = 'Pending...' where id in ({0});", ethKtLogIds.TrimEnd(',')));
                            var sql = updateSql.ToString();
                            if (updateSql.ToString() != "")
                            {
                                dao.ExecuteBySql(updateSql.ToString());
                            }
                            //ethOrOccUnSuccessList.Select(p => p.id).ToList().ForEach(r => { ids += r.ToString() + ","; });
                            //ids = ids.TrimEnd(',');
                            //if (ids != "") { throw new ValidateException("部分下发成功，其中部分用户已经提交"); }
                        }
                    }
                    dao.ExecuteBySql(string.Format("update Currency set ff = 1 where addDate = '{0}' and uid in (select uid from CurrencyXf where isxfETH = 1 and addDate = '{0}') and ff = 0;", date));
                }
            }
            catch (Exception ex)
            {
                throw new ValidateException(ex.Message);
            }

            //dao.ExecuteBySql(updateStatusSql.ToString());
        }
        public void CurrencyToOcc(int id, string occSignCode, string occUrl, Member current)
        {
            try
            {
                StringBuilder strSql = new StringBuilder();
                StringBuilder updateStatusSql = new StringBuilder();
                strSql.AppendFormat("select  'ETH' as [type], 'A19018KTETH_v1v_' + cast(id as varchar(10)) as orderId, userId as [address], ethValueKb as [value] from InvestRecordKt where id = {0}", id);
                var dt = dao.GetList(strSql.ToString());
                var result = Common.JhInterface.pocGateway(dt, occSignCode, occUrl);
                if (result.IndexOf("{\"code\":\"0000\",\"result\":[") == -1) { throw new ValidateException("提交到链上失败"); }
                dao.ExecuteBySql("update InvestRecordKt set status='已确认',KtLog = 'Pending...', isSend=0,auditUser='ETH审核人：" + current.userId + "' where id=" + id);
            }
            catch (Exception ex)
            {
                throw new ValidateException(ex.Message);
            }

            //dao.ExecuteBySql(updateStatusSql.ToString());
        }
        public void CurrencyToOcc(string occSignCode, string occUrl, string type, string date)
        {
            try
            {
                StringBuilder strSql = new StringBuilder();
                StringBuilder updateStatusSql = new StringBuilder();
                if (type == "OCC")
                {
                    strSql.AppendFormat("select 'OCC' as [type],'A19018OCCXH_v1v_' + cast(id as varchar(10)) as orderId,userId as [address],epointsETHToTCS as [value] from InvestRecord where isAcitve = 1 and isXh = 0 and CONVERT(varchar(10),addTime,21) = '{0}' union select 'OCC' as [type], 'A19018XFOCC_v1v_' + cast(id as varchar(10)) as orderId, userId as [address], epointsETHToTCS as [value] from CurrencyXf where isxfTCS = 0 and addDate = '{0}' union select  'OCC' as [type], 'A19018KTOCC_v1v_' + cast(id as varchar(10)) as orderId, userId as [address], TcsKt as [value] from InvestRecordKt where isSend = 0 and ethOrtcs = 'OCC' and CONVERT(varchar(10),addTime,21) = '{0}';", date);
                    //updateStatusSql.AppendFormat("update SumCurrency set isOcc = 1 where addDate = '{0}'", date);
                }
                else
                {
                    strSql.AppendFormat("select 'ETH' as [type],'A19018XFETH_v1v_' + cast(id as varchar(10)) as orderId,userId as [address],epointsETH as [value] from CurrencyXf where isxfETH = 0 and addDate = '{0}' union select  'ETH' as [type], 'A19018KTETH_v1v_' + cast(id as varchar(10)) as orderId, userId as [address], ethValueKb as [value] from InvestRecordKt where isSend = 0 and ethOrtcs = 'ETH' and CONVERT(varchar(10),addTime,21) = '{0}';", date);
                    //updateStatusSql.AppendFormat("update SumCurrency set isEth = 1 where addDate = '{0}'", date);
                }
                var dt = dao.GetList(strSql.ToString());
                if (dt.Rows.Count > 0)//重新请求取HASH
                {
                    var pageSize = 50;
                    var length = dt.Rows.Count % pageSize > 0 ? dt.Rows.Count / pageSize + 1 : dt.Rows.Count / pageSize;
                    for (int i = 1; i <= length; i++)
                    {
                        var skip = (i - 1) * pageSize;
                        var skipDt = dt.AsEnumerable().Skip(skip).Take(pageSize).CopyToDataTable();
                        var result = Common.JhInterface.pocGateway(skipDt, occSignCode, occUrl);
                        if (result == "操作超时") { throw new ValidateException("操作超时"); }
                        OccResponse data = new OccResponse();
                        try
                        {
                            data = JsonConvert.DeserializeObject<OccResponse>(result);
                        }
                        catch (Exception)
                        {
                            throw new ValidateException("无法连接到远程服务器:" + result);
                        }
                        if (data.code == "0001") { throw new ValidateException("系统升级中，请迟些再试"); }
                        else if (data.code == "0002") { throw new ValidateException("数据与签名不匹配"); }
                        else if (data.code == "0003") { throw new ValidateException("JSON数据结构错误"); }
                        else if (data.code == "1001") { throw new ValidateException("提交参数错误"); }
                        else if (data.code == "1002") { throw new ValidateException("数据验证失败"); }
                        else
                        {
                            var ethOrOccList = data.result;
                            ethOrOccList.ToList().ForEach(r =>
                            {
                                r.id = Convert.ToInt32(r.orderId.Replace("A19018OCCXH_v1v_", "").Replace("A19018XFOCC_v1v_", "").Replace("A19018KTOCC_v1v_", "").Replace("A19018XFETH_v1v_", "").Replace("A19018KTETH_v1v_", ""));
                            });
                            var ethOrOccSuccessList = ethOrOccList.Where(p => p.code == "0000" && p.result.msg == "ok");
                            var ethOrOccUnSuccessList = ethOrOccList.Where(p => p.code != "0000" || p.result.msg != "ok");
                            StringBuilder updateSql = new StringBuilder();
                            string occXhLogIds = "", occXfLogTCSIds = "", occKtLogIds = "", ethXfLogETHIds = "", ethKtLogIds = "", ids = "";
                            ethOrOccSuccessList.Where(p => p.orderId.Contains("A19018OCCXH_v1v_")).Select(p => p.id).ToList().ForEach(r => { occXhLogIds += r.ToString() + ","; });
                            updateSql.Append(occXhLogIds == "" ? "" : string.Format("update InvestRecord set XhLog = 'Pending...' where id in ({0});", occXhLogIds.TrimEnd(',')));
                            ethOrOccSuccessList.Where(p => p.orderId.Contains("A19018XFOCC_v1v_")).Select(p => p.id).ToList().ForEach(r => { occXfLogTCSIds += r.ToString() + ","; });
                            updateSql.Append(occXfLogTCSIds == "" ? "" : string.Format("update CurrencyXf set XfLogTCS = 'Pending...' where id in ({0});", occXfLogTCSIds.TrimEnd(',')));
                            ethOrOccSuccessList.Where(p => p.orderId.Contains("A19018KTOCC_v1v_")).Select(p => p.id).ToList().ForEach(r => { occKtLogIds += r.ToString() + ","; });
                            updateSql.Append(occKtLogIds == "" ? "" : string.Format("update InvestRecordKt set KtLog = 'Pending...' where id in ({0});", occKtLogIds.TrimEnd(',')));
                            ethOrOccSuccessList.Where(p => p.orderId.Contains("A19018XFETH_v1v_")).Select(p => p.id).ToList().ForEach(r => { ethXfLogETHIds += r.ToString() + ","; });
                            updateSql.Append(ethXfLogETHIds == "" ? "" : string.Format("update CurrencyXf set XfLogETH = 'Pending...' where id in ({0});", ethXfLogETHIds.TrimEnd(',')));
                            ethOrOccSuccessList.Where(p => p.orderId.Contains("A19018KTETH_v1v_")).Select(p => p.id).ToList().ForEach(r => { ethKtLogIds += r.ToString() + ","; });
                            updateSql.Append(ethKtLogIds == "" ? "" : string.Format("update InvestRecordKt set KtLog = 'Pending...' where id in ({0});", ethKtLogIds.TrimEnd(',')));
                            var sql = updateSql.ToString();
                            if (updateSql.ToString() != "")
                            {
                                dao.ExecuteBySql(updateSql.ToString());
                            }
                            //ethOrOccUnSuccessList.Select(p => p.id).ToList().ForEach(r => { ids += r.ToString() + ","; });
                            //ids = ids.TrimEnd(',');
                            //if (ids != "") { throw new ValidateException("部分下发成功，其中部分用户已经提交"); }
                        }
                    }
                    dao.ExecuteBySql(string.Format("update Currency set ff = 1 where addDate = '{0}' and uid in (select uid from CurrencyXf where isxfETH = 1 and addDate = '{0}') and ff = 0;", date));
                }
            }
            catch (Exception ex)
            {
                throw new ValidateException(ex.Message);
            }

            //dao.ExecuteBySql(updateStatusSql.ToString());
        }
        /// <summary>
        /// 获取下发的hash
        /// </summary>
        public void AutoCurrencyToOCCHASH()
        {
            string orderID = "";
            string address = "";
            string value = "";
            //空闲时间回调
            DataTable dt_XFETH_Calback = dao.GetList("select id,userId,epointsETHToTCS,isxfETH,isxfTCS from CurrencyXf where  (isxfETH=1 or isxfTCS = 1) and DateDiff(dd,jstime,GETDATE())>0 and XfLogETH not like '%hash_is%'");
            if (dt_XFETH_Calback.Rows.Count > 0)//重新请求取HASH
            {
                for (int i = 0; i < dt_XFETH_Calback.Rows.Count; i++)
                {
                    if (dt_XFETH_Calback.Rows[i]["isxfETH"].ToString() == "1")
                    {
                        orderID = "A19018XFETH_v1v_" + dt_XFETH_Calback.Rows[i]["id"].ToString();
                    }
                    else
                    {
                        orderID = "A19018XFOCC_v1v_" + dt_XFETH_Calback.Rows[i]["id"].ToString();
                    }
                    address = dt_XFETH_Calback.Rows[i]["userId"].ToString();
                    value = dt_XFETH_Calback.Rows[i]["epointsETHToTCS"].ToString();
                    OCCgateway("online", orderID);
                }
            }
        }

        /// <summary>
        /// 下发
        /// </summary>
        public void AutoCurrencyToOCC()
        {
            string sql_update_pl = "";
            string orderID = "";
            string address = "";
            string value = "";
            Dictionary<string, ParameterSet> ps = psBLL.GetDictionaryByCodes("CurrencyFdbs", "ETHkg", "TCSkg");
            double CurrencyFdbs = Convert.ToDouble(ps["CurrencyFdbs"].paramValue);
            string ETHkg = ps["ETHkg"].paramValue;
            string TCSkg = ps["TCSkg"].paramValue;
            if (ETHkg == "开" || TCSkg == "开")
            {

                if (TCSkg == "开")
                {
                    DataTable dt_TCSXH = dao.GetList("select id,userId,epointsETHToTCS from InvestRecord where isAcitve=1 and isXh=0");
                    DataTable dt_XFTCS = dao.GetList("select id,userId,epointsETHToTCS from CurrencyXf where isxfTCS = 0 and isxfETH = 0");
                    DataTable dt_KTTCS = dao.GetList("select id,userId,TcsKt from InvestRecordKt where  isSend=0 and ethOrtcs='OCC' ");
                    if (dt_TCSXH.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt_TCSXH.Rows.Count; i++)
                        {
                            orderID = "A19018OCCXH_v1v_" + dt_TCSXH.Rows[i]["id"].ToString();
                            address = dt_TCSXH.Rows[i]["userId"].ToString();
                            value = dt_TCSXH.Rows[i]["epointsETHToTCS"].ToString();
                            sql_update_pl += " " + TCSgatewayNoSelectReturnUpdateSql("online", orderID, address, value);
                        }
                    }
                    if (dt_XFTCS.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt_XFTCS.Rows.Count; i++)
                        {
                            orderID = "A19018XFOCC_v1v_" + dt_XFTCS.Rows[i]["id"].ToString();
                            address = dt_XFTCS.Rows[i]["userId"].ToString();
                            value = dt_XFTCS.Rows[i]["epointsETHToTCS"].ToString();
                            sql_update_pl += " " + TCSgatewayNoSelectReturnUpdateSql("online", orderID, address, value);
                        }
                    }
                    if (dt_KTTCS.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt_KTTCS.Rows.Count; i++)
                        {
                            orderID = "A19018KTOCC_v1v_" + dt_KTTCS.Rows[i]["id"].ToString();
                            address = dt_KTTCS.Rows[i]["userId"].ToString();
                            value = dt_KTTCS.Rows[i]["TcsKt"].ToString();
                            sql_update_pl += " " + TCSgatewayNoSelectReturnUpdateSql("online", orderID, address, value);
                        }
                    }
                }

                if (ETHkg == "开")
                {
                    DataTable dt_XFETH = dao.GetList("select id,userId,epointsETH from CurrencyXf where isxfETH = 0 and isxfTCS = 0 ");
                    DataTable dt_KTETH = dao.GetList("select id from InvestRecordKt where isSend=0 and ethOrtcs='ETH' ");

                    if (dt_XFETH.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt_XFETH.Rows.Count; i++)
                        {
                            orderID = "A19018XFETH_v1v_" + dt_XFETH.Rows[i]["id"].ToString();
                            address = dt_XFETH.Rows[i]["userId"].ToString();
                            value = dt_XFETH.Rows[i]["epointsETH"].ToString();
                            sql_update_pl += " " + TCSgatewayNoSelectReturnUpdateSql("online", orderID, address, value);
                        }
                    }

                    if (dt_KTETH.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt_KTETH.Rows.Count; i++)
                        {
                            orderID = "A19018KTETH_v1v_" + dt_KTETH.Rows[i]["id"].ToString();
                            OCCgateway("online", orderID);
                        }
                    }


                }
                sql_update_pl += " update Currency set ff=1 where addDate in (select addDate from CurrencyXf where isxfETH=1) and uid in (select uid from CurrencyXf where isxfETH=1) and ff=0;";
                if (sql_update_pl != "")
                    dao.ExecuteBySql(sql_update_pl);
            }
            else
            {
                throw new ValidateException("ETH和OCC的下发开关都未打开！请在参数设置打开要下发的开关");
            }
        }

        public int SaveInvestRecord(InvestRecord model)
        {
            if (model == null) { throw new ValidateException("保存内容为空"); }
            if (dao.GetCount("select count(1) from InvestRecord where HASH = @HASH", ParamUtil.Get().Add(new DbParameterItem("HASH", ConstUtil.EQ, model.HASH)).Result(), false) > 0) { throw new ValidateException("HASH 已存在不能插入重复记录"); }

            //不存在会员则新建会员
            Member myMem = memberBLL.GetModelByUserId(model.fromAddress);
            if (myMem == null)
            {
                Member pocMem = new Member();
                pocMem.userId = model.fromAddress;
                pocMem.addTime = DateTime.Now;
                pocMem.reId = 1;
                pocMem.reName = "system";
                pocMem.reuserName = "system";
                myMem = memberBLL.SaveMember(pocMem);
            }
            BaseSet set = setBLL.GetModel();
            model.uid = myMem.id.Value;
            model.userId = myMem.userId;
            model.userName = myMem.userName;
            model.gzxhqs = set.gzxhqs.Value;
            model.ethTotcsPrice = set.ethTotcsPrice.Value;
            model.isAcitve = 1;

            int investCount = dao.GetCount("select count(1) from InvestRecord where uid = @uid", ParamUtil.Get().Add(new DbParameterItem("uid", ConstUtil.EQ, model.uid)).Result(), false);
            //1、	没绑定推荐人的会员所转入的投资 记录； 

            //3、	结束共振后，仍转入ETH投资；
            if (myMem.reId.Value == 1 || myMem.reId == null) { model.isAcitve = 0; }

            Dictionary<string, ParameterSet> ps = psBLL.GetDictionaryByCodes("upUlevel1Dy", "upUlevel2Dy", "upUlevel3Dy", "upUlevel4Dy", "upUlevel5Dy", "upUlevel6Dy", "upUlevel1Xy", "upUlevel2Xy", "upUlevel3Xy", "upUlevel4Xy", "upUlevel5Xy", "InvestTzBili", "gzxhqs", "zzzInvestMin");
            double upUlevel1Dy = Convert.ToDouble(ps["upUlevel1Dy"].paramValue);
            double upUlevel2Dy = Convert.ToDouble(ps["upUlevel2Dy"].paramValue);
            double upUlevel3Dy = Convert.ToDouble(ps["upUlevel3Dy"].paramValue);
            double upUlevel4Dy = Convert.ToDouble(ps["upUlevel4Dy"].paramValue);
            double upUlevel5Dy = Convert.ToDouble(ps["upUlevel5Dy"].paramValue);
            double upUlevel6Dy = Convert.ToDouble(ps["upUlevel6Dy"].paramValue);
            double upUlevel1Xy = Convert.ToDouble(ps["upUlevel1Xy"].paramValue);
            double upUlevel2Xy = Convert.ToDouble(ps["upUlevel2Xy"].paramValue);
            double upUlevel3Xy = Convert.ToDouble(ps["upUlevel3Xy"].paramValue);
            double upUlevel4Xy = Convert.ToDouble(ps["upUlevel4Xy"].paramValue);
            double upUlevel5Xy = Convert.ToDouble(ps["upUlevel5Xy"].paramValue);
            double InvestTzBili = Convert.ToDouble(ps["InvestTzBili"].paramValue);
            double zzzInvestMin = Convert.ToDouble(ps["zzzInvestMin"].paramValue);
            int gzxhqs = int.Parse(ps["gzxhqs"].paramValue);
            if (model.ethValue.Value < upUlevel1Dy && investCount == 0)//第一笔投资需大于VIP1
            { model.isAcitve = 0; model.remark = "投资金额小于最低OCC1"; }
            if (model.gzxhqs.Value > gzxhqs) { model.isAcitve = 0; model.remark = "共振销毁期数已过"; }
            //double maxethValue = double.Parse(dao.GetList("SELECT isnull(MAX(ethValue),0) as maxethValue FROM InvestRecord where uid=" + current.id.Value).Rows[0]["maxethValue"].ToString());
            //if (model.ethValue.Value < maxethValue) { model.isAcitve = 0; model.remark = "投资额小于上一次最大投资额"; }
            //共建中的会员第二笔需大于0.1
            if (myMem.status == "共建中" && model.ethValue.Value < zzzInvestMin) { model.isAcitve = 0; model.remark = "共建中的会员第二笔需大于" + zzzInvestMin; }

            if (myMem.activeInvestDate == null) { myMem.activeInvestDate = DateTime.Parse("2019-10-28"); }

            //已停止投资金额必须大于上次激活到现在的投资总额
            if (myMem.status == "已停止" || myMem.status == "ETH退出" || myMem.status == "OCC退出")
            {

                object o = dao.ExecuteScalar("select isnull(SUM(ethValueTZ),0) as sumInvest FROM InvestRecord  where addTime>='" + myMem.activeInvestDate.Value.ToString("yyyy-MM-dd") + "' and uid=1 and isAcitve=1");
                double sumInvest = o == null ? 0 : Convert.ToDouble(o);
                if (model.ethValue.Value < sumInvest) { model.isAcitve = 0; model.remark = "小于上次激活到现在的投资总额" + sumInvest; }
            }
            if (model.isAcitve == 1)//级别确定
            {
                DateTime cal_activeInvestDate = myMem.activeInvestDate.Value;//未参与或共建中的按会员的此时间计算
                object o = dao.ExecuteScalar("SELECT isnull(sum(ethValue),0) as ethValueYx FROM InvestRecord where addTime>='" + cal_activeInvestDate.ToString("yyyy-MM-dd") + "' and isAcitve=1 and  uid=" + myMem.id.Value);
                double ethValueYx = o == null ? 0 : Convert.ToDouble(o);

                if (myMem.status != "共建中") ethValueYx = 0;//不是增值状态也即是无激活时间或此时间无效以前投资不算

                ethValueYx = ethValueYx + model.ethValue.Value;
                if (ethValueYx >= upUlevel1Dy && ethValueYx <= upUlevel1Xy)
                {
                    model.uLevel = 40;
                    model.uLevelName = "OCC1";
                }
                if (ethValueYx >= upUlevel2Dy && ethValueYx <= upUlevel2Xy)
                {
                    model.uLevel = 41;
                    model.uLevelName = "OCC2";
                }
                if (ethValueYx >= upUlevel3Dy && ethValueYx <= upUlevel3Xy)
                {
                    model.uLevel = 42;
                    model.uLevelName = "OCC3";
                }
                if (ethValueYx >= upUlevel4Dy && ethValueYx <= upUlevel4Xy)
                {
                    model.uLevel = 43;
                    model.uLevelName = "OCC4";
                }
                if (ethValueYx >= upUlevel5Dy && ethValueYx <= upUlevel5Xy)
                {
                    model.uLevel = 44;
                    model.uLevelName = "OCC5";
                }
                if (ethValueYx > upUlevel6Dy)
                {
                    model.uLevel = 87;
                    model.uLevelName = "合伙人";
                }
            }
            //投资本金比例：90%：作为投资本金，算奖金、业绩时，只用这90%来算，不是按全额算\
            model.ethValueTz = model.ethValue.Value * InvestTzBili / 100;
            model.ethValueXh = model.ethValue.Value - model.ethValueTz.Value;
            model.epointsETHToTCS = model.ethValueXh.Value * model.ethTotcsPrice.Value;
            model.isXh = 0;
            model.XhLog = "";
            DateTime now = DateTime.Now;
            //未参与状态变为共建中
            if (model.isAcitve == 1)
            {
                var isUpdate = false;
                //如果investCount==0证明没有投资记录，算是第一笔投资，必须大于等于vip1的最低值，否则就是无效，不能更新状态，但是能插入投资记录
                if (model.ethValue.Value >= upUlevel1Dy)
                {
                    isUpdate = true;
                }
                else
                {
                    //如果invest表中有大于等于vip1的最低值的数据，那么本次投资要验证是不是超过第二笔投资的数目
                    var investModel = dao.GetList("select * from InvestRecord where uid = @uid", ParamUtil.Get().Add(new DbParameterItem("uid", ConstUtil.EQ, model.uid)).Result(), false);
                    IList<InvestRecord> list = JsonConvert.DeserializeObject<IList<InvestRecord>>(JsonConvert.SerializeObject(investModel));
                    var isVip = list.Where(p => p.ethValue.Value >= upUlevel1Dy).Any();
                    if (isVip && model.ethValue.Value >= zzzInvestMin)
                    {
                        isUpdate = true;
                    }
                    else
                    {
                        model.isAcitve = 0;
                        isUpdate = false;
                    }
                }
                if (isUpdate)
                {
                    dao.ExecuteBySql("update Member set status='共建中',activeInvestDate=getdate() where status in ('未参与','已停止','ETH退出','OCC退出') and id=" + model.uid);
                }
            }
            model.addTime = now.AddSeconds(3);
            object b = dao.SaveByIdentity(model);
            //更新级别
            if (model.uLevel != null) { dao.ExecuteBySql("update Member set uLevel=" + model.uLevel + " where id=" + model.uid); }

            setBLL.CalGzxh();//共振销毁重新计算价格
            return Convert.ToInt32(b);
        }

        public int UpdateInvestRecord(InvestRecord model, Member current)
        {
            if (model == null || ValidateUtils.CheckIntZero(model.id)) { throw new ValidateException("保存内容为空"); }

            DateTime now = DateTime.Now;

            return dao.Update(model);
        }

        public InvestRecord GetModel(int id)
        {
            string sql = "select * from InvestRecord where id= " + id;
            return this.GetOne(sql);
        }

        public DataTable GetExcelListPageInvestYx(InvestRecord model)
        {

            string sql = "select OrderId,userName,fromAddress,ethValue,ethValueTz,epointsETHToTCS,status,addTime,HASH from InvestRecord m where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    param.Add(new DbParameterItem("m.uid", ConstUtil.EQ, model.uid));
                }
                if (!ValidateUtils.CheckIntZero(model.isAcitve))
                {
                    if (model.isAcitve == -1) model.isAcitve = 0;
                    param.Add(new DbParameterItem("m.isAcitve", ConstUtil.EQ, model.isAcitve));
                }
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.userName))
                {
                    param.Add(new DbParameterItem("m.userName", ConstUtil.LIKE, model.userName));
                }
                if (!ValidateUtils.CheckNull(model.fromAddress))
                {
                    param.Add(new DbParameterItem("m.fromAddress", ConstUtil.LIKE, model.fromAddress));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }

            DataTable dt = dao.GetList(sql, param, true);
            return dt;

        }
        public DataTable GetExcelListPageInvestSj(InvestRecord model)
        {

            string sql = @"select ir.userId,ir.userName,ir.addTime,ir.tzbjETH,ir.xhTCS,ir.addTime as sjTime,isnull(jj.all_epointsETH,0) as srETH,isnull(xf.all_epointsETH,0) as xfETH,isnull(jj.all_epointsETH,0)-isnull(xf.all_epointsETH,0) as szceETH,isnull(xf.all_epointsTCS,0) as xfTCS  from 
(
select uid,userId,userName,cast(addTime as date) as addTime,
sum(case when isAcitve = 1 then ethValueTz else 0 end) as tzbjETH,
sum(case when isAcitve = 1 then epointsETHToTCS  else 0 end) as xhTCS
from InvestRecord group by uid,userId,userName,cast(addTime as date)
) ir

left join 
(
select SUM(epoints) as all_epoints,SUM(epointsETH) as all_epointsETH,SUM(epointsETHToTCS) as all_epointsTCS,uid from Currency where 1=1 group by uid
) jj
on ir.uid=jj.uid

left join 
(
select SUM(epointsETH) as all_epointsETH,SUM(epointsETHToTCS) as all_epointsTCS, uid from CurrencyXf where 1=1 group by uid
) xf
on ir.uid=xf.uid where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("ir.userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.userName))
                {
                    param.Add(new DbParameterItem("ir.userName", ConstUtil.LIKE, model.userName));
                }

                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("ir.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("ir.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }

            DataTable dt = dao.GetList(sql, param, true);
            return dt;

        }

    }
}
