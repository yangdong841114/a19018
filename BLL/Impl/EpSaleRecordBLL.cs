﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class EpSaleRecordBLL : BaseBLL<EpSaleRecord>, IEpSaleRecordBLL
    {

        private System.Type type = typeof(EpSaleRecord);

        private System.Type statusType = typeof(EpSaleStatus);

        public IBaseDao dao { get; set; }

        public IParamSetBLL psBLL { get; set; }

        public IMemberAccountBLL accBLL { get; set; }

        public IMemberBLL mbBLL { get; set; }


        public override IBaseDao GetDao()
        {
            return dao;
        }

        public PageResult<EpSaleRecord> GetListPage(EpSaleRecord model)
        {
            PageResult<EpSaleRecord> page = new PageResult<EpSaleRecord>();
            string sql = "select *,row_number() over(order by m.id desc) rownumber from EpSaleRecord m where m.flag<4 ";
            string countSql = "select count(1) from EpSaleRecord m where m.flag<4 ";
            List<DbParameterItem> param = new List<DbParameterItem>();

            //查询条件
            if (model != null)
            {
                if (model.flagList != null && model.flagList.Count > 0)
                {
                    string str = "";
                    foreach (int flag in model.flagList)
                    {
                        str = str + flag + ",";
                    }
                    str = str.Substring(0, str.Length - 1);
                    str = "and flag in (" + str + ") ";
                    param.Add(new DbParameterItem(str, ConstUtil.STATIC_STR, null));
                }
                if (model.egtWaitNum != null)
                {
                    param.Add(new DbParameterItem("m.waitNum", ConstUtil.EGT, model.egtWaitNum));
                }
                if (model.uid != null)
                {
                    param.Add(new DbParameterItem("m.uid", ConstUtil.EQ, model.uid));
                }
                if (model.flag != null && model.flag >= 0)
                {
                    param.Add(new DbParameterItem("m.flag", ConstUtil.EQ, model.flag));
                }
                if (model.number != null)
                {
                    param.Add(new DbParameterItem("m.number", ConstUtil.LIKE, model.number));
                }
                if (model.userId != null)
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<EpSaleRecord> list = new List<EpSaleRecord>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((EpSaleRecord)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public DataTable GetExportList(EpSaleRecord model)
        {
            string sql = "select *,case when flag=0 then '挂卖中' when flag=1 then '部分售出' when flag=2 then '全部售出' when flag=3 then '已完成' else '已取消' end status," +
                         "row_number() over(order by m.id desc) rownumber from EpSaleRecord m where m.flag<4 ";
            List<DbParameterItem> param = new List<DbParameterItem>();

            //查询条件
            if (model != null)
            {
                if (model.flagList != null && model.flagList.Count > 0)
                {
                    string str = "";
                    foreach (int flag in model.flagList)
                    {
                        str = str + flag + ",";
                    }
                    str = str.Substring(0, str.Length - 1);
                    str = "and flag in (" + str + ") ";
                    param.Add(new DbParameterItem(str, ConstUtil.STATIC_STR, null));
                }
                if (model.egtWaitNum != null)
                {
                    param.Add(new DbParameterItem("m.waitNum", ConstUtil.EGT, model.egtWaitNum));
                }
                if (model.uid != null)
                {
                    param.Add(new DbParameterItem("m.uid", ConstUtil.EQ, model.uid));
                }
                if (model.flag != null && model.flag >= 0)
                {
                    param.Add(new DbParameterItem("m.flag", ConstUtil.EQ, model.flag));
                }
                if (model.number != null)
                {
                    param.Add(new DbParameterItem("m.number", ConstUtil.LIKE, model.number));
                }
                if (model.userId != null)
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            DataTable dt = dao.GetList(sql, param, true);
            return dt;
        }


        public PageResult<EpSaleStatus> GetSatusListPage(EpSaleStatus model)
        {
            PageResult<EpSaleStatus> page = new PageResult<EpSaleStatus>();
            string sql = "select *,row_number() over(order by m.id desc) rownumber from EpSaleStatus m where 1=1 ";
            string countSql = "select count(1) from EpSaleStatus m where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (model.sid != null)
                {
                    param.Add(new DbParameterItem("m.sid", ConstUtil.EQ, model.sid));
                }
                if (model.uid != null)
                {
                    param.Add(new DbParameterItem("m.uid", ConstUtil.EQ, model.uid));
                }
                if (model.number != null)
                {
                    param.Add(new DbParameterItem("m.number", ConstUtil.LIKE, model.number));
                }
                if (model.userId != null)
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<EpSaleStatus> list = new List<EpSaleStatus>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((EpSaleStatus)ReflectionUtil.GetModel(statusType, row));
                }
            }
            page.rows = list;
            return page;
        }



        public int SaveRecord(EpSaleRecord model, Member current)
        {
            if (model == null) { throw new ValidateException("操作对象为空"); }
            if (model.phone == null) { throw new ValidateException("请录入手机号"); }
            if (model.qq == null) { throw new ValidateException("请录入QQ号"); }
            if (ValidateUtils.CheckDoubleZero(model.saleNum)) { throw new ValidateException("请录入挂卖数量"); }
            if (model.saleNum < 0) { throw new ValidateException("挂卖数量不能为负数"); }
            if (model.uid == null) { throw new ValidateException("挂卖人不能为空"); }

            //挂卖人信息
            Member mm = mbBLL.GetModelById(model.uid.Value);
            if (mm == null) { throw new ValidateException("挂卖人不存在"); }

            //ep交易手续费
            Dictionary<string, ParameterSet> pp = psBLL.GetDictionaryByCodes("epsxf", "epSaleBei");
            double bei = Convert.ToDouble(pp["epSaleBei"].paramValue); //ep需是x的倍数
            double bili = Convert.ToDouble(pp["epsxf"].paramValue); //ep手续费比例
            double fee = model.saleNum.Value * bili / 100;

            if (model.saleNum % bei != 0) { throw new ValidateException("挂卖数量需是" + bei + "的倍数"); }

            //挂卖人账户
            MemberAccount acc = accBLL.GetModel(model.uid.Value);
            if (acc.agentJj < (model.saleNum + fee)) { throw new ValidateException("您的奖金分余额不足，需要挂卖金额：" + model.saleNum + "，手续费：" + fee); }

            //保存挂卖记录
            DateTime now = DateTime.Now;
            model.scNum = 0;
            model.waitNum = model.saleNum;
            model.fee = fee;
            model.feeBili = bili;
            model.number = OrderNumberUtils.GetRandomNumber("ES");
            model.addTime = now;
            model.flag = 0;
            model.userId = mm.userId;
            model.userName = mm.userName;
            int newId = Convert.ToInt32(dao.SaveByIdentity(model));

            //扣除账户
            dao.ExecuteBySql("update MemberAccount set agentJj=agentJJ-" + (model.saleNum + fee) + " where id=" + mm.id);

            //记录流水
            LiuShuiZhang liu = new LiuShuiZhang();
            liu.uid = model.uid;
            liu.userId = model.userId;
            liu.accountId = ConstUtil.JOURNAL_JJB;
            liu.abst = "EP挂卖，单号：" + model.number + ",扣除挂卖金额：" + model.saleNum + "，手续费：" + fee;
            liu.addtime = now;
            liu.sourceId = newId;
            liu.income = 0;
            liu.outlay = model.saleNum + fee;
            liu.last = acc.agentJj - liu.outlay;
            liu.tableName = "EpSaleRecord";
            liu.addUid = current.id;
            liu.addUser = current.userId;
            dao.Save(liu);

            //记录状态记录
            EpSaleStatus status = new EpSaleStatus();
            status.sid = newId;
            status.number = model.number;
            status.flag = 0;
            status.opStatus = "提交挂卖信息";
            status.uid = current.id;
            status.userId = current.userId;
            status.addTime = now;
            dao.Save(status);

            return newId;
        }


        public int SaveCancel(int id, Member current)
        {
            DataRow row = dao.GetOne("select * from EpSaleRecord where id=" + id);
            if (row == null) { throw new ValidateException("操作的记录不存在"); }
            EpSaleRecord dto = (EpSaleRecord)ReflectionUtil.GetModel(type, row);
            if (dto.waitNum <= 0 || (dto.flag != 0 && dto.flag != 1)) { throw new ValidateException("待售数量为0，不能取消"); }
            //有未完成的购买记录（购买当前挂卖记录的）不能取消
            int ct = dao.GetCount("select count(1) from EpBuyRecord where sid=" + dto.id + " and flag not in (2,3)");
            if (ct > 0) { throw new ValidateException("该挂卖记录还有未完成的购买记录，不能取消"); }

            dto.flag = 4;
            dao.Update(dto);

            //挂卖人账户
            MemberAccount acc = accBLL.GetModel(dto.uid.Value);
            DateTime now = DateTime.Now;

            //记录状态记录
            EpSaleStatus status = new EpSaleStatus();
            status.sid = dto.id;
            status.number = dto.number;
            status.flag = 4;
            status.opStatus = "取消挂卖信息";
            status.uid = current.id;
            status.userId = current.userId;
            status.addTime = now;
            dao.Save(status);

            //退回奖金分给用户
            double fee = dto.waitNum.Value * dto.feeBili.Value / 100;//应退回的手续费
            dao.ExecuteBySql("update MemberAccount set agentJj=agentJJ+" + (fee + dto.waitNum) + " where id=" + dto.uid);

            //记录流水
            LiuShuiZhang liu = new LiuShuiZhang();
            liu.uid = dto.uid;
            liu.userId = dto.userId;
            liu.accountId = ConstUtil.JOURNAL_JJB;
            liu.abst = "取消EP挂卖，单号：" + dto.number + ",退回挂卖金额：" + dto.waitNum + "，手续费：" + fee;
            liu.addtime = now;
            liu.sourceId = dto.id;
            liu.outlay = 0;
            liu.income = dto.waitNum + fee;
            liu.last = acc.agentJj + liu.income;
            liu.tableName = "EpSaleRecord";
            liu.addUid = current.id;
            liu.addUser = current.userId;
            dao.Save(liu);

            return 1;
        }

    }
}