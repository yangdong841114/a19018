﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class InvestRecordKtBLL : BaseBLL<InvestRecordKt>, IInvestRecordKtBLL
    {

        private System.Type type = typeof(InvestRecordKt);
        public IBaseDao dao { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new InvestRecordKt GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param, true);
            if (row == null) return null;
            InvestRecordKt mb = (InvestRecordKt)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new InvestRecordKt GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            InvestRecordKt mb = (InvestRecordKt)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<InvestRecordKt> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<InvestRecordKt> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<InvestRecordKt>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((InvestRecordKt)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<InvestRecordKt> GetList(string sql)
        {
            List<InvestRecordKt> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<InvestRecordKt>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((InvestRecordKt)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public PageResult<InvestRecordKt> GetListPage(InvestRecordKt model)
        {
            PageResult<InvestRecordKt> page = new PageResult<InvestRecordKt>();
            string sql = "select m.*,case when m.ethOrtcs ='ETH' then ethValueKb else TcsKt end as je ,mem.userName,mem.uLevel,mem.addTime as regaddTime,row_number() over(order by m.id desc) rownumber from InvestRecordKt m,Member mem,v_Invest vi where m.uid=mem.id and m.uid=vi.uid ";
            string countSql = "select count(1) from  InvestRecordKt m,Member mem,v_Invest vi where m.uid=mem.id and m.uid=vi.uid ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    param.Add(new DbParameterItem("m.uid", ConstUtil.EQ, model.uid));
                }
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.userName))
                {
                    param.Add(new DbParameterItem("mem.userName", ConstUtil.LIKE, model.userName));
                }
                if (!ValidateUtils.CheckNull(model.ethOrtcs) && model.ethOrtcs != "全部")
                {
                    param.Add(new DbParameterItem("m.ethOrtcs", ConstUtil.LIKE, model.ethOrtcs));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<InvestRecordKt> list = new List<InvestRecordKt>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    InvestRecordKt model_row = (InvestRecordKt)ReflectionUtil.GetModel(type, row);
                    if (model_row.KtLog != null && model_row.KtLog.IndexOf("hash_is_txhash") != -1) model_row.HASH = model_row.KtLog.Substring(model_row.KtLog.IndexOf("hash_is_txhash") + 14).Replace("\\u0027", "").Replace("}}\",", "").Replace(":", "");
                    list.Add(model_row);
                }
            }
            page.rows = list;
            return page;
        }

        public System.Data.DataTable GetExcelListPageInvestKt(InvestRecordKt model)
        {

            string sql = @"select m.userId,m.ethValueTz,m.ethValueSy,m.ethOrtcs,m.status,m.addTime,m.getTime,m.KtLog,m.HASH,m.auditUser,
            case when m.ethOrtcs ='ETH' then ethValueKb else TcsKt end as je ,mem.userName,d.name as uLevelName,mem.addTime as regaddTime from InvestRecordKt m,Member mem,v_Invest vi,DataDictionary d where m.uid=mem.id and m.uid=vi.uid and mem.uLevel=d.id ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    param.Add(new DbParameterItem("m.uid", ConstUtil.EQ, model.uid));
                }
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.userName))
                {
                    param.Add(new DbParameterItem("mem.userName", ConstUtil.LIKE, model.userName));
                }
                if (!ValidateUtils.CheckNull(model.ethOrtcs) && model.ethOrtcs != "全部")
                {
                    param.Add(new DbParameterItem("m.ethOrtcs", ConstUtil.LIKE, model.ethOrtcs));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }

            DataTable dt = dao.GetList(sql, param, true);
            return dt;
        }

        public int SaveInvestRecordKt(InvestRecordKt model, Member current)
        {
            if (model == null) { throw new ValidateException("保存内容为空"); }
            model.HASH = "";
            DataTable dt_v_Invest = dao.GetList("select ethValueTz,ethValueTz_Wcj,SumCurrency,SumCurrencyETHWcj from v_Invest where uid=" + model.uid);
            model.ethValueTz = double.Parse(dt_v_Invest.Rows[0]["ethValueTz_Wcj"].ToString());
            model.ethValueSy = double.Parse(dt_v_Invest.Rows[0]["SumCurrencyETHWcj"].ToString());
            model.ethValueKb = model.ethValueTz.Value - model.ethValueSy.Value;
            if (model.ethValueKb.Value <= 0) { throw new ValidateException("您的收益已回本不能申请取消合约"); }
            double ethTotcsPrice = double.Parse(dao.GetList("SELECT ethTotcsPrice FROM BaseSet where 1=1").Rows[0]["ethTotcsPrice"].ToString());
            model.ethTotcsPrice = ethTotcsPrice;
            model.TcsKt = model.ethValueKb.Value * ethTotcsPrice;
            DataTable dt_Member = dao.GetList("select * from Member where id=" + model.uid);
            if (dt_Member.Rows[0]["status"].ToString() != "共建中") { throw new ValidateException("会员状态必须为-共建中"); }
            model.KtLog = "";
            //if (model.isSend != -1)
            //{
            //    model.isSend = 0;
            //    model.status = "已确认";
            //}
            //else
            //    model.status = "待审核";
            model.isSend = 0;
            model.status = "待审核";
            model.auditUid = current.id.Value;
            model.auditUser = current.userId;
            DateTime now = DateTime.Now;

            model.addTime = now;
            object o = dao.SaveByIdentity(model);
            dao.ExecuteBySql(@"INSERT INTO [dbo].[OutRecord]
           ([uid]
           ,[userId]
           ,[uLevel]
           ,[uLevelName]
           ,[ethValueTz_Wcj]
           ,[SumCurrencyWcj]
           ,[addTime]
           ,[userName])
         select m.id,m.userId,m.uLevel,'',vi.ethValueTz_Wcj,vi.SumCurrencyWcj,GETDATE(),m.userName from Member m,v_Invest vi where m.id=vi.uid and m.id=" + model.uid);
            dao.ExecuteBySql("update Member set uLevel=3, status='已停止',backETH=" + model.ethValueKb + " where id=" + model.uid);
            //if(model.ethOrtcs == "ETH")
            //    dao.ExecuteBySql("update Member set uLevel=3, status='ETH退出',backETH=" + model.ethValueKb + " where id=" + model.uid);
            //if (model.ethOrtcs == "TCS")
            //    dao.ExecuteBySql("update Member set uLevel=3, status='OCC退出',backTCS=" + model.TcsKt + " where id=" + model.uid);
            return Convert.ToInt32(o);
        }

        public int UpdateInvestRecordKt(InvestRecordKt model, Member current)
        {
            if (model == null || ValidateUtils.CheckIntZero(model.id)) { throw new ValidateException("保存内容为空"); }

            DateTime now = DateTime.Now;

            return dao.Update(model);
        }

        public InvestRecordKt GetModel(int id)
        {
            string sql = "select * from InvestRecordKt where id= " + id;
            return this.GetOne(sql);
        }

    }
}
