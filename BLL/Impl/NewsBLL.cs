﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class NewsBLL : INewsBLL
    {
        private System.Type type = typeof(News);
        public IBaseDao dao { get; set; }

        public News GetModelByTypeId(int typeId)
        {
            DataRow row = dao.GetOne("select * from News where typeId=" + typeId);
            if (row == null) return null;
            News model = (News)ReflectionUtil.GetModel(type, row);
            return model;
        }


        public News GetModelById(int id)
        {
            DataRow row = dao.GetOne("select * from News where id=" + id);
            if (row == null) return null;
            News model = (News)ReflectionUtil.GetModel(type, row);
            return model;
        }

        public PageResult<News> GetListPage(News model,string fields)
        {
            PageResult<News> page = new PageResult<News>();
            string sql = "select " + fields + ",row_number() over(order by topTime desc, m.id desc) rownumber from News m where 1=1 ";
            string countSql = "select count(1) from News where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (model.typeId != null)
                {
                    param.Add(new DbParameterItem("typeId", ConstUtil.EQ, model.typeId));
                }
                if (model.title!= null)
                {
                    param.Add(new DbParameterItem("title", ConstUtil.LIKE, model.title));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<News> list = new List<News>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((News)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public int Update(News model, Member login)
        {
            if (model == null) { throw new ValidateException("修改内容为空"); }
            else if (ValidateUtils.CheckNull(model.content)) { throw new ValidateException("修改内容为空"); }
            else if (ValidateUtils.CheckIntZero(model.typeId)) { throw new ValidateException("修改内容为空"); }
            model.editTime = DateTime.Now;
            model.editor = login.userId;
            int c = dao.Update(model);
            return c;
        }

        public int Save(News model, Member login)
        {
            if (model == null) { throw new ValidateException("修改内容为空"); }
            else if (ValidateUtils.CheckNull(model.content)) { throw new ValidateException("修改内容为空"); }
            else if (ValidateUtils.CheckIntZero(model.typeId)) { throw new ValidateException("修改内容为空"); }
            model.addTime = DateTime.Now;
            model.topTime = model.addTime;
            model.isTop = 1;
            model.author = login.userId;
            int c = dao.Save(model);
            return c;
        }

        public int Delete(int id)
        {
            return dao.Delte("News", id);
        }


        public int SaveOrUpdate(News model, Member login)
        {
            if (model == null) { throw new ValidateException("内容为空"); }
            else if (ValidateUtils.CheckNull(model.content)) { throw new ValidateException("内容为空"); }
            else if (ValidateUtils.CheckIntZero(model.typeId)) { throw new ValidateException("内容为空"); }
            News ne = GetModelByTypeId(model.typeId.Value);
            int c = 0;
            if (ne == null)
            {
                c = Save(model, login);
            }
            else
            {
                model.id = ne.id;
                c = Update(model, login);
            }
            return c;
        }

        public int SaveTop(int id)
        {
            string sql = "update News set topTime = getDate(),isTop=2 where id = " + id;
            return dao.ExecuteBySql(sql);
        }

        public int SaveCancelTop(int id)
        {
            string sql = "update News set topTime = addTime,isTop=1 where id = " + id;
            return dao.ExecuteBySql(sql);
        }

    }
}
