﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class CurrencyBLL : ICurrencyBLL
    {

        private System.Type type = typeof(CurrencySum);
        private System.Type dtype = typeof(Currency);
        private System.Type btype = typeof(Bobi);
        public IBaseDao dao { get; set; }

        public PageResult<CurrencySum> GetAllSumCurrency(CurrencySum model)
        {
            PageResult<CurrencySum> page = new PageResult<CurrencySum>();
            string sql = "select t.*,(cat1+cat2+cat3+cat4+cat5+cat6+cat7) yf,(cat1+cat2+cat3+cat4+cat5+cat6+cat7-epointsTCS) sf,(cat1+cat2+cat3+cat4+cat5+cat6+cat7-epointsTCS-fee1-fee2-fee3) getETH," +
                         " row_number() over(order by t.addDate desc) rownumber " +
                         " from( " +
                         " select addDate," +
                         " sum(case when cat = 45 then ePoints else 0 end) as cat1," +
                         " sum(case when cat = 46 then ePoints else 0 end) as cat2," +
                         " sum(case when cat = 47 then ePoints else 0 end) as cat3," +
                         " sum(case when cat = 48 then ePoints else 0 end) as cat4," +
                         " sum(case when cat = 49 then ePoints else 0 end) as cat5," +
                         " sum(case when cat = 50 then ePoints else 0 end) as cat6," +
                         " sum(case when cat = 51 then ePoints else 0 end) as cat7," +
                         " sum(epointsETHToTCS) as epointsETHToTCS," +
                         " sum(epointsTCS) as epointsTCS," +
                         " SUM(fee1) fee1,SUM(fee2) fee2,SUM(fee3) fee3 from Currency group by addDate " +
                         " ) t where 1=1 ";
            string countSql = "select count(1) " +
                         " from( " +
                         " select addDate," +
                         " sum(case when cat = 45 then ePoints else 0 end) as cat1," +
                         " sum(case when cat = 46 then ePoints else 0 end) as cat2," +
                         " sum(case when cat = 47 then ePoints else 0 end) as cat3," +
                         " sum(case when cat = 48 then ePoints else 0 end) as cat4," +
                         " sum(case when cat = 49 then ePoints else 0 end) as cat5," +
                         " sum(case when cat = 50 then ePoints else 0 end) as cat6," +
                         " sum(case when cat = 51 then ePoints else 0 end) as cat7," +
                         " SUM(fee1) fee1,SUM(fee2) fee2,SUM(fee3) fee3 from Currency group by addDate " +
                         " ) t where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (model.startTime != null)
                {
                    sql += " and t.addDate >= cast(@startTime as date) ";
                    countSql += " and t.addDate >= cast(@startTime as date) ";
                    param.Add(new DbParameterItem("startTime", null, model.startTime));
                }
                if (model.endTime != null)
                {
                    sql += " and t.addDate <= cast(@endTime as date) ";
                    countSql += " and t.addDate >= cast(@endTime as date) ";
                    param.Add(new DbParameterItem("endTime", null, model.endTime));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, false);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageListNotJoinSql(sql, param);
            List<CurrencySum> list = new List<CurrencySum>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((CurrencySum)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            //底部汇总
            page.footer = GetAllSumCurrencyFooter(model);
            return page;
        }


        public PageResult<CurrencySum> GetAllSumCurrencyXf(CurrencySum model)
        {
            PageResult<CurrencySum> page = new PageResult<CurrencySum>();
            string sql = "select t.*," +
                         " row_number() over(order by t.addDate desc) rownumber " +
                         " from( " +
                         " select * from SumCurrency) t where 1 = 1 ";
            string countSql = "select count(1) " +
                         " from( " +
                         " select * from SumCurrency) t where 1 = 1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (model.startTime != null)
                {
                    sql += " and t.addDate >= cast(@startTime as date) ";
                    countSql += " and t.addDate >= cast(@startTime as date) ";
                    param.Add(new DbParameterItem("startTime", null, model.startTime));
                }
                if (model.endTime != null)
                {
                    sql += " and t.addDate <= cast(@endTime as date) ";
                    countSql += " and t.addDate >= cast(@endTime as date) ";
                    param.Add(new DbParameterItem("endTime", null, model.endTime));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, false);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageListNotJoinSql(sql, param);
            List<CurrencySum> list = new List<CurrencySum>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((CurrencySum)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            //底部汇总
            page.footer = GetAllSumCurrencyFooterXf(model);
            return page;
        }

        /// <summary>
        /// 获取底部汇总数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private List<CurrencySum> GetAllSumCurrencyFooter(CurrencySum model)
        {
            string sql = "select t.*,(cat1+cat2+cat3+cat4+cat5+cat6+cat7) yf,(cat1+cat2+cat3+cat4+cat5+cat6+cat7-epointsTCS) sf,(cat1+cat2+cat3+cat4+cat5+cat6+cat7-epointsTCS-fee1-fee2-fee3) getETH " +
                         " from( select  " +
                         " sum(case when cat = 45 then ePoints else 0 end) as cat1,  " +
                         " sum(case when cat = 46 then ePoints else 0 end) as cat2,  " +
                         " sum(case when cat = 47 then ePoints else 0 end) as cat3,  " +
                         " sum(case when cat = 48 then ePoints else 0 end) as cat4,  " +
                         " sum(case when cat = 49 then ePoints else 0 end) as cat5,  " +
                         " sum(case when cat = 50 then ePoints else 0 end) as cat6,  " +
                         " sum(case when cat = 51 then ePoints else 0 end) as cat7,  " +
                         " sum(epointsETHToTCS) as epointsETHToTCS," +
                         " sum(epointsTCS) as epointsTCS," +
                         " SUM(fee1) fee1,SUM(fee2) fee2,SUM(fee3) fee3 from Currency where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (model.startTime != null)
                {
                    sql += " and addDate >= cast(@addDate as date) ";
                    param.Add(new DbParameterItem("addDate", null, model.startTime));
                }
                if (model.endTime != null)
                {
                    sql += " and addDate <= cast(@endTime as date) ";
                    param.Add(new DbParameterItem("endTime", null, model.endTime));
                }
            }
            sql += " ) t where 1=1 ";
            DataRow row = dao.GetOne(sql, param, false);
            if (row == null) return null;
            CurrencySum mb = (CurrencySum)ReflectionUtil.GetModel(type, row);
            List<CurrencySum> list = new List<CurrencySum>();
            list.Add(mb);
            return list;
        }

        private List<CurrencySum> GetAllSumCurrencyFooterXf(CurrencySum model)
        {
            string sql = "select t.* " +
                         " from( select  " +
                         " sum(case when isxfETH = 1 then epointsETH else 0 end) as XfzsYxfETH," +
                         " sum(case when isxfETH != 1 then epointsETH else 0 end) as XfzsDxfETH," +
                         " sum(case when isxfTCS = 1 then epointsETHToTCS else 0 end) as XfzsYxfTCS," +
                         " sum(case when isxfTCS != 1 then epointsETHToTCS else 0 end) as XfzsDxfTCS," +
                         " sum(epointsETHToTCS) as XfzsTCS," +
                         " sum(epointsTCS) as epointsTCS," +
                         " SUM(fee1) fee1,SUM(epointsETH) XfzsETH,SUM(fee3) fee3 from CurrencyXf where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (model.startTime != null)
                {
                    sql += " and addDate >= cast(@addDate as date) ";
                    param.Add(new DbParameterItem("addDate", null, model.startTime));
                }
                if (model.endTime != null)
                {
                    sql += " and addDate <= cast(@endTime as date) ";
                    param.Add(new DbParameterItem("endTime", null, model.endTime));
                }
            }
            sql += " ) t where 1=1 ";
            DataRow row = dao.GetOne(sql, param, false);
            if (row == null) return null;
            CurrencySum mb = (CurrencySum)ReflectionUtil.GetModel(type, row);
            List<CurrencySum> list = new List<CurrencySum>();
            list.Add(mb);
            return list;
        }

        public PageResult<CurrencySum> GetByUserSumCurrency(CurrencySum model)
        {
            PageResult<CurrencySum> page = new PageResult<CurrencySum>();
            string sql = "select t.*,(cat1+cat2+cat3+cat4+cat5+cat6+cat7) yf,(cat1+cat2+cat3+cat4+cat5+cat6+cat7-epointsTCS) sf,(epointsETH-fee1-fee2-fee3) getETH,m.userName," +
                         " row_number() over(order by t.addDate desc,t.uid desc) rownumber " +
                         " from( " +
                         " select addDate,userId,uid," +
                         " sum(case when cat = 45 then ePoints else 0 end) as cat1," +
                         " sum(case when cat = 46 then ePoints else 0 end) as cat2," +
                         " sum(case when cat = 47 then ePoints else 0 end) as cat3," +
                         " sum(case when cat = 48 then ePoints else 0 end) as cat4," +
                         " sum(case when cat = 49 then ePoints else 0 end) as cat5," +
                         " sum(case when cat = 50 then ePoints else 0 end) as cat6," +
                         " sum(case when cat = 51 then ePoints else 0 end) as cat7," +
                         " sum(epointsETHToTCS) as epointsETHToTCS," +
                         " sum(epointsTCS) as epointsTCS," +
                          " sum(epointsETH) as epointsETH," +
                         " SUM(fee1) fee1,SUM(fee2) fee2,SUM(fee3) fee3 from Currency group by addDate,userId,uid " +
                         " ) t inner join Member m on t.uid = m.id where 1=1 ";
            string countSql = "select count(1) " +
                         " from( " +
                         " select addDate,userId,uid," +
                         " sum(case when cat = 45 then ePoints else 0 end) as cat1," +
                         " sum(case when cat = 46 then ePoints else 0 end) as cat2," +
                         " sum(case when cat = 47 then ePoints else 0 end) as cat3," +
                         " sum(case when cat = 48 then ePoints else 0 end) as cat4," +
                         " sum(case when cat = 49 then ePoints else 0 end) as cat5," +
                         " sum(case when cat = 50 then ePoints else 0 end) as cat6," +
                         " sum(case when cat = 51 then ePoints else 0 end) as cat7," +
                         " SUM(fee1) fee1,SUM(fee2) fee2,SUM(fee3) fee3 from Currency group by addDate,userId,uid " +
                         " ) t inner join Member m on t.uid = m.id where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (model.startTime != null)
                {
                    sql += " and t.addDate >= cast(@startTime as date) ";
                    countSql += " and t.addDate >= cast(@startTime as date) ";
                    param.Add(new DbParameterItem("startTime", null, model.startTime));
                }
                if (model.endTime != null)
                {
                    sql += " and t.addDate <= cast(@endTime as date) ";
                    countSql += " and t.addDate >= cast(@endTime as date) ";
                    param.Add(new DbParameterItem("endTime", null, model.endTime));
                }
                if (model.addDate != null)
                {
                    sql += " and t.addDate = cast(@addDate as date) ";
                    countSql += " and t.addDate = cast(@addDate as date) ";
                    param.Add(new DbParameterItem("addDate", null, model.addDate));
                }
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    sql += " and t.userId like '%'+@userId+'%' ";
                    countSql += " and t.userId like '%'+@userId+'%' ";
                    param.Add(new DbParameterItem("userId", null, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.userName))
                {
                    sql += " and m.userName like '%'+@userName+'%' ";
                    countSql += " and m.userName like '%'+@userName+'%' ";
                    param.Add(new DbParameterItem("userName", null, model.userName));
                }
                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    sql += " and t.uid = @uid ";
                    countSql += " and t.uid = @uid ";
                    param.Add(new DbParameterItem("uid", null, model.uid));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, false);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageListNotJoinSql(sql, param);
            List<CurrencySum> list = new List<CurrencySum>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((CurrencySum)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            //底部汇总
            page.footer = GetByUserSumCurrencyFooter(model);
            return page;
        }

        /// <summary>
        /// 获取底部汇总数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<CurrencySum> GetByUserSumCurrencyFooter(CurrencySum model)
        {
            string sql = "select t.*,(cat1+cat2+cat3+cat4+cat5+cat6+cat7) yf,(cat1+cat2+cat3+cat4+cat5+cat6+cat7-epointsTCS) sf,(epointsETH-fee1-fee2-fee3) getETH " +
                         " from( select  " +
                         " sum(case when cat = 45 then ePoints else 0 end) as cat1,  " +
                         " sum(case when cat = 46 then ePoints else 0 end) as cat2,  " +
                         " sum(case when cat = 47 then ePoints else 0 end) as cat3,  " +
                         " sum(case when cat = 48 then ePoints else 0 end) as cat4,  " +
                         " sum(case when cat = 49 then ePoints else 0 end) as cat5,  " +
                         " sum(case when cat = 50 then ePoints else 0 end) as cat6,  " +
                         " sum(case when cat = 51 then ePoints else 0 end) as cat7,  " +
                         " sum(epointsETHToTCS) as epointsETHToTCS," +
                         " sum(epointsTCS) as epointsTCS," +
                          " sum(epointsETH) as epointsETH," +
                         " SUM(fee1) fee1,SUM(fee2) fee2,SUM(fee3) fee3 from Currency c inner join Member m on c.uid = m.id " +
                         " where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (model.startTime != null)
                {
                    sql += " and c.addDate >= cast(@startTime as date) ";
                    param.Add(new DbParameterItem("startTime", null, model.startTime));
                }
                if (model.endTime != null)
                {
                    sql += " and c.addDate <= cast(@endTime as date) ";
                    param.Add(new DbParameterItem("endTime", null, model.endTime));
                }
                if (model.addDate != null)
                {
                    sql += " and c.addDate = cast(@addDate as date) ";
                    param.Add(new DbParameterItem("addDate", null, model.addDate));
                }
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    sql += " and c.userId like '%'+@userId+'%' ";
                    param.Add(new DbParameterItem("userId", null, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.userName))
                {
                    sql += " and m.userName like '%'+@userName+'%' ";
                    param.Add(new DbParameterItem("userName", null, model.userName));
                }
                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    sql += " and c.uid = @uid ";
                    param.Add(new DbParameterItem("uid", null, model.uid));
                }
            }
            sql += " ) t where 1=1 ";
            DataRow row = dao.GetOne(sql, param, false);
            if (row == null) return null;
            CurrencySum mb = (CurrencySum)ReflectionUtil.GetModel(type, row);
            List<CurrencySum> list = new List<CurrencySum>();
            list.Add(mb);
            return list;
        }

        public PageResult<Currency> GetDetailListPage(Currency model)
        {
            PageResult<Currency> page = new PageResult<Currency>();
            string sql = "select c.cat,c.userId,d.name catName,c.epoints yf,c.epointsETHToTCS,c.epointsTCS,(c.epoints-c.epointsTCS) sf,(c.epoints-c.epointsTCS-c.fee1-c.fee2-c.fee3) getETH,c.fee1,c.fee2,c.fee3,c.jstime,c.ff,c.mulx, " +
                         "row_number() over(order by c.jstime desc) rownumber from Currency c inner join DataDictionary d on c.cat = d.id where 1=1 ";
            string countSql = "select count(1) from Currency c where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    sql += " and c.userId =@userId";
                    countSql += " and c.userId =@userId";
                    param.Add(new DbParameterItem("userId", null, model.userId));
                }
                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    sql += " and c.uid = @uid ";
                    countSql += " and c.uid = @uid ";
                    param.Add(new DbParameterItem("uid", null, model.uid));
                }
                if (!ValidateUtils.CheckIntZero(model.cat))
                {
                    sql += " and c.cat = @cat ";
                    countSql += " and c.cat = @cat ";
                    param.Add(new DbParameterItem("cat", null, model.cat));
                }
                if (model.addDate != null)
                {
                    sql += " and c.addDate = cast(@addDate as date) ";
                    countSql += " and c.addDate = cast(@addDate as date) ";
                    param.Add(new DbParameterItem("addDate", null, model.addDate));
                }

            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, false);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageListNotJoinSql(sql, param);
            List<Currency> list = new List<Currency>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Currency)ReflectionUtil.GetModel(dtype, row));
                }
            }
            page.rows = list;
            return page;
        }

        public PageResult<Currency> GetDetailListPageXf(Currency model)
        {
            PageResult<Currency> page = new PageResult<Currency>();
            string sql = "select c.id,c.userId,d.userName,c.epointsETH,c.isxfETH,c.XfLogETH,c.epointsETHToTCS,c.isxfTCS,c.XfLogTCS,c.jstime,c.fftime,c.ff,c.mulx,c.addDate,  " +
                         "row_number() over(order by c.jstime desc) rownumber from CurrencyXf c inner join Member d on c.uid = d.id where 1=1 ";
            string countSql = "select count(1) from CurrencyXf c inner join Member d on c.uid = d.id where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    if (Common.SqlChecker.CheckKeyWord(model.userId) != "") model.userId = "";
                    sql += " and c.userId  like '%" + model.userId + "%'";
                    countSql += " and c.userId  like '%" + model.userId + "%'";

                }
                if (!ValidateUtils.CheckNull(model.userName))
                {
                    if (Common.SqlChecker.CheckKeyWord(model.userName) != "") model.userName = "";
                    sql += " and d.userName like '%" + model.userName + "%'";
                    countSql += " and d.userName like '%" + model.userName + "%'";

                }
                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    sql += " and c.uid = @uid ";
                    countSql += " and c.uid = @uid ";
                    param.Add(new DbParameterItem("uid", null, model.uid));
                }

                if (model.addDate != null)
                {
                    sql += " and c.addDate = cast(@addDate as date) ";
                    countSql += " and c.addDate = cast(@addDate as date) ";
                    param.Add(new DbParameterItem("addDate", null, model.addDate));
                }
                if (model.startTime != null)
                {
                    sql += " and c.addDate >= cast('" + model.startTime.Value.ToString("yyyy-MM-dd") + "' as date) ";
                    countSql += " and c.addDate >= cast('" + model.startTime.Value.ToString("yyyy-MM-dd") + "' as date) ";
                }
                if (model.endTime != null)
                {
                    sql += " and c.addDate <= cast('" + model.endTime.Value.ToString("yyyy-MM-dd") + "' as date) ";
                    countSql += " and c.addDate <= cast('" + model.endTime.Value.ToString("yyyy-MM-dd") + "' as date) ";
                }
                if (model.isxfETH != null && model.isxfETH > -1)
                {
                    sql += " and c.isxfETH = " + model.isxfETH;
                    countSql += " and c.isxfETH =" + model.isxfETH;
                }


            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, false);
            List<DbParameterItem> param_nopage = param;

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageListNotJoinSql(sql, param);
            List<Currency> list = new List<Currency>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    Currency model_row = (Currency)ReflectionUtil.GetModel(dtype, row);
                    try
                    {
                        if (model_row.XfLogETH.IndexOf("hash_is_txhash") != -1)
                        {
                            model_row.HASHETH = model_row.XfLogETH.Substring(model_row.XfLogETH.IndexOf("hash_is_txhash") + 14).Replace("\\u0027", "").Replace("}}\",", "").Replace(":", "");
                            model_row.isxfETH = 3;//成功下发
                        }
                        else if (model_row.XfLogETH.IndexOf("hash_is_") != -1)
                        {
                            model_row.HASHETH = System.Text.RegularExpressions.Regex.Split(model_row.XfLogETH, "hash_is_")[1];
                            model_row.isxfETH = 3;
                        }
                        else if (model_row.XfLogETH.IndexOf("hash_is:") != -1)
                        {
                            model_row.HASHETH = System.Text.RegularExpressions.Regex.Split(model_row.XfLogETH, "hash_is:")[1];
                            model_row.isxfETH = 3;
                        }

                    }
                    catch { }
                    list.Add(model_row);
                }
            }
            page.rows = list;

            //汇总已审未审
            dt = dao.GetList(sql, param_nopage, true);
            double all_epointsETH = 0;
            double all_epointsETHToTCS = 0;
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    Currency model_row = (Currency)ReflectionUtil.GetModel(dtype, row);
                    all_epointsETH += model_row.epointsETH.Value;
                    all_epointsETHToTCS += model_row.epointsETHToTCS.Value;
                }
            }
            Currency foot_model = new Currency();
            list = new List<Currency>();
            foot_model.userId = "合计";
            foot_model.epointsETH = all_epointsETH;
            foot_model.epointsETHToTCS = all_epointsETHToTCS;
            foot_model.HASHETH = "";
            foot_model.HASHTCS = "";
            list.Add(foot_model);
            page.footer = list;

            return page;
        }

        public int AuditXFETH(int id, Member current)
        {
            DataTable dt = dao.GetList("select * from CurrencyXf where id=" + id);
            if (dt.Rows.Count == 0) { throw new ValidateException("未找到记录"); }
            if (dt.Rows[0]["isxfETH"].ToString() == "0" || dt.Rows[0]["isxfETH"].ToString() == "1") { throw new ValidateException("状态不是待审核"); }
            dao.ExecuteBySql("update CurrencyXf set isxfETH=0,mulx=mulx+'ETH审核人：" + current.userId + "' where id=" + id);
            return 1;

        }


        public int AuditXFETH_KT(int id, Member current)
        {
            DataTable dt = dao.GetList("select * from InvestRecordKt where id=" + id);
            if (dt.Rows.Count == 0) { throw new ValidateException("未找到记录"); }
            if (dt.Rows[0]["isSend"].ToString() != "-1") { throw new ValidateException("状态不是待审核"); }
            string sql = "select  'ETH' as [type], 'A19018KTETH_v1v_' + cast(id as varchar(10)) as orderId, userId as [address], ethValueKb as [value] from InvestRecordKt where id = " + id;
            var model = dao.GetList(sql);

            dao.ExecuteBySql("update InvestRecordKt set status='已确认', isSend=0,auditUser='ETH审核人：" + current.userId + "' where id=" + id);
            return 1;

        }

        public int AuditXFOCC(int id, Member current)
        {
            DataTable dt = dao.GetList("select * from CurrencyXf where id=" + id);
            if (dt.Rows.Count == 0) { throw new ValidateException("未找到记录"); }
            if (dt.Rows[0]["isxfTCS"].ToString() != "-1") { throw new ValidateException("状态不是待审核"); }
            dao.ExecuteBySql("update CurrencyXf set isxfTCS=0,mulx=mulx+'OCC审核人：" + current.userId + "' where id=" + id);
            return 1;

        }



        public PageResult<Bobi> GetBoBiListPage(Bobi model)
        {
            PageResult<Bobi> page = new PageResult<Bobi>();
            string sql = "select b.jstime,b.income,b.outlay,round(b.profit,2) profit,cast(round(rate*100,2) as varchar(20))+'%' as bili,rate,row_number() over(order by b.jstime desc) rownumber " +
                         " from bobi b where (income>0 or outlay>0) ";
            string countSql = "select count(1) from bobi b where (income>0 or outlay>0) ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询记录条数
            page.total = dao.GetCount(countSql, param, false);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageListNotJoinSql(sql, param);
            List<Bobi> list = new List<Bobi>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Bobi)ReflectionUtil.GetModel(btype, row));
                }
            }
            page.rows = list;
            return page;
        }


        public Bobi GetTotalBobi()
        {
            string sql = "SELECT income,outlay,profit,case when outlay=0 then '0' else cast(round((outlay/income)*100,2) as varchar(20))+'%' end bili from " +
                         " (select SUM(income) income,SUM(outlay) outlay,SUM(profit) profit from bobi ) t ";
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            Bobi b = (Bobi)ReflectionUtil.GetModel(btype, row);
            return b;
        }

        public DataTable GetBobiExcelList()
        {
            string sql = "select b.jstime,b.income,b.outlay,round(b.profit,2) profit,cast(round(rate*100,2) as varchar(20))+'%' as bili" +
                         " from bobi b where (income>0 or outlay>0) ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            //if (type != null)
            //{
            //    if (!ValidateUtils.CheckNull(type.ToString()))
            //    {
            //        param.Add(new DbParameterItem("isPay", ConstUtil.EQ, type));
            //    }
            //}
            DataTable dt = dao.GetList(sql, param, true);
            return dt;
        }

        public DataTable GetBounsExcel()
        {
            string sql = "select t.*,(cat1+cat2+cat3) yf,(cat1+cat2+cat3-epointsTCS) sf,(cat1+cat2+cat3-epointsTCS-fee1) getETH" +
                         " from( " +
                         " select addDate," +
                         " sum(case when cat = 45 then ePoints else 0 end) as cat1," +
                         " sum(case when cat = 46 then ePoints else 0 end) as cat2," +
                         " sum(case when cat = 47 then ePoints else 0 end) as cat3," +
                         " sum(epointsETHToTCS) as epointsETHToTCS," +
                         " sum(epointsTCS) as epointsTCS," +
                         " SUM(fee1) fee1 from Currency group by addDate " +
                         " ) t where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            //if (type != null)
            //{
            //    if (!ValidateUtils.CheckNull(type.ToString()))
            //    {
            //        param.Add(new DbParameterItem("isPay", ConstUtil.EQ, type));
            //    }
            //}
            DataTable dt = dao.GetList(sql, param, true);
            return dt;
        }

        public DataTable GetBounsExcelXf()
        {
            string sql = "select t.*" +
                         " from( " +
                          " select addDate," +
                         " sum(case when isxfETH = 1 then epointsETH else 0 end) as XfzsYxfETH," +
                         " sum(case when isxfETH != 1 then epointsETH else 0 end) as XfzsDxfETH," +
                         " sum(case when isxfTCS = 1 then epointsETHToTCS else 0 end) as XfzsYxfTCS," +
                         " sum(case when isxfTCS != 1 then epointsETHToTCS else 0 end) as XfzsDxfTCS," +
                         " sum(epointsETHToTCS) as XfzsTCS," +
                         " SUM(epointsETH) XfzsETH from CurrencyXf group by addDate " +
                         " ) t where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            //if (type != null)
            //{
            //    if (!ValidateUtils.CheckNull(type.ToString()))
            //    {
            //        param.Add(new DbParameterItem("isPay", ConstUtil.EQ, type));
            //    }
            //}
            DataTable dt = dao.GetList(sql, param, true);
            return dt;
        }

        public DataTable GetBounsDetailExcel(CurrencySum model)
        {
            string sql = "select t.userId,m.userName,t.addDate,t.cat1,t.cat2,t.cat3,t.epointsETHToTCS,t.epointsTCS,t.fee1,(cat1+cat2+cat3) yf,(cat1+cat2+cat3-epointsTCS) sf,(cat1+cat2+cat3-epointsTCS-fee1) getETH " +
                         " from( " +
                         " select addDate,userId,uid," +
                         " sum(case when cat = 45 then ePoints else 0 end) as cat1," +
                         " sum(case when cat = 46 then ePoints else 0 end) as cat2," +
                         " sum(case when cat = 47 then ePoints else 0 end) as cat3," +
                         " sum(epointsETHToTCS) as epointsETHToTCS," +
                         " sum(epointsTCS) as epointsTCS," +
                         " SUM(fee1) fee1 from Currency group by addDate,userId,uid " +
                         " ) t inner join Member m on t.uid = m.id where 1=1 ";
            //查询条件
            if (model != null)
            {
                if (model.addDate != null)
                {
                    sql += " and t.addDate = cast('" + model.addDate.Value.ToString("yyyy-MM-dd") + "' as date) ";
                }
            }
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            //if (type != null)
            //{
            //    if (!ValidateUtils.CheckNull(type.ToString()))
            //    {
            //        param.Add(new DbParameterItem("isPay", ConstUtil.EQ, type));
            //    }
            //}
            DataTable dt = dao.GetList(sql, param, true);
            return dt;
        }

        public DataTable GetBounsDetailExcelXf(Currency model)
        {

            string sql = "select c.userId,d.userName,c.epointsETH,case when c.isxfETH=-1 then '未审核' when c.isxfETH=0 then '未下发' else '已下发' end  isxfETH,c.XfLogETH,c.epointsETHToTCS,case when c.isxfTCS=-1 then '未审核' when c.isxfTCS=0 then '未下发' else '已下发' end  isxfTCS,c.XfLogTCS,c.addDate " +
                         " from CurrencyXf c inner join Member d on c.uid = d.id where 1=1 ";
            string countSql = "select count(1) from CurrencyXf c where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    sql += " and c.userId =@userId";
                    countSql += " and c.userId =@userId";
                    param.Add(new DbParameterItem("userId", null, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.userName))
                {
                    sql += " and d.userName =@userName";
                    countSql += " and d.userName =@userName";
                    param.Add(new DbParameterItem("userName", null, model.userName));
                }
                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    sql += " and c.uid = @uid ";
                    countSql += " and c.uid = @uid ";
                    param.Add(new DbParameterItem("uid", null, model.uid));
                }

                if (model.addDate != null)
                {
                    sql += " and c.addDate = cast(@addDate as date) ";
                    countSql += " and c.addDate = cast(@addDate as date) ";
                    param.Add(new DbParameterItem("addDate", null, model.addDate));
                }
                if (model.startTime != null)
                {
                    sql += " and c.addDate >= cast('" + model.startTime.Value.ToString("yyyy-MM-dd") + "' as date) ";
                    countSql += " and c.addDate >= cast('" + model.startTime.Value.ToString("yyyy-MM-dd") + "' as date) ";
                }
                if (model.endTime != null)
                {
                    sql += " and c.addDate <= cast('" + model.endTime.Value.ToString("yyyy-MM-dd") + "' as date) ";
                    countSql += " and c.addDate <= cast('" + model.endTime.Value.ToString("yyyy-MM-dd") + "' as date) ";
                }

            }
            DataTable dt = dao.GetList(sql, param, true);
            return dt;
        }

        public DataTable GetUserBonusDetailExcel(CurrencySum model)
        {
            string sql = "select c.userId,d.name catName,c.epoints yf,c.epointsTCS,c.epointsETHToTCS,(c.epoints-epointsTCS) sf,(c.epoints-c.fee1-c.epointsTCS) getETH,c.fee1,c.jstime,c.mulx " +
                         " from Currency c inner join DataDictionary d on c.cat = d.id where 1=1 ";
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    sql += " and c.uid =" + model.uid + "";
                }
                if (model.addDate != null)
                {
                    sql += " and c.addDate = cast('" + model.addDate.Value.ToString("yyyy-MM-dd") + "' as date) ";
                }

            }
            List<DbParameterItem> param = new List<DbParameterItem>();
            DataTable dt = dao.GetList(sql, param, true);
            return dt;
        }
    }
}
