﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace BLL.Impl
{
    public class MemberBLL : BaseBLL<Member>, IMemberBLL
    {
        private string allField = "[id],[userId],[userName],[nickName],[phone],[address],[email],[code]" +
                                  ",[qq],[province],[city],[area],[isLock],[isAgent],[agentIslock],[agentName],[applyAgentTime]" +
                                  ",[openAgentTime],[regAgentmoney],[shopid],[shopName],[uLevel],[rLevel],[treePlace]" +
                                  ",[fatherID],[fatherName],[pLevel],[pPath],[spare0],[spare1],[encash0],[encash1]" +
                                  ",[new0],[new1],[reId],[reName],[rePath],[reLevel],[reCount],[reTreeCount],[regMoney]" +
                                  ",[regNum],[isPay],[addTime],[passTime],[byopenId],[byopen],[empty],bankName,bankCard,bankAddress,bankUser";
        private System.Type type = typeof(Member);
        public IBaseDao dao { get; set; }

        public INewsBLL newsBLL { get; set; }

        public IParamSetBLL psBLL { get; set; }

        public ILoginHistoryBLL lhBll { get; set; }

        public IMemberAccountBLL accountBLL { get; set; }

        public IOrderBLL orderBLL { get; set; }

        public IMobileNoticeBLL noticeBLL { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new Member GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param, true);
            if (row == null) return null;
            Member mb = (Member)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new Member GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            Member mb = (Member)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public List<Member> GetMemberList(string sql, List<Common.DbParameterItem> param, bool isJoinSql)
        {
            List<Member> list = null;
            DataTable dt = dao.GetList(sql, param, isJoinSql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<Member>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Member)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<Member> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<Member> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<Member>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Member)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<Member> GetList(string sql)
        {
            List<Member> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<Member>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Member)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public int DeleteById(int id)
        {
            //调用存储过程
            string pro = "delMember";
            IDataParameter[] p = new IDataParameter[]{
                  new SqlParameter("@uid",SqlDbType.Int,4),
                  new SqlParameter("@return",SqlDbType.VarChar,200)
            };
            p[0].Value = id;
            p[1].Direction = ParameterDirection.Output;
            string result = dao.RunProceOnlyPara(pro, p, "return");
            if (result == "success")
            {
                return 1;
            }
            else
            {
                throw new ValidateException(result);
            }
        }

        public string OpenMember(int uid, int flag, Member current)
        {
            //调用存储过程
            string pro = "openMember";
            IDataParameter[] p = new IDataParameter[]{
                  new SqlParameter("@uid",SqlDbType.Int,4),
                  new SqlParameter("@empty",SqlDbType.Int,4),
                  new SqlParameter("@ipAddress",SqlDbType.VarChar,20),
                  new SqlParameter("@addUid",SqlDbType.Int,4),
                  new SqlParameter("@return",SqlDbType.VarChar,200)
            };
            p[0].Value = uid;
            p[1].Value = flag;
            p[2].Value = current.ipAddress;
            p[3].Value = current.id;
            p[4].Direction = ParameterDirection.Output;
            string result = dao.RunProceOnlyPara(pro, p, "return");
            if (result == "success")
            {
                //是否需要短信通知
                MobileNotice bf = noticeBLL.GetModel(ConstUtil.MOBILE_NOTICE_OPEN_MEMBER);
                if (bf.flag == 1)
                {
                    Member m = this.GetModelById(uid);
                    //发送短信
                    noticeBLL.SendMessage(m.phone, m.userId + bf.msg);
                }
                return result;
            }
            else
            {
                throw new ValidateException(result);
            }
        }

        public ResponseDtoData loginUser(Model.Member m, int isMan)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            if (m == null) { response.msg = "登录信息不能为空"; }
            else if (ValidateUtils.CheckNull(m.userId) || ValidateUtils.CheckNull(m.password)) { response.msg = "用户名或密码不能为空"; }
            else
            {
                //提交的密码加密
                m.password = DESEncrypt.EncryptDES(m.password, ConstUtil.SALT);
                //查询用户
                string sql = "select *,(case when loginLockTime is not null and loginLockTime>getDate() then 1 else 0 end) isLoginLock," +
                             "datediff(minute,getDate(),isnull(loginLockTime,getDate())) remainingMinutes from member where isAdmin = " + isMan;
                List<DbParameterItem> pdb = ParamUtil.Get().Add(new DbParameterItem("userId", ConstUtil.EQ, m.userId)).Result();
                Member mb = this.GetOne(sql, pdb);

                if (mb == null)
                {
                    response.msg = "账户名或密码错误";
                }
                else
                {
                    //登录日志
                    //状态，0：成功，1：失败（帐号不存在），2：失败（密码验证失败），3：（失败）帐号未开通，4：（失败）帐号被冻结，5：（失败）帐号被登录锁定
                    LoginHistory log = new LoginHistory();
                    log.uid = mb.id;
                    log.userId = m.userId;
                    log.loginIp = m.ipAddress;
                    log.isMan = isMan;
                    log.flag = 0;
                    log.mulx = "登录成功";
                    log.loginTime = DateTime.Now;

                    if (mb.isLoginLock == 1) //如果账户被锁定，直接返回
                    {
                        response.msg = "账户已被锁定，请" + mb.remainingMinutes + "分钟后尝试";
                        log = null;
                    }
                    else if (m.password != mb.password)
                    {
                        log.flag = 2;
                        log.mulx = "密码验证失败";
                        response.msg = "账户名或密码错误";

                        //登录参数
                        List<string> list = new List<string>();
                        list.Add("loginFailTimes");
                        list.Add("loginLockTime");
                        Dictionary<string, ParameterSet> pd = psBLL.GetToDictionary(list);
                        int loginFailTimes = Convert.ToInt32(pd["loginFailTimes"].paramValue); //连续错误次数
                        int loginLockTime = Convert.ToInt32(pd["loginLockTime"].paramValue);   //锁定时长（分钟）
                        int last = (loginFailTimes - (mb.loginFailTimes.Value + 1)); //剩余次数

                        //一般情况，增加用户登录失败次数
                        string usql = "update Member set loginFailTimes=loginFailTimes+1 where id = " + mb.id;

                        //剩余2次时提醒
                        if (last <= 2 && last > 0)
                        {
                            response.msg = "密码错误，再失败" + last + "次后账户将被锁定";
                        }
                        //达到指定连续错误次数,锁定帐号
                        else if (last <= 0)
                        {
                            response.msg = "密码错误，账户已被锁定，请" + loginLockTime + "分钟后尝试";
                            //用户登录失败次数清0，锁定住用户
                            usql = "update Member set loginFailTimes = 0,loginLockTime = DATEADD(mi," + loginLockTime + ",getDate()) where id = " + mb.id;
                        }

                        dao.ExecuteBySql(usql);
                        log.mulx = response.msg;
                    }
                    else if (mb.isPay == 0)
                    {
                        log.flag = 3;
                        log.mulx = "验证通过，帐号未开通";
                        response.msg = "帐号不可用";
                    }
                    else if (mb.isLock == 1)
                    {
                        log.flag = 4;
                        log.mulx = "验证通过，帐号被冻结";
                        response.msg = "帐号不可用";
                    }
                    else
                    {
                        //返回的用户对象不应含有密码、安全问题信息
                        mb.password = null;
                        mb.threepass = null;
                        mb.passOpen = null;
                        mb.question = null;
                        mb.answer = null;

                        //清空登录失败次数
                        string usql = "update Member set loginFailTimes=0,loginLockTime = null where id = " + mb.id;
                        dao.ExecuteBySql(usql);
                        response.result = mb;
                        response.status = "success";
                    }
                    if (log != null)
                    {
                        //保存登录日志
                        lhBll.Save(log);
                    }
                }
            }
            return response;
        }

        public Member GetModelByIdNoPassWord(int memberId)
        {
            Member mm = GetModelById(memberId);
            if (mm != null)
            {
                mm.password = null;
                mm.threepass = null;
                mm.passOpen = null;
                mm.question = null;
                mm.answer = null;
            }
            return mm;
        }

        public Member GetModelAndAccountNoPass(int memberId)
        {
            Member mm = GetModelByIdNoPassWord(memberId);
            if (mm != null)
            {
                mm.account = accountBLL.GetModel(mm.id.Value);
            }
            return mm;
        }

        public Member GetModelById(int memberId)
        {
            List<DbParameterItem> pdb = ParamUtil.Get().Add(new DbParameterItem("id", ConstUtil.EQ, memberId)).Result();
            Member mm = this.GetOne("select * from member ", pdb);
            return mm;
        }

        public double GetEpoints(string userId)
        {
            var model = dao.GetList("select uid,status,isnull(vi.SumCurrencyWcj,0) as epoints,ethValueTz_Wcj * (select paramValue from ParameterSet where paramCode = 'CurrencyFdbs') as wcj from Member m , v_Invest vi where m.id = vi.uid and userid = '" + userId + "'");
            if (model != null || model.Rows.Count > 0)
            {
                var epoints = Convert.ToDouble(model.Rows[0]["epoints"].ToString() == "" ? 0 : model.Rows[0]["epoints"]);
                var wcj = Convert.ToDouble(model.Rows[0]["wcj"].ToString() == "" ? 0 : model.Rows[0]["wcj"]);
                var status = model.Rows[0]["status"].ToString();
                return status == "已停止" ? -1 : wcj - epoints > 0 ? wcj - epoints : wcj - epoints == 0 ? 0 : -1;
            }
            return -1;
        }
        public Member GetModelByUserId(string userId)
        {
            List<DbParameterItem> pdb = ParamUtil.Get().Add(new DbParameterItem("userId", ConstUtil.EQ, userId)).Result();
            return this.GetOne("select * from member ", pdb);
        }

        public Member GetModelByUserName(string userName)
        {
            List<DbParameterItem> pdb = ParamUtil.Get().Add(new DbParameterItem("userName", ConstUtil.EQ, userName)).Result();
            return this.GetOne("select * from member ", pdb);
        }

        public Member GetModelByUserId(string userId, int isAdmin)
        {
            List<DbParameterItem> pdb = ParamUtil.Get()
                .Add(new DbParameterItem("userId", ConstUtil.EQ, userId))
                .Add(new DbParameterItem("isAdmin", ConstUtil.EQ, isAdmin)).Result();
            return this.GetOne("select * from member ", pdb);
        }
        public Member SaveMemberNoVerify(Member mb)
        {
            //默认登录密码
            Dictionary<string, ParameterSet> ps = psBLL.GetDictionaryByCodes("loginPass");
            string loginPass = ps["loginPass"].paramValue;
            mb.password = DESEncrypt.EncryptDES(loginPass, ConstUtil.SALT);
            if (mb.reId == null || mb.reId == 0)
            {
                mb.reId = 1;
                mb.reName = "system";
                mb.reuserName = "system";
            }

            Member re = this.GetModelByUserId(mb.reName, 0);
            mb.rePath = re.rePath + re.id + ",";
            mb.reLevel = re.reLevel + 1;
            mb.uLevel = 3;
            mb.status = "未参与";
            mb.isPay = 1;
            object o = this.SaveByIdentity(mb);
            int id = Convert.ToInt32(o);
            mb.id = id;
            int userName = 10000 + id;
            //改成无序生成
            try
            {

                userName = userName + int.Parse(GetRandom());
                DataTable dt_userName = dao.GetList("select * from Member where userName='" + userName.ToString() + "'");
                while (dt_userName.Rows.Count > 0)
                {
                    userName = userName + int.Parse(GetRandom());
                    dt_userName = dao.GetList("select * from Member where userName='" + userName.ToString() + "'");
                }

            }
            catch
            { }
            dao.ExecuteBySql("update Member set userName='" + userName.ToString() + "' where id=" + id);
            mb.userName = userName.ToString();

            //直推1带人数
            dao.ExecuteBySql("update Member set reCount = reCount+1 where id =" + mb.reId);
            //直推2带人数
            dao.ExecuteBySql("update Member set reTreeCount2 = reTreeCount2+1 where CHARINDEX(','+CAST(id  AS VARCHAR)+',','" + mb.rePath + "')>0 and relevel=" + (mb.reLevel - 2));
            //直推3带人数
            dao.ExecuteBySql("update Member set reTreeCount3 = reTreeCount3+1 where CHARINDEX(','+CAST(id  AS VARCHAR)+',','" + mb.rePath + "')>0 and relevel=" + (mb.reLevel - 3));
            //下线所有人数
            dao.ExecuteBySql("update Member set reTreeCount = reTreeCount+1 where CHARINDEX(','+CAST(id  AS VARCHAR)+',','" + mb.rePath + "')>0 ");
            //加角色
            dao.ExecuteBySql("insert into RoleMember  values(" + id + ",2)");
            return mb;
        }

        private string GetRandom()
        {
            Random ro = new Random(10);
            long tick = DateTime.Now.Ticks;
            Random ran = new Random((int)(tick & 0xffffffffL) | (int)(tick >> 32));
            int iResult = ran.Next(99999999);
            string s = iResult.ToString().PadLeft(8, '0');
            return s;
        }

        public Member SaveMember(Member mb)
        {
            string err = null;

            //string reg = @"^1(3|4|5|7|8)\d{9}$";


            //非空校验
            if (mb == null) { err = "保存内容为空"; }
            else if (ValidateUtils.CheckNull(mb.userId)) { err = "会员编码不能为空"; }
            //else if (ValidateUtils.CheckNull(mb.password)) { err = "登录密码不能为空"; }
            //else if (ValidateUtils.CheckNull(mb.passOpen)) { err = "安全密码不能为空"; }
            //else if (ValidateUtils.CheckNull(mb.threepass)) { err = "交易密码不能为空"; }
            //else if (ValidateUtils.CheckNull(mb.fatherName)) { err = "接点人编码不能为空"; }
            else if (ValidateUtils.CheckNull(mb.reName)) { err = "请输入推荐人"; }
            //else if (ValidateUtils.CheckNull(mb.shopName)) { err = "报单中心不能为空"; }
            //else if (ValidateUtils.CheckIntZero(mb.treePlace)) { err = "放置区域不能为空"; }
            //else if (mb.orderItems == null || mb.orderItems.Count == 0) { err = "注册购买的商品不能为空"; }
            //if (mb.sourceMachine != "app")
            //{
            //    if (!Regex.IsMatch(mb.phone, reg)) { throw new ValidateException("手机号码格式错误"); }
            //    if (ValidateUtils.CheckNull(mb.bankUser)) { err = "开户名不能为空"; }
            //    else if (ValidateUtils.CheckNull(mb.bankCard)) { err = "银行卡号不能为空"; }
            //    else if (ValidateUtils.CheckNull(mb.bankAddress)) { err = "开户支行名称不能为空"; }
            //    else if (mb.bankName == null || mb.bankName == "0") { err = "请选择开户行"; }
            //}
            if (err != null)
            {
                throw new ValidateException(err);
            }

            //检查用户名是否存在
            Member m = this.GetModelByUserId(mb.userId, 0);
            if (m != null) { throw new ValidateException("会员编码：" + m.userId + " 已经存在"); }

            //检查推荐人是否存在
            Member re = this.GetModelByUserName(mb.reName);
            if (re == null) { throw new ValidateException("推荐人：" + mb.reName + " 不存在"); }
            if (re.isPay != 1) { throw new ValidateException("推荐人：" + mb.reName + " 未开通，不能成为推荐人 "); }
            mb.reId = re.id;
            mb.rePath = re.rePath + re.id + ",";
            mb.reLevel = re.reLevel + 1;
            mb.reuserName = re.userName;
            mb.reName = re.userId;
            mb.userName = GetRandom();
            while (dao.GetCount("select count(1) from member where userName = '" + mb.userName + "'") > 0)
            {
                mb.userName = GetRandom();
            }

            //密码加密
            mb.password = DESEncrypt.EncryptDES(GetRandom() + "1", ConstUtil.SALT);
            mb.passOpen = DESEncrypt.EncryptDES(GetRandom() + "2", ConstUtil.SALT);
            mb.threepass = DESEncrypt.EncryptDES(GetRandom() + "3", ConstUtil.SALT);

            mb.regMoney = 0;
            mb.uLevel = 3;
            mb.status = "未参与";
            mb.isPay = 1;
            //其他默认信息
            mb.addTime = DateTime.Now;      //注册时间
            mb.loginFailTimes = 0;          //默认登录失败次数

            //保存会员
            mb.sourceMachine = null;
            object o = this.SaveByIdentity(mb);
            int id = Convert.ToInt32(o);
            mb.id = id;

            //加角色
            dao.ExecuteBySql("insert into RoleMember  values(" + id + ",2)");

            //直推1带人数
            dao.ExecuteBySql("update Member set reCount = reCount+1 where id =" + mb.reId);
            //直推2带人数
            dao.ExecuteBySql("update Member set reTreeCount2 = reTreeCount2+1 where CHARINDEX(','+CAST(id  AS VARCHAR)+',','" + mb.rePath + "')>0 and relevel=" + (mb.reLevel - 2));
            //直推3带人数
            dao.ExecuteBySql("update Member set reTreeCount3 = reTreeCount3+1 where CHARINDEX(','+CAST(id  AS VARCHAR)+',','" + mb.rePath + "')>0 and relevel=" + (mb.reLevel - 3));
            //下线所有人数
            dao.ExecuteBySql("update Member set reTreeCount = reTreeCount+1 where CHARINDEX(','+CAST(id  AS VARCHAR)+',','" + mb.rePath + "')>0 ");

            //默认保存一条账户记录
            MemberAccount account = new MemberAccount();
            account.id = id;
            accountBLL.SaveByIdentity(account);

            if (id <= 0)
            {
                throw new ValidateException("注册会员失败，请联系管理员");
            }
            return mb;
        }

        public PageResult<Member> GetOutRecordListPage(Member model)
        {
            PageResult<Member> page = new PageResult<Member>();

            string sql = "select *,row_number() over(order by addTime desc) rownumber from OutRecord where 1=1 ";
            string countSql = "select count(1) from OutRecord where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();

            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.userName))
                {
                    param.Add(new DbParameterItem("userName", ConstUtil.LIKE, model.userName));
                }

                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }


            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);
            List<DbParameterItem> param_nopage = param;
            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<Member> list = new List<Member>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    Member model_row = (Member)ReflectionUtil.GetModel(type, row);
                    list.Add(model_row);
                }
            }
            page.rows = list;

            //汇总已审未审
            dt = dao.GetList(sql, param_nopage, true);
            double all_ethValueTz_Wcj = 0;
            double all_SumCurrencyWcj = 0;
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    Member model_row = (Member)ReflectionUtil.GetModel(type, row);
                    all_ethValueTz_Wcj += model_row.ethValueTz_Wcj.Value;
                    all_SumCurrencyWcj += model_row.SumCurrencyWcj.Value;
                }
            }
            Member foot_model = new Member();
            list = new List<Member>();
            foot_model.ethValueTz_Wcj = all_ethValueTz_Wcj;
            foot_model.SumCurrencyWcj = all_SumCurrencyWcj;
            foot_model.userName = "合计";
            list.Add(foot_model);
            page.footer = list;

            return page;
        }


        public PageResult<Member> GetMemberListPage(Member model, string fields)
        {
            //异常帐户 总收益ETH大于封顶 总下发ETH大于总收益
            Dictionary<string, ParameterSet> ps = psBLL.GetDictionaryByCodes("CurrencyFdbs");
            string CurrencyFdbs = ps["CurrencyFdbs"].paramValue;
            double ethTotcsPrice = double.Parse(dao.GetList("SELECT ethTotcsPrice FROM BaseSet where 1=1").Rows[0]["ethTotcsPrice"].ToString());

            PageResult<Member> page = new PageResult<Member>();
            fields = fields == null ? allField : fields;
            string sql = "select " + fields + ",row_number() over(order by m.id desc) rownumber from Member m,v_Invest vi where m.id=vi.uid and isAdmin = 0 ";
            string countSql = "select count(1) from Member m,v_Invest vi where m.id=vi.uid and isAdmin = 0 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (model.isAgent != null)
                {
                    param.Add(new DbParameterItem("isAgent", ConstUtil.EQ, model.isAgent));
                }
                if (model.uLevel != null)
                {
                    param.Add(new DbParameterItem("uLevel", ConstUtil.EQ, model.uLevel));
                }
                if (model.empty != null)
                {
                    param.Add(new DbParameterItem("empty", ConstUtil.EQ, model.empty));
                }
                if (!ValidateUtils.CheckIntZero(model.shopid))
                {
                    param.Add(new DbParameterItem("shopId", ConstUtil.EQ, model.shopid));
                }
                if (model.isPay != null)
                {
                    param.Add(new DbParameterItem("isPay", ConstUtil.EQ, model.isPay));
                }
                if (!ValidateUtils.CheckNull(model.reName))
                {
                    param.Add(new DbParameterItem("reName", ConstUtil.LIKE, model.reName));
                }
                if (!ValidateUtils.CheckNull(model.reuserName))
                {
                    param.Add(new DbParameterItem("reuserName", ConstUtil.LIKE, model.reuserName));
                }
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.userName))
                {
                    param.Add(new DbParameterItem("userName", ConstUtil.LIKE, model.userName));
                }

                if (!ValidateUtils.CheckNull(model.status))
                {
                    if (model.status != "全部")
                    {
                        if (model.status != "冻结")
                        {
                            sql += " and status='" + model.status + "'";
                            countSql += " and status='" + model.status + "'";
                        }
                        else
                        {
                            sql += " and isLock=1";
                            countSql += " and isLock=1";
                        }
                    }
                }


                if (!ValidateUtils.CheckNull(model.phone))
                {
                    param.Add(new DbParameterItem("phone", ConstUtil.LIKE, model.phone));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
                if (model.passStartTime != null)
                {
                    param.Add(new DbParameterItem("passTime", ConstUtil.DATESRT_LGT_DAY, model.passStartTime));
                }
                if (model.passEndTime != null)
                {
                    param.Add(new DbParameterItem("passTime", ConstUtil.DATESRT_EGT_DAY, model.passEndTime));
                }
                if (model.isYC != null)
                {

                    sql += " and (vi.SumCurrencyETH>vi.ethValueYx*" + CurrencyFdbs + " or SumCurrencyXfETH>SumCurrencyETH)";
                    countSql += " and (vi.SumCurrencyETH>vi.ethValueYx*" + CurrencyFdbs + " or SumCurrencyXfETH>SumCurrencyETH)";
                }
                if (model.isHB != null)
                {
                    //sql += " and vi.SumCurrency<vi.ethValueTz ";
                    //countSql += " and vi.SumCurrency<vi.ethValueTz ";
                }
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<Member> list = new List<Member>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    Member model_row = (Member)ReflectionUtil.GetModel(type, row);
                    if (model_row.ethValueYx != null)
                    {
                        model_row.SumCurrencyFd = model_row.ethValueYx.Value * double.Parse(CurrencyFdbs);
                        if (model_row.SumCurrencyETH != null) model_row.SumCurrencyETHCfd = model_row.SumCurrencyETH.Value - model_row.SumCurrencyFd.Value;//总收益ETH超过封顶
                    }//封顶值
                    if (model_row.SumCurrencyXfETH != null && model_row.SumCurrencyETH != null)
                    {
                        model.SumCurrencyXfETHCg = model_row.SumCurrencyXfETH.Value - model_row.SumCurrencyETH.Value;//总下发超过 
                    }
                    //回本差额ＥＴＨ换算OCC
                    if (model_row.SumHBETH != null) model_row.SumHBTCS = ethTotcsPrice * model_row.SumHBETH.Value;
                    list.Add(model_row);
                }
            }
            page.rows = list;
            return page;
        }


        public DataTable GetExcelListPageMemberHb(Member model)
        {
            double ethTotcsPrice = double.Parse(dao.GetList("SELECT ethTotcsPrice FROM BaseSet where 1=1").Rows[0]["ethTotcsPrice"].ToString());
            string sql = "select m.userId,m.userName,d.name as uLevelName,ethValueTz,SumCurrency,ethValueTz-SumCurrency as SumHBETH,(ethValueTz-SumCurrency)*" + ethTotcsPrice + " as SumHBTCS,status," +
                            "reName,reuserName,m.addTime from Member m,v_Invest vi,DataDictionary d where m.id=vi.uid and m.uLevel=d.id and isAdmin = 0 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            if (model != null)
            {

                if (model.uLevel != null)
                {
                    param.Add(new DbParameterItem("uLevel", ConstUtil.EQ, model.uLevel));
                }

                if (model.isPay != null)
                {
                    param.Add(new DbParameterItem("isPay", ConstUtil.EQ, model.isPay));
                }
                if (!ValidateUtils.CheckNull(model.reName))
                {
                    param.Add(new DbParameterItem("reName", ConstUtil.LIKE, model.reName));
                }
                if (!ValidateUtils.CheckNull(model.reuserName))
                {
                    param.Add(new DbParameterItem("reuserName", ConstUtil.LIKE, model.reuserName));
                }
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.userName))
                {
                    param.Add(new DbParameterItem("userName", ConstUtil.LIKE, model.userName));
                }

                if (model.isHB != null)
                {
                    sql += " and vi.SumCurrency<vi.ethValueTz ";
                }
            }

            DataTable dt = dao.GetList(sql, param, true);
            return dt;

        }



        public PageResult<Member> GetAdminListPage(Member model, Member current)
        {
            PageResult<Member> page = new PageResult<Member>();
            string sql = "select m.id,m.userId,m.userName,m.addTime,t.nickName,row_number() over(order by m.id desc) rownumber from Member m left join " +
                         " (select memberId, nickName=stuff((select ','+l.roleName from RoleMember a inner join Role l on a.roleId = l.id where A.memberId=r.memberId for xml path('')), 1, 1, '') " +
                         " from RoleMember r group by memberId) t  on m.id = t.memberId" +
                         " where m.isAdmin = 1 ";
            string countSql = "select count(1) from Member m where m.isAdmin = 1 ";
            //非超级管理员不能查询超级管理员信息
            if (current.userId != ConstUtil.SUPER_ADMIN)
            {
                sql += " and m.userId<>'" + ConstUtil.SUPER_ADMIN + "'";
                countSql += " and m.userId<>'" + ConstUtil.SUPER_ADMIN + "'";
            }

            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (model.isPay != null)
                {
                    param.Add(new DbParameterItem("m.isPay", ConstUtil.EQ, model.isPay));
                }
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.userName))
                {
                    param.Add(new DbParameterItem("m.userName", ConstUtil.LIKE, model.userName));
                }
                if (!ValidateUtils.CheckNull(model.phone))
                {
                    param.Add(new DbParameterItem("m.phone", ConstUtil.LIKE, model.phone));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<Member> list = new List<Member>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Member)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public int UpdateLock(List<int> list, int isLock)
        {
            if (list == null || list.Count == 0)
            {
                throw new ValidateException("要更新的内容为空");
            }
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < list.Count; i++)
            {
                sb.Append(list[i]).Append(",");
            }
            if (isLock == 0 || isLock == 1)
            {
                string sql = "update member set isLock = @isLock where id in (" + sb.ToString().Substring(0, sb.Length - 1) + ")";
                List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("isLock", null, isLock))
                               .Result();
                return dao.ExecuteBySql(sql, param);
            }
            else
            {
                //获取参数
                Dictionary<string, ParameterSet> ps = psBLL.GetDictionaryByCodes("loginPass", "passOpen", "threePass");
                string loginPass = ps["loginPass"].paramValue;
                string passOpen = ps["passOpen"].paramValue;
                string threePass = ps["threePass"].paramValue;

                string sql = "update member set password = @password,passOpen = @passOpen,threepass = @threepass  where id in (" + sb.ToString().Substring(0, sb.Length - 1) + ")";

                List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("password", null, DESEncrypt.EncryptDES(loginPass, ConstUtil.SALT)))
                    .Add(new DbParameterItem("passOpen", null, DESEncrypt.EncryptDES(passOpen, ConstUtil.SALT)))
                    .Add(new DbParameterItem("threepass", null, DESEncrypt.EncryptDES(threePass, ConstUtil.SALT)))
                               .Result();
                return dao.ExecuteBySql(sql, param);
            }

        }

        public List<Member> GetDirectTreeList(string userId)
        {
            string sql = "select m.id,m.userId,m.userName,m.reId,m.reName,m.uLevel," +
                         "'（'+isnull(m.userName,'')+'）：'+isnull(d.name,'')+'：'+CAST(isnull(m.reCount,0) as varchar(10))+'：'+CAST(isnull(m.reTreeCount,0) as varchar(10))+'：'+CAST(isnull(yj.xxallxzyj,0) as varchar(10)) dtreeName " +
                         "from Member m inner join DataDictionary d on m.uLevel = d.id inner join v_yj yj on m.id=yj.uid where isAdmin = 0";
            if (!ValidateUtils.CheckNull(userId))
            {
                Member mm = GetModelByUserId(userId);
                if (mm == null) return null;
                sql += " and (CHARINDEX(','+CAST(" + mm.id + " AS VARCHAR)+',',m.rePath)>0 or m.id = " + mm.id + ")";
            }

            return this.GetList(sql);
        }


        public Member GetLevelReCount(string userId)
        {
            string sql = "select m.id,m.userId,m.userName,m.reId,m.reName,m.uLevel,m.reCount," +
                         "(SELECT count(1) FROM member WHERE  CHARINDEX(','+CAST(m.id AS VARCHAR)+',',RePath)>0 AND ReLevel>reLevel and ReLevel<=reLevel+1) nLevel1ReCount, " +
                         "(SELECT count(1) FROM member WHERE  CHARINDEX(','+CAST(m.id AS VARCHAR)+',',RePath)>0 AND ReLevel>reLevel and ReLevel<=reLevel+2) nLevel2ReCount, " +
                         "(SELECT count(1)FROM member WHERE  CHARINDEX(','+CAST(m.id AS VARCHAR)+',',RePath)>0 AND ReLevel>reLevel and ReLevel<=reLevel+3) nLevel3ReCount " +
                         "from Member m where isAdmin = 0";


            if (!ValidateUtils.CheckNull(userId))
            {
                Member mm = GetModelByUserId(userId);
                if (mm == null) return null;

                sql += "and userId = '" + userId + "'";
            }

            return this.GetOne(sql);
        }



        public Member GetDirectTree(int uid, Member current)
        {
            string sql = "select m.id,m.userId,m.userName,m.reId,m.reName,m.uLevel,m.reCount," +
                         "'（'+isnull(m.userName,'')+'）：'+isnull(d.name,'')+'：'+CAST(isnull(m.reCount,0) as varchar(10))+'：'+CAST(isnull(m.reTreeCount,0) as varchar(10))+'：'+CAST(isnull(yj.xxallxzyj,0) as varchar(10)) dtreeName  " +
                         "from Member m inner join DataDictionary d on m.uLevel = d.id inner join v_yj yj on m.id=yj.uid where isAdmin = 0";
            if (current.isAdmin.Value == 0) sql += " and m.reLevel<=" + (current.reLevel + 3).ToString();//下线三代内
            //只能查询当前登录会员及下级节点的会员
            //userId非空则判断userId是否是当前登录会员或下级节点，是：则按userId查询，不是：返回空列表
            if (uid > 0)
            {
                Member mm = GetModelById(uid);
                if (mm == null) { sql += " and 1=2"; }
                else
                {
                    Member cr = GetModelById(current.id.Value);
                    if (cr.isAdmin.Value == 1 || mm.id.Value == cr.id.Value || mm.rePath.Contains("," + cr.id + ","))
                    {
                        sql += " and (m.id=" + uid + " or m.reId=" + uid + ")";
                    }
                    else
                    {
                        sql += " and 1=2";
                    }
                }
            }
            //userId为空则查询当前登录会员直推图
            else
            {
                sql += " and (m.id=" + uid + " or m.reId=" + uid + ")";// +" (CHARINDEX(','+CAST(" + current.id + " AS VARCHAR)+',',m.rePath)>0 or m.id = " + current.id + ")";
            }
            List<Member> list = this.GetList(sql);
            List<Member> reList = new List<Member>();
            Member result = null;
            for (int i = 0; i < list.Count; i++)
            {
                Member m = list[i];
                if (m.id.Value == uid)
                {
                    result = m;
                }
                else
                {
                    reList.Add(m);
                }
            }
            result.reList = reList;
            return result;
        }


        public List<Member> GetDirectTreeList(string userId, Member current)
        {
            string sql = "select m.id,m.userId,m.userName,m.reId,m.reName,m.uLevel," +
                         "'（'+isnull(m.userName,'')+'）：'+isnull(d.name,'')+'：'+CAST(isnull(m.reCount,0) as varchar(10))+'：'+CAST(isnull(m.reTreeCount,0) as varchar(10))+'：'+CAST(isnull(yj.xxallxzyj,0) as varchar(10)) dtreeName  " +
                         "from Member m inner join DataDictionary d on m.uLevel = d.id inner join v_yj yj on m.id=yj.uid where isAdmin = 0";
            if (current.isAdmin.Value == 0) sql += " and m.reLevel<=" + (current.reLevel + 3).ToString();//下线三代内
            //只能查询当前登录会员及下级节点的会员
            //userId非空则判断userId是否是当前登录会员或下级节点，是：则按userId查询，不是：返回空列表
            if (!ValidateUtils.CheckNull(userId))
            {
                Member mm = GetModelByUserId(userId);
                if (mm == null) { sql += " and 1=2"; }
                else
                {
                    Member cr = GetModelById(current.id.Value);
                    if (mm.id.Value == cr.id.Value || mm.rePath.Contains("," + cr.id + ","))
                    {
                        sql += " and (CHARINDEX(','+CAST(" + mm.id + " AS VARCHAR)+',',m.rePath)>0 or m.id = " + mm.id + ")";
                    }
                    else
                    {
                        sql += " and 1=2";
                    }

                }
            }
            //userId为空则查询当前登录会员直推图
            else
            {
                sql += " and (CHARINDEX(','+CAST(" + current.id + " AS VARCHAR)+',',m.rePath)>0 or m.id = " + current.id + ")";
            }
            return this.GetList(sql);
        }

        public int SaveAdmin(Member mb)
        {
            string err = null;

            //非空校验
            if (mb == null) { err = "保存内容为空"; }
            else if (ValidateUtils.CheckNull(mb.userId)) { err = "管理员编码不能为空"; }
            else if (ValidateUtils.CheckNull(mb.password)) { err = "登录密码不能为空"; }
            else if (ValidateUtils.CheckNull(mb.passOpen)) { err = "安全密码不能为空"; }
            else if (ValidateUtils.CheckNull(mb.threepass)) { err = "交易密码不能为空"; }
            if (err != null)
            {
                throw new ValidateException(err);
            }

            //检查用户名是否存在
            Member m = this.GetModelByUserId(mb.userId);
            if (m != null) { throw new ValidateException("会员编码：" + m.userId + " 已经存在"); }

            //密码加密
            mb.password = DESEncrypt.EncryptDES(mb.password, ConstUtil.SALT);
            mb.passOpen = DESEncrypt.EncryptDES(mb.passOpen, ConstUtil.SALT);
            mb.threepass = DESEncrypt.EncryptDES(mb.threepass, ConstUtil.SALT);

            //其他默认信息
            mb.addTime = DateTime.Now;      //注册时间
            mb.loginFailTimes = 0;          //默认登录失败次数
            mb.isAdmin = 1; //后台会员
            mb.isPay = 1;
            mb.isLock = 0;

            //保存会员
            object o = this.SaveByIdentity(mb);
            int id = Convert.ToInt32(o);

            if (id <= 0)
            {
                throw new ValidateException("添加管理员失败，请联系管理员");
            }
            return id;
        }



        public int UpdatePassword(int memberId, string oldPass, string newPass, int flag)
        {
            if (ValidateUtils.CheckNull(newPass)) { throw new ValidateException("修改后的密码不能为空"); }
            Member m = GetModelById(memberId);
            oldPass = DESEncrypt.EncryptDES(oldPass, ConstUtil.SALT);
            newPass = DESEncrypt.EncryptDES(newPass, ConstUtil.SALT);
            Member update = new Member();
            update.id = memberId;
            if (flag == 1)
            {
                if (oldPass != m.password) { throw new ValidateException("旧登录密码错误"); }
                else
                {
                    update.password = newPass;
                }
            }
            else if (flag == 2)
            {
                if (oldPass != m.passOpen) { throw new ValidateException("旧安全密码错误"); }
                else
                {
                    update.passOpen = newPass;
                }
            }
            else if (flag == 3)
            {
                if (oldPass != m.threepass) { throw new ValidateException("旧交易密码错误"); }
                else
                {
                    update.threepass = newPass;
                }
            }
            else
            {
                throw new ValidateException("参数错误");
            }
            return this.Update(update);
        }

        public object[] GetRegMoneyAndNum(int ulevel)
        {
            //会员级别ID与参数注册单数对应关系
            //key:会员级别ID，value:参数paramCode
            Dictionary<int, string> ulelveDi = new Dictionary<int, string>();
            ulelveDi.Add(3, "upUlevel1");
            ulelveDi.Add(4, "upUlevel2");
            ulelveDi.Add(5, "upUlevel3");
            //查询参数
            List<string> list = new List<string>();
            list.Add("upUlevel1");
            list.Add("upUlevel2");
            list.Add("upUlevel3");
            list.Add("regPrice");
            Dictionary<string, ParameterSet> di = psBLL.GetToDictionary(list);
            //单价
            double price = Convert.ToDouble(di["regPrice"].paramValue);
            //会员级别所需单数
            int num = Convert.ToInt32(di[ulelveDi[ulevel]].paramValue);

            object[] objs = { price * num, num };
            return objs;
        }

        public string GetLeftUserId(int uid)
        {
            //调用存储过程 getLeftUid
            string pro = "getLeftUid";
            IDataParameter[] p = new IDataParameter[]{
                new SqlParameter("@uid",SqlDbType.Int,10),
                  new SqlParameter("@return",SqlDbType.VarChar,200)
            };
            p[0].Value = uid;
            p[1].Direction = ParameterDirection.Output;
            string result = dao.RunProceOnlyPara(pro, p, "return");
            return result;
        }

        public int UpdateForgetPass(Member m)
        {
            if (m == null) { throw new ValidateException("密码为空"); }
            if (ValidateUtils.CheckNull(m.password)) { throw new ValidateException("登录密码不能为空"); }
            if (ValidateUtils.CheckNull(m.passOpen)) { throw new ValidateException("安全密码不能为空"); }
            if (ValidateUtils.CheckNull(m.threepass)) { throw new ValidateException("交易密码不能为空"); }
            if (ValidateUtils.CheckIntZero(m.id)) { throw new ValidateException("修改会员不能为空"); }

            m.password = DESEncrypt.EncryptDES(m.password, ConstUtil.SALT);
            m.passOpen = DESEncrypt.EncryptDES(m.passOpen, ConstUtil.SALT);
            m.threepass = DESEncrypt.EncryptDES(m.threepass, ConstUtil.SALT);
            return dao.Update(m);
        }

        public int UpdateMember(Member model, Member current)
        {
            if (model == null || ValidateUtils.CheckIntZero(model.id)) { throw new ValidateException("修改会员不能为空"); }
            if (!ValidateUtils.CheckNull(model.password)) { model.password = DESEncrypt.EncryptDES(model.password, ConstUtil.SALT); }
            if (!ValidateUtils.CheckNull(model.passOpen)) { model.passOpen = DESEncrypt.EncryptDES(model.passOpen, ConstUtil.SALT); }
            if (!ValidateUtils.CheckNull(model.threepass)) { model.threepass = DESEncrypt.EncryptDES(model.threepass, ConstUtil.SALT); }
            if (model.bankCard.Length < 16 || model.bankCard.Length > 19) { throw new ValidateException("银行卡号位数必须为16-19位"); }
            Member old = this.GetModelById(model.id.Value);
            int c = dao.Update(model);
            if (old.bankCard != model.bankCard || old.bankUser != model.bankUser)
            {
                //发短信
                //发送给管理员短信
                noticeBLL.SendMessage(model.phone, "您正在修改收款银行信息，如非本人操作，请及时与管理员联系，" +
                    "以免给您造成不必要的损失！");

                //保存操作日志
                OperateLog log = new OperateLog();
                log.recordId = model.id;
                log.uid = current.id;
                log.userId = current.userId;
                log.ipAddress = current.ipAddress;
                log.mulx = "修改后的：银行卡号：" + model.bankCard + "，开户名：" + model.bankName;
                log.tableName = "Member";
                log.recordName = "修改银行收款信息";
                this.SaveOperateLog(log);
            }
            return c;

        }

        public Dictionary<string, object> GetRegisterDefaultMsg(int uid)
        {
            Dictionary<string, object> di = new Dictionary<string, object>();
            if (uid > 0)
            {
                Member m = GetModelById(uid);
                if (m != null)
                {
                    //默认接点人
                    di.Add("fatherName", m.userId);
                }
            }

            //注册协议
            News n = newsBLL.GetModelByTypeId(ConstUtil.NEWS_ZCXY);
            if (n != null)
            {
                di.Add("zcxy", n.content);
            }

            Dictionary<string, ParameterSet> ps = psBLL.GetDictionaryByCodes("loginPass", "passOpen", "threePass", "regPrice", "upUlevel1", "upUlevel2", "upUlevel3",
                "userIdPrefix");
            double price = Convert.ToDouble(ps["regPrice"].paramValue);             //每单注册金额
            di.Add("loginPass", ps["loginPass"].paramValue);                        //默认登录密码
            di.Add("passOpen", ps["passOpen"].paramValue);                          //默认安全密码
            di.Add("threePass", ps["threePass"].paramValue);                        //默认交易密码
            di.Add("pk", Convert.ToInt32(ps["upUlevel1"].paramValue) * price);      //普卡注册金额
            di.Add("yk", Convert.ToInt32(ps["upUlevel2"].paramValue) * price);      //银卡注册金额
            di.Add("jk", Convert.ToInt32(ps["upUlevel3"].paramValue) * price);      //金卡注册金额
            di.Add("userIdPrefix", ps["userIdPrefix"].paramValue);                  //用户名前缀
            return di;
        }

        public ResponseDtoData CheckUserId(string userId, int flag)
        {
            if (Common.SqlChecker.CheckKeyWord(userId) != "") { throw new ValidateException("含SQL注入字符" + Common.SqlChecker.CheckKeyWord(userId)); }
            ResponseDtoData response = new ResponseDtoData("fail");
            if (userId == null || userId == "")
            {
                response.msg = "编号为空";
            }
            else
            {
                Member m = GetModelByUserId(userId, 0);
                //检查用户编码是否已经存在
                if (flag == 1)
                {
                    if (m != null) { response.msg = "用户已经存在"; }
                    else { response.status = "success"; response.msg = "OK"; }
                }
                //检查接点人编码是否存在
                else if (flag == 2)
                {
                    if (m != null) { response.status = "success"; response.msg = m.userName; }
                    else { response.msg = "接点人不存在"; }
                }
                //检查推荐人编码是否存在
                else if (flag == 3)
                {
                    if (m != null) { response.status = "success"; response.msg = m.userName; }
                    else { response.msg = "推荐人不存在"; }
                }
                //检查报单中心编码是否存在
                else if (flag == 4)
                {
                    if (m != null && m.isAgent.Value == 2) { response.status = "success"; response.msg = m.userName; }
                    else { response.msg = "报单中心不存在"; }
                }
            }
            return response;
        }

        public string GetParentLinkString(int type, int uid)
        {
            Member m = this.GetModelById(uid);
            if (m == null || m.id == 1) { return ""; }
            string path = type == 1 ? m.pPath : m.rePath;
            List<Member> list = this.GetList("select userId,userName from Member where CHARINDEX(','+CAST(id AS VARCHAR)+',','" + path + "')>0 ");
            StringBuilder sb = new StringBuilder("");
            if (list != null && list.Count > 0)
            {
                foreach (Member mm in list)
                {
                    sb.Append(mm.userId).Append("（").Append(mm.userName).Append("）").Append("/ ");
                }
            }
            return sb.Length > 0 ? sb.ToString().Substring(0, sb.Length - 2) : "";
        }


        public int SaveChangeSecurityPass(Member m)
        {
            if (m.oldAnswer != null && Common.SqlChecker.CheckKeyWord(m.oldAnswer) != "") { throw new ValidateException("含SQL注入字符" + Common.SqlChecker.CheckKeyWord(m.oldAnswer)); }
            if (m.question != null && Common.SqlChecker.CheckKeyWord(m.question) != "") { throw new ValidateException("含SQL注入字符" + Common.SqlChecker.CheckKeyWord(m.question)); }
            if (string.IsNullOrEmpty(m.answer)) { throw new ValidateException("密保答案不能为空"); }


            if (m.fristAnswer != "Y")
            {
                if (string.IsNullOrEmpty(m.oldAnswer))
                {
                    throw new ValidateException("旧密保答案不能为空");
                }
                else
                {
                    Member oldMember = GetModelById(m.id.Value);
                    string oldAnswer = DESEncrypt.EncryptDES(m.oldAnswer, ConstUtil.SALT);
                    if (oldAnswer != oldMember.answer) { throw new ValidateException("旧密保答案错误"); }
                }
            }
            else
            {
                if (string.IsNullOrEmpty(m.question)) { throw new ValidateException("密保问题不能为空"); }
            }
            if (!string.IsNullOrEmpty(m.question))
            {
                m.question = DESEncrypt.EncryptDES(m.question, ConstUtil.SALT);
            }
            m.answer = DESEncrypt.EncryptDES(m.answer, ConstUtil.SALT);
            m.oldAnswer = null;
            m.fristAnswer = null;
            return dao.Update(m);
        }

        public DataTable GetMemberExcelList(int type)
        {
            string sql = "";
            if (type == 0) //未开通
            {
                sql = "select userId,userName,addTime,fatherName,reName,shopName,uLevel,regMoney,phone from Member where 1=1 and id>3";
            }
            else if (type == 1)  //已开通
            {
                sql = "select userId,userName,addTime,fatherName,reName,shopName,uLevel,regMoney,phone,byopen,passTime from Member where 1=1 and id>3";
            }
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (type != null)
            {
                if (!ValidateUtils.CheckNull(type.ToString()))
                {
                    param.Add(new DbParameterItem("isPay", ConstUtil.EQ, type));
                }
            }
            DataTable dt = dao.GetList(sql, param, true);
            return dt;
        }
    }
}
