﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class RelationEditBLL : BaseBLL<RelationEdit>, IRelationEditBLL
    {

        private System.Type type = typeof(RelationEdit);
        public IBaseDao dao { get; set; }

        public IMemberBLL memberBLL { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new RelationEdit GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param, true);
            if (row == null) return null;
            RelationEdit mb = (RelationEdit)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new RelationEdit GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            RelationEdit mb = (RelationEdit)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<RelationEdit> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<RelationEdit> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<RelationEdit>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((RelationEdit)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<RelationEdit> GetList(string sql)
        {
            List<RelationEdit> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<RelationEdit>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((RelationEdit)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public RelationEdit SaveRelationEdit(RelationEdit mb, Member current)
        {
            string err = null;
            //校验start----------------------------------
            //非空校验
            if (mb == null) { err = "保存内容为空"; }
            else if (ValidateUtils.CheckNull(mb.userId)) { err = "会员编码不能为空"; }
            else if (ValidateUtils.CheckNull(mb.zh_userId)) { err = "置换会员不能为空"; }
            else if (mb.userId==mb.zh_userId) { err = "置换双方不能为同一人"; }
            if (err != null) { throw new ValidateException(err); }

            //修改的会员
            Member m = memberBLL.GetModelByUserId(mb.userId);
            if (m == null) { throw new ValidateException("会员编号1不存在"); }
            //置换的会员
            Member m_zh = memberBLL.GetModelByUserId(mb.zh_userId);
            if (m_zh == null) { throw new ValidateException("会员编号2不存在"); }

            //原系谱图信息
            int treePlace = m.treePlace.Value;
            int fatherID = m.fatherID.Value;
            string fatherName = m.fatherName;
            int pLevel = m.pLevel.Value;
            string pPath = m.pPath;

            int treePlace_zh = m_zh.treePlace.Value;
            int fatherID_zh = m_zh.fatherID.Value;
            string fatherName_zh = m_zh.fatherName;
            int pLevel_zh = m_zh.pLevel.Value;
            string pPath_zh = m_zh.pPath;

            //与是否在同颗树无关的信息置换
            m.treePlace = treePlace_zh;
            m_zh.treePlace = treePlace;
            m.pLevel = pLevel_zh;
            m_zh.pLevel = pLevel;
            //父节点置换是否为父子关系
            string sql_update_child = "";
            string sql_update_child_zh = "";
            if(fatherID == m_zh.id.Value || fatherID_zh == m.id.Value)
            {
                if(fatherID == m_zh.id.Value)//置换会员是父
                {
                    m.fatherID = fatherID_zh;
                    m.fatherName = fatherName_zh;
                    m_zh.fatherID = m.id;
                    m_zh.fatherName = m.userName;

                    //取出需要更新的会员，不能使用SQL，不然每一次更新后数据已经不准

                    //置换会员的其它子->会员的子
                    List<Member> u_update_child = new List<Member>();
                    string sql = "select * from Member where " +
                                 " fatherID=@fatherID and id not in (@id)";
                    List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("fatherID", null, m_zh.id.ToString()))
                                    .Add(new DbParameterItem("id", null, m.id.ToString()))
                                    .Result();
                    u_update_child = memberBLL.GetMemberList(sql, param, false);
                    string id_in = "0";
                    if (u_update_child != null)
                    foreach (Member m_in in u_update_child)  
                    {
                        id_in += "," + m_in.id.ToString();
                    }
                    sql_update_child = " update member set fatherID=" + m.id.ToString() + ",fatherName='" + m.userName + "' where id  in (" + id_in + ") ";
                    //会员的子->置换会员的子
                    u_update_child = new List<Member>();
                    sql = "select * from Member where " +
                                 " fatherID=@fatherID";
                   param = ParamUtil.Get().Add(new DbParameterItem("fatherID", null, m.id.ToString()))
                                    .Result();
                   u_update_child = memberBLL.GetMemberList(sql, param, false);
                    id_in = "0";
                    if (u_update_child != null)
                    foreach (Member m_in in u_update_child)  
                    {
                        id_in += "," + m_in.id.ToString();
                    }
                    sql_update_child_zh = " update member set fatherID=" + m_zh.id.ToString() + ",fatherName='" + m_zh.userName + "' where id  in (" + id_in + ") ";
                }
                if(fatherID_zh == m.id.Value)//置换会员是子
                {
                     m.fatherID = m_zh.id; ;
                     m.fatherName = m_zh.userName;
                     m_zh.fatherID = fatherID;
                     m_zh.fatherName = fatherName;
                     //置换会员的全部子->会员的子
                     List<Member> u_update_child = new List<Member>();
                     string sql = "select * from Member where " +
                                  " fatherID=@fatherID";
                     List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("fatherID", null, m_zh.id.ToString()))
                                     .Result();
                     u_update_child = memberBLL.GetMemberList(sql, param, false);
                     string id_in = "0";
                     if (u_update_child != null)
                     foreach (Member m_in in u_update_child) 
                     {
                         id_in += "," + m_in.id.ToString();
                     }
                     sql_update_child = " update member set fatherID=" + m.id.ToString() + ",fatherName='" + m.userName + "'  where id  in (" + id_in + ") ";
                     //会员的其它子->置换会员的子
                     u_update_child = new List<Member>();
                     sql = "select * from Member where " +
                                  " fatherID=@fatherID and id not in (@id)";
                     param = ParamUtil.Get().Add(new DbParameterItem("fatherID", null, m.id.ToString()))
                                     .Add(new DbParameterItem("id", null, m_zh.id.ToString()))
                                     .Result();
                     u_update_child = memberBLL.GetMemberList(sql, param, false);
                     id_in = "0";
                     if (u_update_child != null)
                     foreach (Member m_in in u_update_child)  
                     {
                         id_in += "," + m_in.id.ToString();
                     }
                     sql_update_child_zh = " update member set fatherID=" + m_zh.id.ToString() + ",fatherName='" + m_zh.userName + "' where id  in (" + id_in + ") ";
                }
            }
            else
            {
                m.fatherID = fatherID_zh;
                m.fatherName = fatherName_zh;
                m_zh.fatherID = fatherID;
                m_zh.fatherName = fatherName;
                //置换会员的全部子->会员的子
                List<Member> u_update_child = new List<Member>();
                string sql = "select * from Member where " +
                             " fatherID=@fatherID";
                List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("fatherID", null, m_zh.id.ToString()))
                                .Result();
                u_update_child = memberBLL.GetMemberList(sql, param, false);
                string id_in = "0";
                if(u_update_child!=null)
                foreach (Member m_in in u_update_child)
                {
                    id_in += "," + m_in.id.ToString();
                }
                sql_update_child = " update member set fatherID=" + m.id.ToString() + ",fatherName='" + m.userName + "' where id  in (" + id_in + ") ";
                //会员的子->置换会员的子
                u_update_child = new List<Member>();
                 sql = "select * from Member where " +
                             " fatherID=@fatherID";
                param = ParamUtil.Get().Add(new DbParameterItem("fatherID", null, m.id.ToString()))
                                .Result();
                u_update_child = memberBLL.GetMemberList(sql, param, false);
                id_in = "0";
                if (u_update_child != null)
                foreach (Member m_in in u_update_child)
                {
                    id_in += "," + m_in.id.ToString();
                }
                sql_update_child_zh = " update member set fatherID=" + m_zh.id.ToString() + ",fatherName='" + m_zh.userName + "'  where id  in (" + id_in + ") ";
            }

            
            //接点路径是否为同颗树:暂时不改路径，影响到奖金计算
            string sql_update_child_path = "";
            if(pPath_zh.Contains(pPath) || pPath.Contains(pPath_zh))
            {
                if(pPath.Contains(pPath_zh))//置换会员是父
                {
                    m.pPath = pPath_zh;//子取代父路径
                    m_zh.pPath = pPath.Replace("," + m_zh.id + ",", "," + m.id + ",");//父用子路径同时 自己->子点
                    //子层下方路径必须先更新
                    //会员的子:会员ID->置换ID
                    List<Member> u_update_child_path = new List<Member>();
                    string sql = "select * from Member  " +
                                 "  where pPath like '%'+@pPath_like+'%'";
                    List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("pPath_like", null, pPath+m.id+","))
                                    .Result();
                    u_update_child_path = memberBLL.GetMemberList(sql, param, false);
                    string id_in = "0";
                    if (u_update_child_path != null)
                    foreach (Member m_in in u_update_child_path)
                    {
                        id_in += "," + m_in.id.ToString();
                    }

                    sql_update_child_path = " update member set pPath=replace(pPath,'" + pPath + m.id.ToString() + ",','" + pPath + m_zh.id.ToString() + ",') where id  in (" + id_in + ") ";


                    //置换会员的其它子:置换ID->会员ID
                    u_update_child_path = new List<Member>();
                    sql = "select * from Member  " +
                                 "  where pPath like '%'+@pPath_like+'%'" + " and id not in (@id)";
                    param = ParamUtil.Get().Add(new DbParameterItem("pPath_like", null, pPath_zh + m_zh.id + ","))
                                      .Add(new DbParameterItem("id", null, m.id.ToString()))
                                    .Result();
                    u_update_child_path = memberBLL.GetMemberList(sql, param, false);
                    id_in = "0";
                    if (u_update_child_path != null)
                    foreach (Member m_in in u_update_child_path)
                    {
                        id_in += "," + m_in.id.ToString();
                    }
                    sql_update_child_path += " update member set pPath=replace(pPath,'" + pPath_zh + m_zh.id.ToString() + ",','" + pPath_zh + m.id.ToString() + ",') where id  in (" + id_in + ") ";
                }
                if (pPath_zh.Contains(pPath))//置换会员是子
                {
                    m_zh.pPath = pPath;//子取代父路径
                    m.pPath = pPath_zh.Replace("," + m.id + ",", "," + m_zh.id + ",");//父用子路径同时 自己->子点
                    //置换会员的子:置换ID->会员ID
                    List<Member>  u_update_child_path = new List<Member>();
                    string sql = "select * from Member  " +
                                 "  where pPath like '%'+@pPath_like+'%'";
                    List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("pPath_like", null, pPath_zh + m_zh.id + ","))
                                    .Result();
                    u_update_child_path = memberBLL.GetMemberList(sql, param, false);
                    string id_in = "0";
                    if (u_update_child_path != null)
                    foreach (Member m_in in u_update_child_path)
                    {
                        id_in += "," + m_in.id.ToString();
                    }
                    sql_update_child_path = " update member set pPath=replace(pPath,'" + pPath_zh + m_zh.id.ToString() + ",','" + pPath_zh + m.id.ToString() + ",') where id  in (" + id_in + ") ";
                    //会员的其它子:会员ID->置换ID
                    u_update_child_path = new List<Member>();
                    sql = "select * from Member  " +
                                 "  where pPath like '%'+@pPath_like+'%'" + " and id not in (@id)";
                     param = ParamUtil.Get().Add(new DbParameterItem("pPath_like", null, pPath + m.id + ","))
                           .Add(new DbParameterItem("id", null, m_zh.id.ToString()))
                                    .Result();
                     u_update_child_path = memberBLL.GetMemberList(sql, param, false);
                     id_in = "0";
                     if (u_update_child_path != null)
                    foreach (Member m_in in u_update_child_path)
                    {
                        id_in += "," + m_in.id.ToString();
                    }

                    sql_update_child_path += " update member set pPath=replace(pPath,'" + pPath + m.id.ToString() + ",','" + pPath + m_zh.id.ToString() + ",') where id  in (" + id_in + ") ";
                }
            }
            else
            {
                    m_zh.pPath = pPath;
                    m.pPath = pPath_zh;
                    //置换会员的子:置换ID->会员ID
                    List<Member> u_update_child_path = new List<Member>();
                    string sql = "select * from Member  " +
                                 "  where pPath like '%'+@pPath_like+'%'";
                    List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("pPath_like", null, pPath_zh + m_zh.id + ","))
                                    .Result();
                    u_update_child_path = memberBLL.GetMemberList(sql, param, false);
                    string id_in = "0";
                    if (u_update_child_path != null)
                    foreach (Member m_in in u_update_child_path)
                    {
                        id_in += "," + m_in.id.ToString();
                    }
                    sql_update_child_path = " update member set pPath=replace(pPath,'" + pPath_zh + m_zh.id.ToString() + ",','" + pPath_zh + m.id.ToString() + ",') where id  in (" + id_in + ") ";

                    //会员的其它子:会员ID->置换ID
                    u_update_child_path = new List<Member>();
                    sql = "select * from Member  " +
                                 "  where pPath like '%'+@pPath_like+'%'";
                    param = ParamUtil.Get().Add(new DbParameterItem("pPath_like", null, pPath + m.id + ","))
                                   .Result();
                    u_update_child_path = memberBLL.GetMemberList(sql, param, false);
                    id_in = "0";
                    if (u_update_child_path != null)
                    foreach (Member m_in in u_update_child_path)
                    {
                        id_in += "," + m_in.id.ToString();
                    }

                    sql_update_child_path += " update member set pPath=replace(pPath,'" + pPath + m.id.ToString() + ",','" + pPath + m_zh.id.ToString() + ",') where id  in (" + id_in + ") ";
            }
             

           
            //校验end----------------------------------

            //保存修改安置关系记录--被置换人
            mb.id = null;
            mb.addTime = DateTime.Now;
            mb.createId = current.id;
            mb.createUser = current.userId;
            mb.new_fatherID = m_zh.fatherID;
            mb.new_fatherName = memberBLL.GetModelById(m_zh.fatherID.Value).userId;
            mb.new_treePlace = m_zh.treePlace;
            mb.old_fatherID = fatherID_zh;
            mb.old_fatherName = memberBLL.GetModelById(fatherID_zh).userId;
            mb.old_treePlace = treePlace_zh;
            mb.uid = m_zh.id;
            mb.userId = m_zh.userId;
            mb.userName = m_zh.userName;
            mb.zh_uid = m.id;
            mb.zh_userId = m.userId;
            mb.zh_userName = m.userName;
            mb.remark = mb.userId + "和" + mb.zh_userId + "位置互换";
            this.SaveByIdentity(mb);

            //保存修改安置关系记录
            mb.addTime = DateTime.Now;
            mb.createId = current.id;
            mb.createUser = current.userId;
            mb.new_fatherID = m.fatherID;
            mb.new_fatherName = memberBLL.GetModelById(m.fatherID.Value).userId;
            mb.new_treePlace = m.treePlace;
            mb.old_fatherID = fatherID;
            mb.old_fatherName = memberBLL.GetModelById(fatherID).userId; 
            mb.old_treePlace = treePlace;
            mb.uid = m.id;
            mb.userId = m.userId;
            mb.userName = m.userName;
            mb.zh_uid = m_zh.id;
            mb.zh_userId = m_zh.userId;
            mb.zh_userName = m_zh.userName;
            mb.remark = mb.userId + "和" + mb.zh_userId + "位置互换";
            object o = this.SaveByIdentity(mb);
            int newId = Convert.ToInt32(o);
            mb.id = newId;


            
            //保存操作日志
            OperateLog log = new OperateLog();
            log.recordId = newId;
            log.uid = current.id;
            log.userId = current.userId;
            log.ipAddress = current.ipAddress;
            log.mulx = "{userId:'"+ mb.userId +"',"
                + "fatherID:'" +fatherID.ToString() + "',"
                 + "fatherName:'" + fatherName + "',"
                 + "treePlace:'" + treePlace.ToString() + "',"
                  + "pLevel:'" + pLevel.ToString() + "',"
                  + "pPath:'" + pPath + "'"
            +"}-与置换-"
            + "{userId:'" + mb.zh_userId + "',"
                + "fatherID:'" +fatherID_zh.ToString() + "',"
                 + "fatherName:'" + fatherName_zh + "',"
                 + "treePlace:'" + treePlace_zh.ToString() + "',"
                  + "pLevel:'" + pLevel_zh.ToString() + "',"
                  + "pPath:'" + pPath_zh + "'"
            +"}";
            log.tableName = "RelationEdit";
            log.recordName = "置换关系方式一";
            this.SaveOperateLog(log);


            

            //修改会员信息
            memberBLL.Update(m);
            memberBLL.Update(m_zh);

           
            //sql
            dao.ExecuteBySql(sql_update_child + sql_update_child_zh + sql_update_child_path);



            if (newId <= 0)
            {
                throw new ValidateException("修改失败，请联系管理员");
            }

            return mb;
        }

        public RelationEdit SaveRelationEdit1(RelationEdit mb, Member current)
        {
            string err = null;
            //校验start----------------------------------
            //非空校验
            if (mb == null) { err = "保存内容为空"; }
            else if (ValidateUtils.CheckNull(mb.userId)) { err = "会员编码不能为空"; }
            else if (ValidateUtils.CheckNull(mb.zh_userId)) { err = "置换会员不能为空"; }
            else if (mb.userId == mb.zh_userId) { err = "置换双方不能为同一人"; }
            if (err != null) { throw new ValidateException(err); }

            //修改的会员
            Member m = memberBLL.GetModelByUserId(mb.userId);
            if (m == null) { throw new ValidateException("会员编号1不存在"); }
            //置换的会员
            Member m_zh = memberBLL.GetModelByUserId(mb.zh_userId);
            if (m_zh == null) { throw new ValidateException("会员编号2不存在"); }

            //原系谱图信息
            int treePlace = m.treePlace.Value;
            int fatherID = m.fatherID.Value;
            string fatherName = m.fatherName;
            int pLevel = m.pLevel.Value;
            string pPath = m.pPath;

            int treePlace_zh = m_zh.treePlace.Value;
            int fatherID_zh = m_zh.fatherID.Value;
            string fatherName_zh = m_zh.fatherName;
            int pLevel_zh = m_zh.pLevel.Value;
            string pPath_zh = m_zh.pPath;
            if (pPath_zh.Contains("'"+m.id.ToString()+"'"))//置换会员是子
            {
                throw new ValidateException("置换会员" + mb.zh_userId + "不能是会员" + mb.userId + "的子级节点-将产生无限循环并且断开的树");
            }
            //置换会员的子:置换ID->会员ID
            List<Member> u_child = new List<Member>();
            string sql = "select * from Member  " +
                         "  where fatherID=@fatherID and treePlace=@treePlace";
            List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("fatherID", null, m_zh.id))
                                .Add(new DbParameterItem("treePlace", null, mb.new_treePlace.ToString()))
                            .Result();
            u_child = memberBLL.GetMemberList(sql, param, false);
            if (u_child != null)
            {
             string treePlace_Name = "";
             if (mb.new_treePlace.ToString() == "0")
                 treePlace_Name = "左区";
             if (mb.new_treePlace.ToString() == "1")
                 treePlace_Name = "右区";
             if(u_child.Count>0)
             {
             string userName_in = "";
             foreach (Member m_in in u_child)
             {
                 userName_in += " " +m_in.userId+"("+m_in.userName+")";
             }
             throw new ValidateException("置换会员" + mb.zh_userId + "的-" + treePlace_Name + "-已经有子节点-" + userName_in);
             }
            }

            //指定树区
            m.treePlace = mb.new_treePlace;
            //成为置换会员的子
            m.fatherID = m_zh.id;
            m.fatherName = m_zh.userName;
            m.pLevel = pLevel_zh + 1;
            m.pPath = pPath_zh + m_zh.id.ToString() + ",";

            //抓出会员的所有子
            u_child = new List<Member>();
            sql = "select * from Member  " +
                         "  where pPath like '%'+@pPath_like+'%'";
            param = ParamUtil.Get().Add(new DbParameterItem("pPath_like", null, pPath + m.id + ","))
                            .Result();
            u_child = memberBLL.GetMemberList(sql, param, false);
            string id_in = "0";
            if(u_child != null)
            foreach (Member m_in in u_child)
            {
                    id_in += "," + m_in.id.ToString();
            }
            string sql_update_child_path = " update member set pPath=replace(pPath,'" + pPath + m.id.ToString() + ",','" + m.pPath + m.id.ToString() + ",') where id  in (" + id_in + ") "
                 + " update member set pLevel=dbo.Get_StrArrayLength(pPath,',')-2 where id  in (" + id_in + ") ";

            //校验end----------------------------------

            //保存修改安置关系记录
            mb.addTime = DateTime.Now;
            mb.createId = current.id;
            mb.createUser = current.userId;
            mb.new_fatherID = m.fatherID;
            mb.new_fatherName = memberBLL.GetModelById(m.fatherID.Value).userId;
            mb.new_treePlace = m.treePlace;
            mb.old_fatherID = fatherID;
            mb.old_fatherName = memberBLL.GetModelById(fatherID).userId;
            mb.old_treePlace = treePlace;
            mb.uid = m.id;
            mb.userId = m.userId;
            mb.userName = m.userName;
            mb.zh_uid = m_zh.id;
            mb.zh_userId = m_zh.userId;
            mb.zh_userName = m_zh.userName;
            mb.remark = "把" + mb.userId + "点位及其下线移至" + mb.zh_userId + "的" + m.treePlace.Value.ToString()=="0"?"左区":"右区";
            object o = this.SaveByIdentity(mb);
            int newId = Convert.ToInt32(o);
            mb.id = newId;

            //保存操作日志
            OperateLog log = new OperateLog();
            log.recordId = newId;
            log.uid = current.id;
            log.userId = current.userId;
            log.ipAddress = current.ipAddress;
            log.mulx = "{userId:'" + mb.userId + "',"
                + "fatherID:'" + fatherID.ToString() + "',"
                 + "fatherName:'" + fatherName + "',"
                 + "treePlace:'" + treePlace.ToString() + "',"
                  + "pLevel:'" + pLevel.ToString() + "',"
                  + "pPath:'" + pPath + "'"
            + "}-与置换-"
            + "{userId:'" + mb.zh_userId + "',"
                + "fatherID:'" + fatherID_zh.ToString() + "',"
                 + "fatherName:'" + fatherName_zh + "',"
                 + "treePlace:'" + treePlace_zh.ToString() + "',"
                  + "pLevel:'" + pLevel_zh.ToString() + "',"
                  + "pPath:'" + pPath_zh + "'"
            + "}";
            log.tableName = "RelationEdit";
            log.recordName = "置换关系方式二";
            this.SaveOperateLog(log);

            //修改会员信息
            memberBLL.Update(m);

            //sql
            dao.ExecuteBySql(sql_update_child_path);



            if (newId <= 0)
            {
                throw new ValidateException("修改失败，请联系管理员");
            }
            return mb;
        }


        public PageResult<RelationEdit> GetListPage(RelationEdit model)
        {
            PageResult<RelationEdit> page = new PageResult<RelationEdit>();
            string sql = "select *,row_number() over(order by m.id desc) rownumber from RelationEdit m where 1=1 ";
            string countSql = "select count(1) from RelationEdit where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.userName))
                {
                    param.Add(new DbParameterItem("userName", ConstUtil.LIKE, model.userName));
                }
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<RelationEdit> list = new List<RelationEdit>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((RelationEdit)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

    }
}
