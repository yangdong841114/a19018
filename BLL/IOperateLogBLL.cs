﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 操作日志业务逻辑接口
    /// </summary>
    public interface IOperateLogBLL
    {

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <returns></returns>
        PageResult<OperateLog> GetListPage(OperateLog model, Member current);


        /// <summary>
        /// 查询业务类型
        /// </summary>
        /// <returns></returns>
        PageResult<OperateLog> GetListType(Member current);

       
    }
}
