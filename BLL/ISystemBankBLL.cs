﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 公司银行账户业务逻辑接口
    /// </summary>
    public interface ISystemBankBLL : IBaseBLL<SystemBank>
    {

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <returns></returns>
        PageResult<SystemBank> GetListPage(SystemBank model);

        /// <summary>
        /// 添加公司银行记录
        /// </summary>
        /// <param name="model">保存数据</param>
        /// <param name="current">当前会员</param>
        /// <returns></returns>
        int SaveSystemBank(SystemBank model, Member current);

        /// <summary>
        /// 修改公司银行记录
        /// </summary>
        /// <param name="model">修改数据</param>
        /// <param name="current">当前会员</param>
        /// <returns></returns>
        int UpdateSystemBank(SystemBank model, Member current);

        /// <summary>
        /// 获取银行实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        SystemBank GetModel(int id);

    }
}
