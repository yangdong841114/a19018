﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace BLL
{
    /// <summary>
    /// 投资回调记录业务逻辑接口
    /// </summary>
    public interface IInvestBackRecordBLL : IBaseBLL<InvestBackRecord>
    {
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="model">保存数据</param>
        /// <returns></returns>
        void SaveModel(InvestBackRecord model);

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model">修改数据</param>
        /// <returns></returns>
        int UpdateModel(InvestBackRecord model);


    }
}
