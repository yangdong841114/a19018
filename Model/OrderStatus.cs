﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 订单状态记录表实体
    /// </summary>
    [Serializable]
    public class OrderStatus : DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //订单号
        public virtual string hid { get; set; }
        //操作
        public virtual string operate { get; set; }
        //操作时间
        public virtual DateTime? addTime { get; set; }
        //状态
        public virtual int? status { get; set; }
        //操作人ID
        public virtual int? uid { get; set; }
        //操作人编码
        public virtual string userId { get; set; }
       
    }
}
