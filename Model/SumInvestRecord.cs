﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class SumInvestRecord : Page, DtoData
    {
        public virtual string userId { get; set; }
        public virtual string userName { get; set; }

        public virtual double? ethValue { get; set; }
    }
}
