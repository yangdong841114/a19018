﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 投资表实体
    /// </summary>
    [Serializable]
    public class InvestRecord : Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //用户ID
        public virtual int? uid { get; set; }
        //用户编码
        public virtual string userId { get; set; }
        public virtual string userName { get; set; }
        //订单号
        public virtual string OrderId { get; set; }
        //转出地址
        public virtual string fromAddress { get; set; }
        //HASH
        public virtual string HASH { get; set; }
        //金额
        public virtual double? ethValue { get; set; }
        //提交时间
        public virtual DateTime? addTime { get; set; }
        //到帐时间
        public virtual DateTime? getTime { get; set; }
        //1有效0无效
        public virtual int? isAcitve { get; set; }
        //档次ID
        public virtual int? uLevel { get; set; }
        public virtual string uLevelName { get; set; }
        //remark
        public virtual string remark { get; set; }
        //共享销毁期数
        public virtual int? gzxhqs { get; set; }
        //ethTotcs价格
        public virtual double? ethTotcsPrice { get; set; }
        //eth投资
        public virtual double? ethValueTz { get; set; }
        //eth销毁
        public virtual double? ethValueXh { get; set; }
        public virtual double? epointsETHToTCS { get; set; }
        //1已销毁0未销毁
        public virtual int? isXh { get; set; }
        //销毁链上日志
        public virtual string XhLog { get; set; }
        //状态
        public virtual string status { get; set; }

        //查询 条件冗余字段start--------------------------------------------------------
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }


        //查询 条件冗余字段end--------------------------------------------------------

        //审计字段
        //审计日期
        public virtual DateTime? sjTime { get; set; }
        //收入ETH总数
        public virtual double? srETH { get; set; }
        //下发ETH总数
        public virtual double? xfETH { get; set; }
        //收支差额ETH
        public virtual double? szceETH { get; set; }
        //投资本金ETH
        public virtual double? tzbjETH { get; set; }
        //下发OCC总数
        public virtual double? xfTCS { get; set; }
        //销毁OCC总数
        public virtual double? xhTCS { get; set; }
        //审计字段

    }
}
