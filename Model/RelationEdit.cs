﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 修改推荐关系实体
    /// </summary>
    [Serializable]
    public class RelationEdit : Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //用户ID
        public virtual int? uid { get; set; }
        //用户编码
        public virtual string userId { get; set; }
        //用户名称
        public virtual string userName { get; set; }

        //置换用户ID
        public virtual int? zh_uid { get; set; }
        //置换用户编码
        public virtual string zh_userId { get; set; }
        //置换用户名称
        public virtual string zh_userName { get; set; }

        //旧_系谱图区位=> 0：左区，1：右区
        public virtual int? old_treePlace { get; set; }
        //旧_系谱图父节点ID
        public virtual int? old_fatherID { get; set; }
        //旧_系谱图父节点名称
        public virtual string old_fatherName { get; set; }

        //新_系谱图区位=> 0：左区，1：右区
        public virtual int? new_treePlace { get; set; }
        //新_系谱图父节点ID
        public virtual int? new_fatherID { get; set; }
        //新_系谱图父节点名称
        public virtual string new_fatherName { get; set; }

        //修改时间
        public virtual DateTime? addTime { get; set; }
        //创建人ID
        public virtual int? createId { get; set; }
        //创建人编号
        public virtual string createUser { get; set; }
        //备注
        public virtual string remark { get; set; }
        

        //查询 条件start
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }

        //查询条件end

    }
}
