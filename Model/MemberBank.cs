﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 用户银行账户表实体
    /// </summary>
    [Serializable]
    public class MemberBank : DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //用户ID
        public virtual int? uid { get; set; }
        //开户名
        public virtual string bankUser { get; set; }
        //开户行
        public virtual string bankName { get; set; }
        //卡号
        public virtual string bankCard { get; set; }
        //省
        public virtual string province { get; set; }
        //市
        public virtual string city { get; set; }
        //区
        public virtual string area { get; set; }
        //详细地址
        public virtual string address { get; set; } 
       
    }
}
