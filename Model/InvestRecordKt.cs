﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 投资空投表实体
    /// </summary>
    [Serializable]
    public class InvestRecordKt : Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //用户ID
        public virtual int? uid { get; set; }
        public virtual int? uLevel { get; set; }
        //用户编码
        public virtual string userId { get; set; }
        public virtual string userName { get; set; }
        //HASH
        public virtual string HASH { get; set; }
        //总投资
        public virtual double? ethValueTz { get; set; }
        //收益
        public virtual double? ethValueSy { get; set; }
        //亏本
        public virtual double? ethValueKb { get; set; }
        //ethTotcs价格
        public virtual double? ethTotcsPrice { get; set; }
        //空投金额
        public virtual double? TcsKt { get; set; }
        public virtual double? je { get; set; }
        //提交时间
        public virtual DateTime? addTime { get; set; }
        public virtual DateTime? regaddTime { get; set; }
        //1已发0未发
        public virtual int? isSend { get; set; }
        //链上日志
        public virtual string KtLog { get; set; }
        //状态
        public virtual string status { get; set; }
        //类型
        public virtual string ethOrtcs { get; set; }
        //到帐时间
        public virtual DateTime? getTime { get; set; }
        //审核人ID
        public virtual int? auditUid { get; set; }
        //审核人编码
        public virtual string auditUser { get; set; }

        //查询 条件冗余字段start--------------------------------------------------------
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }


        //查询 条件冗余字段end--------------------------------------------------------
    }
}
