﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class OccResponse
    {
        public string code { get; set; }
        public IList<OccResponseData> result { get; set; }
    }
    public class OccResponseData
    { 
        public int id { get; set; }
        public string code { get; set; }
        public string orderId { get; set; }
        public OccResponseResult result { get; set; }
    }
    public class OccResponseResult
    { 
        public string msg { get; set; }
        public string txhash { get; set; }
    }
}
