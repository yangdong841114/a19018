﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 投资回调记录表
    /// </summary>
    [Serializable]
    public class InvestBackRecord : Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //处理状态，0：未处理，1：处理失败，2：处理成功
        public virtual int? flag { get; set; }
        //调用来源IP
        public virtual string invokerIp { get; set; }
        //调用传入参数
        public virtual string cont { get; set; }
        //回调记录写入时间
        public virtual DateTime? addTime { get; set; }
        //回调记录完成时间
        public virtual DateTime? endTime { get; set; }

    }
}
