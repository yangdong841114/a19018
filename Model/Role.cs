﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 角色实体
    /// </summary>
    [Serializable]
    public class Role : Page,DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //角色名称
        public virtual string roleName { get; set; }
        //角色描述
        public virtual string remark { get; set; }

        //角色类型，1：前台角色，2：后台角色
        public virtual int? roleType { get; set; }

        //冗余字段
        //会员ID
        public virtual int? memberId { get; set; }
        //会员编码
        public virtual string userId { get; set; }
        //会员名称
        public virtual string userName { get; set; }
        //角色会员分配关系表id
        public virtual int? rmId { get; set; }

        //查询条件，是否后台会员
        public virtual int? isAdmin { get; set; }

        //菜单ID
        public virtual string resourceId { get; set; }
       
    }
}
