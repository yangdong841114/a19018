﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 会员晋升表实体
    /// </summary>
    [Serializable]
    public class MemberUp :Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //会员ID
        public virtual int? uid { get; set; }
        //会员编码
        public virtual string userId { get; set; }
        //会员名称
        public virtual string userName { get; set; }
        //晋升前级别ID
        public virtual int? oldId { get; set; }
        //晋升后级别ID
        public virtual int? newId { get; set; }
        //缴纳金额
        public virtual double? money { get; set; }
        //申请时间
        public virtual DateTime? addTime { get; set; }
        //状态，0：待审核，1：已通过
        public virtual int? flag { get; set; }
        //创建人ID
        public virtual int? createId { get; set; }
        //创建人编码
        public virtual string createUser { get; set; }
        //审核通过时间
        public virtual DateTime? passTime { get; set; }
        //审核人ID
        public virtual int? auditId { get; set; }
        //审核人编码
        public virtual string auditUser { get; set; }
        //备注
        public virtual string remark { get; set; }

        //1：修改会员级别，2：修改荣誉级别
        public virtual int? cls { get; set; }

        //冗余字段
        public virtual string rowNo { get; set; }

        //荣誉查询条件字段
        public DateTime? startTime { get; set; }

        public DateTime? endTime { get; set; }
    }
}
