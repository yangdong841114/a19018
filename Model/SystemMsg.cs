﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 系统消息实体
    /// </summary>
    [Serializable]
    public class SystemMsg : DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //0:发给后台,其他:发给对应前台会员ID
        public virtual int? toUid { get; set; }
        //地址
        public virtual string url { get; set; }
        public virtual string msg { get; set; }
        public virtual int? isRead { get; set; }

        //关联记录id
        public virtual int? recordId { get; set; }

        //关联表名
        public virtual string recordTable { get; set; }


        //冗余信息
        public virtual string userId { get; set; }
        public virtual string userName { get; set; }
        public virtual string typeName { get; set; }

        public virtual double? money { get; set; }
        public virtual DateTime? addTime { get; set; }

        public virtual int? count { get; set; }
        
    }
}
