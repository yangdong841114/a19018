﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 广告图实体
    /// </summary>
    [Serializable]
    public class Banner : DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //图片地址
        public virtual string imgUrl { get; set; }
        
    }
}
