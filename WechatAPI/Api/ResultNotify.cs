﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WechatAPI.Lib;
using WechatAPI.Mod;

namespace WechatAPI.Api
{
    /// <summary>
    /// 支付结果通知回调处理类
    /// 负责接收微信支付后台发送的支付结果并对订单有效性进行验证，将验证结果反馈给微信支付后台
    /// </summary>
    public class ResultNotify : Notify
    {
        public ResultNotify(HttpRequestBase Request, HttpResponseBase Response)
            : base(Request, Response)
        {
        }

        public override WxPayData ProcessNotify()
        {
            WxPayData notifyData = GetNotifyData();

            //检查支付结果中transaction_id是否存在
            if (!notifyData.IsSet("transaction_id"))
            {
                //若transaction_id不存在，则立即返回结果给微信支付后台
                WxPayData res = new WxPayData();
                res.SetValue("return_code", "FAIL");
                res.SetValue("return_msg", "支付结果中微信订单号不存在");
                Log.Error(this.GetType().ToString(), "The Pay result is error : " + res.ToXml());
                Response.Write(res.ToXml());
                Response.End();
            }

            string transaction_id = notifyData.GetValue("transaction_id").ToString();

            //查询订单，判断订单真实性
            if (!QueryOrder(transaction_id))
            {
                //若订单查询失败，则立即返回结果给微信支付后台
                WxPayData res = new WxPayData();
                res.SetValue("return_code", "FAIL");
                res.SetValue("return_msg", "订单查询失败");
                Log.Error(this.GetType().ToString(), "Order query failure : " + res.ToXml());
                Response.Write(res.ToXml());
                Response.End();
            }
            //查询订单成功
            else
            {
                WxPayData res = new WxPayData();
                //BLL.Charge bc = new BLL.Charge();
                //Model.Charge mc = new Model.Charge();
                //BLL.member bm = new BLL.member();
                //Model.member mm = new Model.member();
                //Model.GeneralJournal mg = new Model.GeneralJournal();
                //BLL.GeneralJournal bg = new BLL.GeneralJournal();
                //string out_trade_no = notifyData.GetValue("out_trade_no").ToString();  //商户订单号
                //string attach = notifyData.GetValue("attach").ToString();   //附加数据商家数据包

                //mc.Content = "微信扫码支付,微信订单号" + out_trade_no;
                //List<ParamModel> prm=new List<ParamModel>();
                //prm.Add(new ParamModel(ParamModel.EQ, "Content", mc.Content.Trim()));

                //if (bc.GetCountByWhere(prm) == 0)
                //{
                //    //Log.Error(this.GetType().ToString(), out_trade_no + "[" + attach + "]");

                //    BLL.Parameter bp = new BLL.Parameter();
                //    Model.Parameter mp = new Model.Parameter();
                //    mp = bp.GetModel(1);

                //    if (string.IsNullOrEmpty(attach))
                //    {
                //        res.SetValue("return_code", "FAIL");
                //        res.SetValue("return_msg", "订单更新失败");
                //        Log.Error(this.GetType().ToString(), "Order query failure : " + res.ToXml());
                //        page.Response.Write(res.ToXml());
                //        page.Response.End();
                //    }

                //    mm = bm.GetModel(int.Parse(attach.Split('|')[0]));  //实体

                //    mc.epoints = double.Parse(attach.Split('|')[1]);
                //    mc.Uid = mm.id;
                //    mc.Userid = mm.UserId;
                //    mc.AddTime = DateTime.Now;
                //    mc.IsPay = 1;
                //    mc.type = int.Parse(attach.Split('|')[2]);
                //    mc.FromBank = "";
                //    mc.BankTime = DateTime.Now.ToString();
                //    mc.bankcard = "";
                //    mc.bankuser = "";
                //    mc.bankname = "微信";
                //    mc.ex = mp.exchange.Split(',')[0];
                //    mc.PassTime = DateTime.Now;

                //    //向平台购买
                //    //if (mc.ctype == 0)
                //    //{
                //    if (bc.AddWx(mc) > 0)
                //    {
                //        //充值成功系统金币和会员金币增加
                //        #region 返还金币
                //        bm.AccountAdd("agentcash", mc.epoints, mc.Uid);  //更新账户

                //        //会员
                //        mg.Uid = mm.id;
                //        mg.userid = mm.UserId;
                //        mg.type = 1;  //电子钱包
                //        mg.Abst = "汇款收入:微信支付";
                //        mg.InCome = mc.epoints;
                //        mg.OutLay = 0;
                //        mg.Last = mm.AgentCash + mc.epoints;
                //        mg.AddTime = Convert.ToDateTime(mc.PassTime);
                //        bg.Add(mg);//插入流水账
                //        #endregion

                //        res.SetValue("return_code", "SUCCESS");
                //        res.SetValue("return_msg", "OK");
                //        Log.Info(this.GetType().ToString(), "order query success : " + res.ToXml());
                //        page.Response.Write(res.ToXml());
                //        page.Response.End();
                //    }
                //    else
                //    {
                //        res.SetValue("return_code", "FAIL");
                //        res.SetValue("return_msg", "订单更新失败");
                //        Log.Error(this.GetType().ToString(), "Order query failure : " + res.ToXml());
                //        page.Response.Write(res.ToXml());
                //        page.Response.End();
                //    }
                //}
            }

            return notifyData;
        }

        //查询订单
        public bool QueryOrder(string transaction_id)
        {
            WxPayData req = new WxPayData();
            req.SetValue("transaction_id", transaction_id);
            WxPayData res = WxPay.OrderQuery(req);
            if (res.GetValue("return_code").ToString() == "SUCCESS" &&
                res.GetValue("result_code").ToString() == "SUCCESS")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
