﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 已开通会员Controller
    /// </summary>
    public class UMemberPassedController : Controller
    {
        public IMemberBLL memberBLL { get; set; }
        public ICurrencyBLL cyBll { get; set; }
        public IInvestRecordBLL irBLL { get; set; }
        public IInvestRecordKtBLL irktBLL { get; set; }

        /// <summary>
        /// 分页查询已开通列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult GetListPage(Member model)
        {
            if (model == null)
            {
                model = new Member();
            }
            //当前登录用户
            Member mb = (Member)Session["MemberUser"];
            model.isPay = 1;
            model.shopid = mb.id;
            string fields = "id,userId,userName,uLevel,regMoney,phone," +
                            "reName,fatherName,passTime,isLock,addTime,isFt";
            PageResult<Member> page = memberBLL.GetMemberListPage(model, fields);
            return Json(page, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 获取主页数据
        /// </summary>
        /// <returns></returns>
        public JsonResult GetMainData()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                if (mb != null)
                {
                    int row = 5;
                    mb = memberBLL.GetModelById(mb.id.Value);

                    Dictionary<string, object> di = new Dictionary<string, object>();
                    di["userName"] = mb.userName;
                    di["ulevel"] = mb.uLevel;
                    di["userStatus"] = mb.status;
                    di["retreecount1"] = mb.reCount;
                    di["retreecount"] = mb.reTreeCount;
                    di["retreecount2"] = mb.reTreeCount2;
                    di["retreecount3"] = mb.reTreeCount3;

                    //投资记录
                    InvestRecord query1 = new InvestRecord();
                    query1.page = 1;
                    query1.rows = row;
                    query1.userId = mb.userId;
                    query1.isAcitve = 1;
                    PageResult<InvestRecord> page1 = irBLL.GetListPage(query1);
                    di["investList"] = page1;

                    //贡献排行
                    InvestRecord query2 = new InvestRecord();
                    query2.page = 1;
                    query2.rows = row;
                    query2.isAcitve = 1;
                    PageResult<SumInvestRecord> page2 = irBLL.GetListPageGroupBy(query2);
                    di["sortList"] = page2;

                    //奖金记录
                    CurrencySum query3 = new CurrencySum();
                    query3.page = 1;
                    query3.rows = row;
                    query3.uid = mb.id;
                    PageResult<CurrencySum> page3 = cyBll.GetByUserSumCurrency(query3);
                    di["cryList"] = page3;
                    response.map = di;
                    response.Success();
                }
                else
                {
                    response.msg = "请您先登录!!";
                }

            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "加载数据失败，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 投资记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult GetListPageInvestYx(InvestRecord model)
        {
            Member mb = (Member)Session["MemberUser"];
            if (model == null) { model = new InvestRecord(); }
            model.userId = mb.userId;
            model.isAcitve = 1;
            PageResult<InvestRecord> page = irBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 贡献排行
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult GetListPageGroupBy(InvestRecord model)
        {
            Member mb = (Member)Session["MemberUser"];
            if (model == null) { model = new InvestRecord(); }
            model.isAcitve = 1;
            PageResult<SumInvestRecord> page = irBLL.GetListPageGroupBy(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 分页查询（按用户汇总某天的数据)
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult GetByUserSumCurrency(CurrencySum model)
        {
            Member user = (Member)Session["MemberUser"];
            if (model == null) { model = new CurrencySum(); }
            model.uid = user.id;
            PageResult<CurrencySum> result = cyBll.GetByUserSumCurrency(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }
        
    }
}
