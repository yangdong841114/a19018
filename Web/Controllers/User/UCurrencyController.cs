﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 奖金查询Controller
    /// </summary>
    public class UCurrencyController : Controller
    {
        public ICurrencyBLL cyBll { get; set; }

        /// <summary>
        /// 分页查询（按用户汇总某天的数据)
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult GetByUserSumCurrency(CurrencySum model)
        {
            Member user = (Member)Session["MemberUser"];
            if (model == null) { model = new CurrencySum(); }
            model.uid = user.id;
            PageResult<CurrencySum> result = cyBll.GetByUserSumCurrency(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }

        //获取用户汇总数据
        public JsonResult GetUserTotal(CurrencySum model)
        {
            ResponseDtoList<CurrencySum> response = new ResponseDtoList<CurrencySum>("fail");
            try
            {
                Member user = (Member)Session["MemberUser"];
                if (model == null) { model = new CurrencySum(); }
                model.uid = user.id;
                List<CurrencySum> list = cyBll.GetByUserSumCurrencyFooter(model);
                response.Success();
                response.list = list;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "查询失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
            

        }

        /// <summary>
        /// 查询奖金明细记录（按某个会员)
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult GetDetailListPage(Currency model)
        {
            Member user = (Member)Session["MemberUser"];
            if (model == null) { model = new Currency(); }
            model.userId = user.userId;
            try
            { string addDate = Session["addDate"].ToString(); model.addDate = DateTime.Parse(addDate); }
            catch
            { }
          
          
            PageResult<Currency> result = cyBll.GetDetailListPage(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }


        public ActionResult UCurrencyUserDetail(string id)
        {
            string addDate = id;
            Session["addDate"] = addDate;
            Response.Redirect("/wap/User/indexUCurrencyUserDetail.html");
            return null;
        }

        public ActionResult UCurrencyUserDetail_en(string id)
        {
            string addDate = id;
            Session["addDate"] = addDate;
            Response.Redirect("/wap/User/indexUCurrencyUserDetail_en.html");
            return null;
        }

        public ActionResult UCurrencyUserDetail_jp(string id)
        {
            string addDate = id;
            Session["addDate"] = addDate;
            Response.Redirect("/wap/User/indexUCurrencyUserDetail_jp.html");
            return null;
        }

        public ActionResult UCurrencyUserDetail_ko(string id)
        {
            string addDate = id;
            Session["addDate"] = addDate;
            Response.Redirect("/wap/User/indexUCurrencyUserDetail_ko.html");
            return null;
        }

        public ActionResult UCurrencyUserDetail_po(string id)
        {
            string addDate = id;
            Session["addDate"] = addDate;
            Response.Redirect("/wap/User/indexUCurrencyUserDetail_po.html");
            return null;
        }
        

    }
}
