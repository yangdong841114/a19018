﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.Data;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 已开通会员Controller
    /// </summary>
    public class MemberPassedController : Controller
    {
        public IMemberBLL memberBLL { get; set; }

        public IMobileNoticeBLL noticeBLL { get; set; }
        public IInvestRecordBLL irBLL { get; set; }
        public IInvestRecordKtBLL irktBLL { get; set; }

        /// <summary>
        /// 群发短信
        /// </summary>
        /// <param name="phones">群发短信的电话列表</param>
        /// <param name="msg">发送信息</param>
        /// <param name="flag">1：勾选群发，2：群发所有</param>
        /// <returns></returns>
        public JsonResult SendMessage(List<string> phones,string msg,int flag)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                if (flag == 2)
                {
                    List<Member> list = memberBLL.GetList("select phone from Member where id>1");
                    if (list != null && list.Count > 0)
                    {
                        phones = new List<string>();
                        foreach (Member m in list)
                        {
                            phones.Add(m.phone);
                        }
                    }
                }
                noticeBLL.SendMessage(phones, msg);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "发送失败，请联系管理员"; }
            
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }


        public JsonResult GetListPageInvestYx(InvestRecord model)
        {
            model.isAcitve = 1;
            PageResult<InvestRecord> page = irBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListPageInvestWx(InvestRecord model)
        {
            model.isAcitve = -1;
            PageResult<InvestRecord> page = irBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListPageInvestSj(InvestRecord model)
        {
            PageResult<InvestRecord> page = irBLL.GetListPageSj(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageInvestKt(InvestRecordKt model)
        {
            PageResult<InvestRecordKt> page = irktBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        
        
        public ActionResult ExportExcelInvestYx(InvestRecord model)
        {
            model.isAcitve = 1;
            DataTable dt = irBLL.GetExcelListPageInvestYx(model);

            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("OrderId", "OrderId");
            htb.Add("userName", "会员ID");
            htb.Add("fromAddress", "钱包地址");
            htb.Add("ethValue", "投资金额ETH");
            htb.Add("ethValueTz", "投资本金ETH");
            htb.Add("epointsETHToTCS", "销毁OCC数量");
            htb.Add("status", "状态");
            htb.Add("addTime", "投资日期");
            htb.Add("HASH", "HASH");
          

            string FilePath = Server.MapPath("~/Admin/excel/");
            DataToExcel dte = new DataToExcel();
            string filename = "";
            try
            {
                //if (dt.Rows.Count > 0)
                //{
                filename = dte.DataExcel(dt, "标题", FilePath, htb);
                //}
                //return File(FilePath + filename, "application/javascript", filename);

                dte.CreateExcel(dt, "application/ms-excel", filename, htb);
                return File(FilePath + filename, "application/javascript", filename);
            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }

        }

        public ActionResult ExportExcelInvestKt(InvestRecordKt model)
        {
          

            DataTable dt = irktBLL.GetExcelListPageInvestKt(model);

            System.Collections.Hashtable htb = new System.Collections.Hashtable();
       
            htb.Add("userName", "会员ID");
            htb.Add("userId", "钱包地址");
            htb.Add("ethValueTz", "投资本金ETH");
            htb.Add("ethValueSy", "总收益ETH");
            htb.Add("ethOrtcs", "退款类型");
            htb.Add("status", "状态");
            htb.Add("regaddTime", "注册日期");
            htb.Add("addTime", "退款日期");
            htb.Add("getTime", "到帐日期");
            htb.Add("KtLog", "接口日志");
            htb.Add("HASH", "HASH");
            htb.Add("auditUser", "操作人");
            htb.Add("je", "金额");
            htb.Add("uLevelName", "级别");

            string FilePath = Server.MapPath("~/Admin/excel/");
            DataToExcel dte = new DataToExcel();
            string filename = "";
            try
            {
                //if (dt.Rows.Count > 0)
                //{
                filename = dte.DataExcel(dt, "标题", FilePath, htb);
                //}
                //return File(FilePath + filename, "application/javascript", filename);

                dte.CreateExcel(dt, "application/ms-excel", filename, htb);
                return File(FilePath + filename, "application/javascript", filename);
            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }

        }
        

        public ActionResult ExportExcelInvestWx(InvestRecord model)
        {
            model.isAcitve = -1;
            DataTable dt = irBLL.GetExcelListPageInvestYx(model);

            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("OrderId", "OrderId");
            htb.Add("userName", "会员ID");
            htb.Add("fromAddress", "钱包地址");
            htb.Add("ethValue", "投资金额ETH");
            htb.Add("ethValueTz", "投资本金ETH");
            htb.Add("epointsETHToTCS", "销毁OCC数量");
            htb.Add("status", "状态");
            htb.Add("addTime", "投资日期");
            htb.Add("HASH", "HASH");


            string FilePath = Server.MapPath("~/Admin/excel/");
            DataToExcel dte = new DataToExcel();
            string filename = "";
            try
            {
                //if (dt.Rows.Count > 0)
                //{
                filename = dte.DataExcel(dt, "标题", FilePath, htb);
                //}
                //return File(FilePath + filename, "application/javascript", filename);

                dte.CreateExcel(dt, "application/ms-excel", filename, htb);
                return File(FilePath + filename, "application/javascript", filename);
            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }

        }

        public ActionResult ExportExcelInvestSj(InvestRecord model)
        {
            model.isAcitve = -1;
            DataTable dt = irBLL.GetExcelListPageInvestSj(model);

            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("userName", "会员ID");
            htb.Add("userId", "钱包地址");
            htb.Add("srETH", "收入ETH总数");
            htb.Add("xfETH", "下发ETH总数");
            htb.Add("szceETH", "收支差额ETH");
            htb.Add("tzbjETH", "投资本金ETH");
            htb.Add("xfTCS", "下发OCC总数");
            htb.Add("xhTCS", "销毁OCC总数");
            htb.Add("sjTime", "审计日期");
            htb.Add("addTime", "投资日期");


            string FilePath = Server.MapPath("~/Admin/excel/");
            DataToExcel dte = new DataToExcel();
            string filename = "";
            try
            {
                //if (dt.Rows.Count > 0)
                //{
                filename = dte.DataExcel(dt, "标题", FilePath, htb);
                //}
                //return File(FilePath + filename, "application/javascript", filename);

                dte.CreateExcel(dt, "application/ms-excel", filename, htb);
                return File(FilePath + filename, "application/javascript", filename);
            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }

        }

        public ActionResult ExportExcelMemberHb(Member model)
        {
            model.isPay = 1;
            model.isHB = 1;
            DataTable dt = memberBLL.GetExcelListPageMemberHb(model);
       
            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("userName", "会员ID");
            htb.Add("userId", "钱包地址");
            htb.Add("uLevelName", "级别");
            htb.Add("ethValueTz", "投资总额ETH");
            htb.Add("SumCurrency", "总收益ETH");
            htb.Add("SumHBETH", "回本差额ETH");
            htb.Add("SumHBTCS", "回本差额OCC");

            htb.Add("reuserName", "推荐人ID");
            htb.Add("reName", "推荐人钱包");
            htb.Add("status", "状态");

            htb.Add("addTime", "注册日期");


            string FilePath = Server.MapPath("~/Admin/excel/");
            DataToExcel dte = new DataToExcel();
            string filename = "";
            try
            {
                //if (dt.Rows.Count > 0)
                //{
                filename = dte.DataExcel(dt, "标题", FilePath, htb);
                //}
                //return File(FilePath + filename, "application/javascript", filename);

                dte.CreateExcel(dt, "application/ms-excel", filename, htb);
                return File(FilePath + filename, "application/javascript", filename);
            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }

        }


        

        


        /// <summary>
        /// 分页查询已开通列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult GetListPage(Member model)
        {
            if (model == null)
            {
                model = new Member();
            }
            model.isPay = 1;

            string fields = "id,userId,userName,uLevel,rLevel,regMoney,bankName,bankCard,bankUser,code,phone,address,status," +
                            "reName,reuserName,addTime,byopen,byopenId,empty,isLock,isFt";
            PageResult<Member> page = memberBLL.GetMemberListPage(model, fields);
            return Json(page, JsonRequestBehavior.AllowGet); ;
        }


        public JsonResult GetListPageYc(Member model)
        {
            if (model == null)
            {
                model = new Member();
            }
            model.isPay = 1;
            model.isYC = 1;
            string fields = "id,userId,userName,uLevel,ethValueYx,SumCurrencyETH,SumCurrencyXfETH,SumCurrencyTCS,SumCurrencyXfTCS,code,phone,address,status," +
                            "reName,reuserName,addTime,byopen,byopenId,empty,isLock,isFt";
            PageResult<Member> page = memberBLL.GetMemberListPage(model, fields);
            return Json(page, JsonRequestBehavior.AllowGet); ;
        }

        public JsonResult GetListPageHb(Member model)
        {
            if (model == null)
            {
                model = new Member();
            }
            model.isPay = 1;
            model.isHB = 1;
            string fields = "id,userId,userName,uLevel,ethValueTz,ethValueTz_Wcj,SumCurrency,SumCurrencyETHWcj,ethValueTz_Wcj-SumCurrencyETHWcj as SumHBETH,code,phone,address,status," +
                            "reName,reuserName,addTime,byopen,byopenId,empty,isLock,isFt";
            PageResult<Member> page = memberBLL.GetMemberListPage(model, fields);
            return Json(page, JsonRequestBehavior.AllowGet); ;
        }


        public JsonResult GetListPageOutRecord(Member model)
        {
            if (model == null)
            {
                model = new Member();
            }
            PageResult<Member> page = memberBLL.GetOutRecordListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet); ;
        }


        public JsonResult TceETH(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            if (id == null || id == 0)
            {
                response.msg = "要操作的会员为空";
            }
            else
            {
                try
                {
                    InvestRecordKt model = new InvestRecordKt();
                    model.uid = id;
                    Member select_mem = memberBLL.GetModelById(id);
                    model.userId = select_mem.userId;
                    model.ethOrtcs = "ETH";
                    Member mb = (Member)Session["LoginUser"];
                    int c = irktBLL.SaveInvestRecordKt(model, mb);
                    response.msg = "操作成功";
                    response.status = "success";
                }
                catch (ValidateException va) { response.msg = va.Message; }
                catch (Exception)
                {
                    response.msg = "操作失败！请联系管理员";
                }
            }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        public JsonResult TceOCC(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            if (id == null || id == 0)
            {
                response.msg = "要操作的会员为空";
            }
            else
            {
                try
                {
                    InvestRecordKt model = new InvestRecordKt();
                    model.uid = id;
                    Member select_mem = memberBLL.GetModelById(id);
                    model.userId = select_mem.userId;
                    model.ethOrtcs = "OCC";
                    Member mb = (Member)Session["LoginUser"];
                    int c = irktBLL.SaveInvestRecordKt(model, mb);
                    response.msg = "操作成功";
                    response.status = "success";
                }
                catch (ValidateException va) { response.msg = va.Message; }
                catch (Exception)
                {
                    response.msg = "操作失败！请联系管理员";
                }
            }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }
        
        /// <summary>
        /// 冻结/启用会员
        /// </summary>
        /// <param name="id">会员ID</param>
        /// <param name="isLock">0:启用,1:冻结,2:重置密码</param>
        /// <returns></returns>
        public JsonResult LockMember(List<int> ids, int isLock)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            if (ids == null || ids.Count == 0)
            {
                response.msg = "要操作的会员为空";
            }
            else
            {
                try
                {
                    int c = memberBLL.UpdateLock(ids, isLock);
                    response.msg = "操作成功";
                    response.status = "success";
                }
                catch (ValidateException va) { response.msg = va.Message; }
                catch (Exception)
                {
                    response.msg = "操作失败！请联系管理员";
                }
            }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 登录前台
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult ToForward(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member m = memberBLL.GetModelById(id);
                if (m != null)
                {
                    m.password = null;
                    m.passOpen = null;
                    m.threepass = null;
                    m.answer = null;
                    m.question = null;
                    Session["MemberUser"] = m;
                    response.Success();
                }
                else
                {
                    throw new ValidateException("用户不存在");
                }
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        //导出开通会员excel
        public ActionResult ExportOpenExcel()
        {
            int type = 1;//已开通
            DataTable dt = memberBLL.GetMemberExcelList(type);

            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("userId", "会员编号");
            htb.Add("userName", "会员名称");
            htb.Add("addTime", "注册日期");
            htb.Add("fatherName", "接点人编号");
            htb.Add("reName", "推荐人编号");
            htb.Add("shopName", "报单中心");
            htb.Add("uLevel", "会员等级");
            htb.Add("regMoney", "注册金额");
            htb.Add("phone", "联系电话");
            htb.Add("byopen", "操作人");
            htb.Add("passTime", "开通时间");


            DataToExcel dte = new DataToExcel();
            //string FilePath = dte.DataToExcel1(dt, "a");//
            string FilePath = Server.MapPath("~/Admin/excel/");

            string filename = "";
            try
            {
                //if (dt.Rows.Count > 0)
                //{
                    //CreateExcel(dtexcel, "application/ms-excel", excel);

                    filename = dte.DataExcel(dt, "标题", FilePath, htb);
                //}
                dte.CreateExcel(dt, "application/ms-excel", filename, htb);
                return File(FilePath + filename, "application/javascript", filename);

            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }

        }

    }
}
