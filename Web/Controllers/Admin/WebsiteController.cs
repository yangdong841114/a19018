﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 前台设置
    /// </summary>
    public class WebsiteController : Controller
    {
        public IBaseSetBLL setBLL { get; set; }


        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult InitView()
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                BaseSet model = setBLL.GetModel(); //基础配置
                response.result = model;
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "加载失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 保存设置
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult Save(BaseSet model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                int c = setBLL.SaveModel(model);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }
            
            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}
