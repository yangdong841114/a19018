﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 省市区Controller
    /// </summary>
    public class AreaController : Controller
    {
        public IAreaBLL areaBLL { get; set; }

        /// <summary>
        /// 查询省市区
        /// </summary>
        /// <returns></returns>
        public JsonResult GetList()
        {
            ResponseDtoList<Area> response = new ResponseDtoList<Area>();
            try
            {
                List<Area> result = areaBLL.GetList();
                response.list = result;
            }
            catch (Exception)
            {
                response.status = "fail";
                response.msg = "加载失败，请联系管理员";
            }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 保存或更新记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult SaveOrUpdate(Area model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                if (model == null) { throw new ValidateException("操作对象为空"); }

                Area r = null;
                if (ValidateUtils.CheckIntZero(model.id))
                {
                    r = areaBLL.Save(model);
                    response.msg = "添加成功";
                }
                else
                {
                    r = areaBLL.Update(model);
                    response.msg = "更新成功";
                }
                response.result = r;
                response.status = "success";
            }
            catch (ValidateException e) { response.msg = e.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 删除节点以及节点下的所有子节点
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult Delete(int id)
        {
            ResponseDtoData response = new ResponseDtoData();
            try
            {
                response.msg = areaBLL.Delete(id);
                response.Success();
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.Fail(); response.msg = "操作失败，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        } 
    }
}
