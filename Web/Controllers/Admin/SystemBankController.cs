﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    public class SystemBankController : Controller
    {
        public ISystemBankBLL sbBll { get; set; }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(SystemBank model)
        {
            PageResult<SystemBank> result = sbBll.GetListPage(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 保存或更新记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult SaveOrUpdate(SystemBank model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["LoginUser"]; //当前登录用户
                if (model != null && !ValidateUtils.CheckIntZero(model.id)) {
                    sbBll.UpdateSystemBank(model, current);
                }
                else
                {
                    sbBll.SaveSystemBank(model, current);
                }
                response.msg = "操作成功";
                response.status = "success";
            }
            catch (ValidateException e){response.msg = e.Message;}
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 删除记录
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public JsonResult Delete(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                if (ValidateUtils.CheckIntZero(id)) { throw new ValidateException("删除的记录为空"); }
                sbBll.Delte("SystemBank", id);
                response.status = "success";
                response.msg = "删除成功";
            }
            catch (ValidateException e) { response.msg = e.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}
