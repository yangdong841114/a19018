﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 下载专区Controller
    /// </summary>
    public class DownLoadFileController : Controller
    {
        public IDownLoadFileBLL dlBLL { get; set; }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(DownLoadFile model)
        {
            PageResult<DownLoadFile> result = dlBLL.GetListPage(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 上传文件
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult UploadFile(HttpPostedFileBase file, DownLoadFile model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                
                Member login = (Member)Session["LoginUser"]; //当前登录用户
                if (file == null) { throw new ValidateException("请上传文件"); }
                string oldFileName = file.FileName;
                int lastIndex = oldFileName.LastIndexOf(".");
                string suffix = oldFileName.Substring(lastIndex, oldFileName.Length - lastIndex); //扩展名
                if (suffix != ".rar" && suffix != ".zip")
                {
                    throw new ValidateException("只能上传rar、zip格式的压缩文件");
                }

                if (file.ContentLength > (1024 * 1024 * 5)) { throw new ValidateException("文件大小不能超过5M"); }
                TimeSpan ts = DateTime.Now - DateTime.Parse("1970-01-01 00:00:00");
                string endUrl = "file_"+ ts.Ticks + suffix;
                string newFileName = Server.MapPath("~/UpLoad/download/") + endUrl;
                file.SaveAs(newFileName);
                model.fileUrl = "/UpLoad/download/" + endUrl;
                dlBLL.Save(model, login);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Delete(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                dlBLL.Delete(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}
