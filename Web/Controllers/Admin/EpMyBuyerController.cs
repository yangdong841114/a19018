﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// EP回购记录Controller
    /// </summary>
    public class EpMyBuyerController : Controller
    {
        //public　IEpSaleRecordBLL epsBLL { get; set; }

        public IEpBuyRecordBLL epbBLL { get; set; }


        /// <summary>
        /// 分页查询系统回购的买单列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult GetListPage(EpBuyRecord model)
        {
            if (model == null)
            {
                model = new EpBuyRecord();
            }

            model.uid = 1;
            PageResult<EpBuyRecord> page = epbBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet); ;
        }


        /// <summary>
        /// 取消买单记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult SaveCancel(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["LoginUser"];
                epbBLL.SaveCancel(id, current);
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }


        /// <summary>
        /// 买家确认付款
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult PayMoney(EpBuyRecord model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["LoginUser"];
                epbBLL.SavePay(model, current, true);
                //epbBLL.SaveSurePay(model, current);
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 批量付款
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult BatchPayMoney(List<EpBuyRecord> list)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["LoginUser"];
                epbBLL.SaveBatchPay(list, current, true);
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
