﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.IO;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 数据库管理Controller
    /// </summary>
    public class DbManageController : Controller
    {
        public IBaseSetBLL setBLL { get; set; }
        private string baseDir = "~/Admin/bakup";

        /// <summary>
        /// 分页查询数据库备份
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult GetBackModelListPage(BackModel model)
        {
            PageResult<BackModel> page = setBLL.GetBackModelListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 清空数据库
        /// </summary>
        /// <returns></returns>
        public JsonResult ClearDb()
        {
            ResponseData response = new ResponseData("fail");
            try
            {
                setBLL.ClearDataBase();
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception){response.msg="清空数据库失败，请联系管理员";}
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 备份数据库
        /// </summary>
        /// <returns></returns>
        public JsonResult BakDb()
        {
            ResponseData response = new ResponseData("fail");
            try
            {
                string path = Server.MapPath(baseDir);
                setBLL.BackUpDb(path);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception){response.msg="备份失败，请联系管理员";}
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 还原备份
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public JsonResult reBackDb(string fileName)
        {
            ResponseData response = new ResponseData("fail");
            try
            {
                string path = Server.MapPath(baseDir+"/" + fileName);
                BackUpSystem.SQLBack(path);
                response.Success();

            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception e) { response.msg = e.Message; } //"还原数据库失败，请联系管理员";}
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult Delete(int id)
        {
            ResponseData response = new ResponseData("fail");
            try
            {
                string path = Server.MapPath(baseDir);
                setBLL.deleteBak(id, path);
                response.Success();

            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "删除失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 下载数据库备份
        /// </summary>
        /// <returns></returns>
        public ActionResult DownLoadFile(string id)
        {
            try
            {
                string path = Server.MapPath(baseDir + "/" + id);
                return File(path, "application/octet-stream", id);
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
