﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 待开通报单中心Controller
    /// </summary>
    public class ShopPassingController : Controller
    {
        //会员业务处理接口
        public IMemberBLL memberBLL { get; set; }

        public IShopSetBLL shopBLL { get; set; }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(Member model)
        {
            if (model == null)
            {
                model = new Member();
            }
            model.isAgent = 1;
            string fields = "id,userId,userName,uLevel,regAgentmoney,applyAgentTime,isAgent";
            PageResult<Member> page = memberBLL.GetMemberListPage(model, fields);
            return Json(page, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 开通报单中心
        /// </summary>
        /// <param name="model">保存对象</param>
        /// <returns></returns>
        public JsonResult Save(int memberId)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["LoginUser"]; //当前登录用户
                shopBLL.OpenAgent(memberId, current);
                response.msg = "开通成功";
                response.status = "success";
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "开通失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 删除待开通报单中心
        /// </summary>
        /// <param name="memberId"></param>
        /// <returns></returns>
        public JsonResult Delete(int memberId)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                shopBLL.DeleteAgent(memberId);
                response.msg = "删除成功";
                response.status = "success";
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "删除失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
