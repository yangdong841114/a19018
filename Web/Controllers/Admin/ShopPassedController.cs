﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 已开通报单中心Controller
    /// </summary>
    public class ShopPassedController : Controller
    {
        //会员业务处理接口
        public IMemberBLL memberBLL { get; set; }

        public IShopSetBLL shopBLL { get; set; }

        /// <summary>
        /// 分页查询报单中心的会员列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult GetShopMemberList(Member model)
        {
            if (model == null || ValidateUtils.CheckIntZero(model.shopid))
            {
                throw new ValidateException("报单中心ID为空");
            }

            string fields = "id,userId,userName,uLevel,regMoney,bankName,bankCard,bankUser,code,phone,address,qq," +
                            "reName,shopName,passTime,byopen,byopenId,empty,isLock,isPay";
            PageResult<Member> page = memberBLL.GetMemberListPage(model, fields);
            return Json(page, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(Member model)
        {
            if (model == null)
            {
                model = new Member();
            }
            model.isAgent = 2;
            string fields = "id,userId,userName,uLevel,regAgentmoney,applyAgentTime,openAgentTime,agentOpUser,agentIslock,isAgent";
            PageResult<Member> page = memberBLL.GetMemberListPage(model, fields);
            return Json(page, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 冻结/启用报单中心
        /// </summary>
        /// <param name="ids">会员ID列表</param>
        /// <param name="isLock">0:启用,1:冻结</param>
        /// <returns></returns>
        public JsonResult LockShop(List<int> ids, int isLock)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            if (ids == null || ids.Count == 0)
            {
                response.msg = "要操作的报单中心为空";
            }
            else
            {
                try
                {
                    int c = shopBLL.UpdateLock(ids, isLock);
                    response.msg = "操作成功";
                    response.status = "success";
                }
                catch (ValidateException va) { response.msg = va.Message; }
                catch (Exception)
                {
                    response.msg = "操作失败！请联系管理员";
                }
            }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
