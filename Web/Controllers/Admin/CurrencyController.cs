﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.Data;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 奖金查询Controller
    /// </summary>
    public class CurrencyController : Controller
    {
        public ICurrencyBLL cyBll { get; set; }
        public IInvestRecordBLL irBLL { get; set; }

        public static readonly string occSignCode = System.Configuration.ConfigurationManager.AppSettings["OCC_signCode"];
        public static readonly string occSecretKey = System.Configuration.ConfigurationManager.AppSettings["OCC_secretKey"];
        public static readonly string occUrl = System.Configuration.ConfigurationManager.AppSettings["OCC_Url"];
        public static readonly string occTestUrl = System.Configuration.ConfigurationManager.AppSettings["OCC_testUrl"];
        public static readonly string isOccTest = System.Configuration.ConfigurationManager.AppSettings["isOccTest"];
        /// <summary>
        /// 分页查询（按日期汇总)
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult GetAllSumCurrency(CurrencySum model)
        {
            PageResult<CurrencySum> result = cyBll.GetAllSumCurrency(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }

        public JsonResult GetAllSumCurrencyXf(CurrencySum model)
        {
            PageResult<CurrencySum> result = cyBll.GetAllSumCurrencyXf(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }


        /// <summary>
        /// 分页查询（按用户汇总某天的数据)
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult GetByUserSumCurrency(CurrencySum model)
        {
            PageResult<CurrencySum> result = cyBll.GetByUserSumCurrency(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 查询奖金明细记录（按某个会员)
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult GetDetailListPage(Currency model)
        {
            PageResult<Currency> result = cyBll.GetDetailListPage(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult GetDetailListPageXf(Currency model)
        {
            PageResult<Currency> result = cyBll.GetDetailListPageXf(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }

        public JsonResult AuditXFETH(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["LoginUser"];
                int c = cyBll.AuditXFETH(id, mb);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "审核失败，请联系管理员！"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AuditXFETH_KT(int id)
        {

            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["LoginUser"];
                var url = isOccTest == "1" ? occTestUrl : occUrl;
                var isLocalhost = isOccTest == "1" ? 1 : Request.Url.Host.IndexOf("occmny.io");
                if (isLocalhost != -1)
                {
                    irBLL.CurrencyToOcc(id, occSignCode, url, mb);
                    response.msg = "成功";
                    response.Success();
                }
                //int c = cyBll.AuditXFETH_KT(id, mb);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception ex) { response.msg = ex.Message; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AuditXFOCC(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["LoginUser"];
                int c = cyBll.AuditXFOCC(id, mb);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "审核失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        //导出奖金excel
        public ActionResult ExportBonusExcel()
        {
            DataTable dt = cyBll.GetBounsExcel();

            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("addDate", "结算日期");
            htb.Add("cat1", "共建奖励");
            htb.Add("cat2", "节点维护奖励");
            htb.Add("cat3", "准创始人奖励");
            htb.Add("yf", "应发");
            htb.Add("epointsTCS", "转OCC");
            htb.Add("epointsETHToTCS", "转OCC数量");
            htb.Add("sf", "实发");
            htb.Add("fee1", "手续费");
            htb.Add("getETH", "到帐ETH");

            DataToExcel dte = new DataToExcel();
            //string FilePath = dte.DataToExcel1(dt, "a");//
            string FilePath = Server.MapPath("~/Admin/excel/");

            string filename = "";
            try
            {
                //if (dt.Rows.Count > 0)
                //{
                //CreateExcel(dtexcel, "application/ms-excel", excel);

                filename = dte.DataExcel(dt, "标题", FilePath, htb);
                //}
                dte.CreateExcel(dt, "application/ms-excel", filename, htb);
                return File(FilePath + filename, "application/javascript", filename);

            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }

        }

        public ActionResult ExportBonusExcelXf()
        {
            DataTable dt = cyBll.GetBounsExcelXf();

            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("addDate", "结算日期");
            htb.Add("XfzsETH", "ETH总数");
            htb.Add("XfzsYxfETH", "已下发ETH");
            htb.Add("XfzsDxfETH", "待下发ETH");
            htb.Add("XfzsTCS", "OCC总数");
            htb.Add("XfzsYxfTCS", "已下发OCC");
            htb.Add("XfzsDxfTCS", "待下发OCC");


            DataToExcel dte = new DataToExcel();
            //string FilePath = dte.DataToExcel1(dt, "a");//
            string FilePath = Server.MapPath("~/Admin/excel/");

            string filename = "";
            try
            {
                //if (dt.Rows.Count > 0)
                //{
                //CreateExcel(dtexcel, "application/ms-excel", excel);

                filename = dte.DataExcel(dt, "标题", FilePath, htb);
                //}
                dte.CreateExcel(dt, "application/ms-excel", filename, htb);
                return File(FilePath + filename, "application/javascript", filename);

            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }

        }

        /// <summary>
        /// 导出奖金excel（按用户汇总某天的数据)
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public ActionResult ExportBonusDetailExcel(CurrencySum model)
        {
            DataTable dt = cyBll.GetBounsDetailExcel(model);

            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("userId", "会员编号");
            htb.Add("userName", "会员名称");
            htb.Add("addDate", "结算日期");
            htb.Add("cat1", "共建奖励");
            htb.Add("cat2", "节点维护奖励");
            htb.Add("cat3", "准创始人奖励");
            htb.Add("yf", "应发");
            htb.Add("epointsTCS", "转OCC");
            htb.Add("epointsETHToTCS", "转OCC数量");
            htb.Add("sf", "实发");

            htb.Add("fee1", "手续费");
            htb.Add("getETH", "到帐ETH");


            DataToExcel dte = new DataToExcel();
            //string FilePath = dte.DataToExcel1(dt, "a");//
            string FilePath = Server.MapPath("~/Admin/excel/");

            string filename = "";
            try
            {
                //if (dt.Rows.Count > 0)
                //{
                //CreateExcel(dtexcel, "application/ms-excel", excel);

                filename = dte.DataExcel(dt, "标题", FilePath, htb);
                //}
                dte.CreateExcel(dt, "application/ms-excel", filename, htb);
                return File(FilePath + filename, "application/javascript", filename);

            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }

        }


        public ActionResult ExportBonusDetailExcelXf(Currency model)
        {
            DataTable dt = cyBll.GetBounsDetailExcelXf(model);




            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("userId", "钱包地址");
            htb.Add("addDate", "下发日期");
            htb.Add("userName", "会员ID");

            htb.Add("epointsETH", "ETH数量");
            htb.Add("isxfETH", "ETH状态");
            htb.Add("XfLogETH", "ETH下发日志");
            htb.Add("epointsETHToTCS", "OCC数量");
            htb.Add("isxfTCS", "OCC状态");
            htb.Add("XfLogTCS", "OCC下发日志");



            DataToExcel dte = new DataToExcel();
            //string FilePath = dte.DataToExcel1(dt, "a");//
            string FilePath = Server.MapPath("~/Admin/excel/");

            string filename = "";
            try
            {
                //if (dt.Rows.Count > 0)
                //{
                //CreateExcel(dtexcel, "application/ms-excel", excel);

                filename = dte.DataExcel(dt, "标题", FilePath, htb);
                //}
                dte.CreateExcel(dt, "application/ms-excel", filename, htb);
                return File(FilePath + filename, "application/javascript", filename);

            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }

        }

        /// <summary>
        /// 导出会员奖金明细excel（按用户汇总某天的数据)
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public ActionResult ExportUserBonusDetailExcel(CurrencySum model)
        {
            DataTable dt = cyBll.GetUserBonusDetailExcel(model);

            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("userId", "会员编号");
            htb.Add("catName", "奖金名称");
            htb.Add("jstime", "结算日期");
            htb.Add("yf", "应发");
            htb.Add("epointsTCS", "转OCC");
            htb.Add("epointsETHToTCS", "转OCC数量");
            htb.Add("sf", "实发");

            htb.Add("fee1", "手续费");
            htb.Add("getETH", "到帐ETH");
            htb.Add("mulx", "业务摘要");


            DataToExcel dte = new DataToExcel();
            //string FilePath = dte.DataToExcel1(dt, "a");//
            string FilePath = Server.MapPath("~/Admin/excel/");

            string filename = "";
            try
            {
                //if (dt.Rows.Count > 0)
                //{
                //CreateExcel(dtexcel, "application/ms-excel", excel);

                filename = dte.DataExcel(dt, "标题", FilePath, htb);
                //}
                dte.CreateExcel(dt, "application/ms-excel", filename, htb);
                return File(FilePath + filename, "application/javascript", filename);

            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }

        }
    }
}
