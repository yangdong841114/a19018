﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using Web.Models;

namespace Web.Admin.Controllers
{
    public class ParameterSetController : Controller
    {
        public IParamSetBLL paramBLL { get; set; }
        public IInvestRecordBLL irBLL { get; set; }

        /// <summary>
        /// 初始化页面，读取已有参数
        /// </summary>
        /// <returns></returns>
        public JsonResult InitView()
        {
            ResponseDtoMap<string, ParameterSet> response = new ResponseDtoMap<string, ParameterSet>();
            response.map = paramBLL.GetAllToDictionary();
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }
        public JsonResult GetSetCourier()
        {
            Member current = (Member)Session["LoginUser"];
            var list = CourierHelp.GetCourierList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SetCourier()
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                CourierHelp.SetCourier();
                response.status = "success";
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "操作失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }
        
        /// <summary>
        /// 保存参数
        /// </summary>
        /// <param name="list">参数列表</param>
        /// <returns></returns>
        public JsonResult Save(List<ParameterSet> list)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                paramBLL.UpdateList(list);
                response.status = "success";
                //if(Request.Url.Host.IndexOf("occmny.io") != -1)//正式网址才请求POC转帐
                //irBLL.AutoCurrencyToOCC();//下发
            }
            catch (ValidateException ex){response.msg = ex.Message;}
            catch (Exception) { response.msg = "操作失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 下发操作
        /// </summary>
        /// <returns></returns>
        public JsonResult FfCurrency()
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Dictionary<string, ParameterSet> ps = paramBLL.GetDictionaryByCodes("ETHkg", "TCSkg");
                string ETHkg = ps["ETHkg"].paramValue;
                string TCSkg = ps["TCSkg"].paramValue;
                if (ETHkg != "开" && TCSkg != "开")
                {
                    throw new ValidateException("ETH和OCC的下发开关都未打开！请在参数设置打开要下发的开关");
                }
                else { 
                    //System.Timers.Timer timer = new System.Timers.Timer(500);
                    //timer.AutoReset = false;
                    ////设置是否执行System.Timers.Timer.Elapsed事件
                    //timer.Enabled = true;
                    ////绑定Elapsed事件
                    //timer.Elapsed += new System.Timers.ElapsedEventHandler(TimerUp);
                    //response.status = "success";
                }
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "操作失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }


        /// <summary>
         /// Timer类执行定时到点事件
         /// </summary>
         /// <param name="sender"></param>
         /// <param name="e"></param>
         private void TimerUp(object sender, System.Timers.ElapsedEventArgs e)
         {
             try
             {
                 irBLL.AutoCurrencyToOCC();
             }
             catch (Exception ex)
             {
             }
         }

    }
}
