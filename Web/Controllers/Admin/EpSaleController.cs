﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 挂卖记录Controller
    /// </summary>
    public class EpSaleController : Controller
    {
        public　IEpSaleRecordBLL epsBLL { get; set; }

        public IEpBuyRecordBLL epbBLL { get; set; }



        /// <summary>
        /// 分页查询挂卖的EP列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult GetListPage(EpSaleRecord model)
        {
            if (model == null)
            {
                model = new EpSaleRecord();
            }

            PageResult<EpSaleRecord> page = epsBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet); ;
        }


        /// <summary>
        /// 取消挂卖记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult SaveCancel(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["LoginUser"];
                epsBLL.SaveCancel(id, current);
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 批量购买ＥＰ
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult SaveRecordBatch(List<EpBuyRecord> list)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["LoginUser"];
                epbBLL.SaveRecordList(list, current);
                response.msg = "操作成功";
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
