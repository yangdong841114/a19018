﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 会员资料Controller
    /// </summary>
    public class MemberInfoController : Controller
    {
        public IMemberBLL memberBLL { get; set; }

        /// <summary>
        /// 查询会员信息
        /// </summary>
        /// <param name="memberId">会员ID</param>
        /// <returns></returns>
        public JsonResult GetModel(int memberId)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            if (memberId==0)
            {
                response.msg = "会员编号为空";
            }
            try
            {
                Member mm = memberBLL.GetModelByIdNoPassWord(memberId);
                response.status = "success";
                response.result = mm;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "查询会员出错，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 修改会员信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult UpdateMember(Member model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            if (model == null)
            {
                response.msg = "会员信息为空";
            }
            else
            {
                try
                {
                    Member current = (Member)Session["LoginUser"];
                    int c = memberBLL.UpdateMember(model, current);
                    response.msg = "保存成功";
                    response.status = "success";
                }
                catch (ValidateException va) { response.msg = va.Message; }
                catch (Exception) { response.msg = "保存失败！请联系管理员"; }
            }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
