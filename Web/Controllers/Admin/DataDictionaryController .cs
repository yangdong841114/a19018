﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    public class DataDictionaryController : Controller
    {
        public IDataDictionaryBLL dataDictionaryBLL { get; set; }

        /// <summary>
        /// 查询根节点列表
        /// </summary>
        /// <returns></returns>
        public JsonResult GetRootList()
        {
            ResponseDtoList<DataDictionary> response = new ResponseDtoList<DataDictionary>();
            List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("parentId",ConstUtil.EQ,0)).Result();
            List<DataDictionary> list = dataDictionaryBLL.GetList("select * from DataDictionary", param);
            response.list = list;
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 根据父节点ID查询
        /// </summary>
        /// <returns></returns>
        public JsonResult GetListChildren(DataDictionary model)
        {
            //ResponseDtoPage<DataDictionary> response = new ResponseDtoPage<DataDictionary>("fail");
            PageResult <DataDictionary> result = dataDictionaryBLL.GetListPage(model);
            //response.status = "success";
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }

        public JsonResult SaveOrUpdate(DataDictionary model)
        {
            //ResponseDtoPage<DataDictionary> response = new ResponseDtoPage<DataDictionary>("fail");
            //PageResult<DataDictionary> result = dataDictionaryBLL.GetListPage(model);
            //response.status = "success";
            //return Json(result, JsonRequestBehavior.AllowGet);
            return null;
        }
    }
}
