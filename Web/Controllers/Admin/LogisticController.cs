﻿using BLL;
using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Admin.Controllers
{
    /// <summary>
    /// 物流商 Controller
    /// </summary>
    public class LogisticController : Controller
    {
        public ILogisticBLL lgBLL { get; set; }


        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="ShopSet">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(Logistic model)
        {
            PageResult<Logistic> result = lgBLL.GetListPage(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }


        /// <summary>
        /// 同步物流商
        /// </summary>
        /// <returns></returns>
        public JsonResult Save()
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                lgBLL.Save();
                response.msg = "操作成功";
                response.status = "success";
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "操作失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
