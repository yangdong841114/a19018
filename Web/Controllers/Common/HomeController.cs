﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.Text.RegularExpressions;
using System.Configuration;


namespace Web.Common.Controllers
{
    public class HomeController : Controller
    {
        public IResourceBLL resourceBLL { get; set; }
        public IBaseSetBLL setBLL { get; set; }
        public IMemberBLL memberBLL { get; set; }
        public IParamSetBLL paramSetBLL { get; set; }
        public IMTransferBLL transferBLL { get; set; }
        public IReMemberEditBLL rmeBLL { get; set; }
        public IInvestRecordBLL irBLL { get; set; }

        public IInvestBackRecordBLL ibrBLL { get; set; }

        public static readonly string INVEST_CB_KEY = ConfigurationManager.AppSettings["INVEST_CB_KEY"];
        public static readonly string SEND_CB_KEY = ConfigurationManager.AppSettings["SEND_CB_KEY"];
        public static readonly string occSignCode = ConfigurationManager.AppSettings["OCC_signCode"];
        public static readonly string occSecretKey = ConfigurationManager.AppSettings["OCC_secretKey"];
        public static readonly string occUrl = ConfigurationManager.AppSettings["OCC_Url"];
        public static readonly string occTestUrl = ConfigurationManager.AppSettings["OCC_testUrl"];
        public static readonly string isOccTest = ConfigurationManager.AppSettings["isOccTest"];

        /// <summary>
        /// 安全退出，清除Session登录状态
        /// </summary>
        /// <returns></returns>
        public JsonResult ExitB()
        {
            ResponseData response = new ResponseData("fail");
            try
            {
                Session["LoginUser"] = null;
                Session["MemberUser"] = null;
                response.Success();
            }
            catch (Exception ex) { response.msg = "失败，请联系管理员！" + ex.Message; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取客户端IP地址
        /// </summary>
        /// <returns>若失败则返回回送地址</returns>
        private string GetIP()
        {
            //如果客户端使用了代理服务器，则利用HTTP_X_FORWARDED_FOR找到客户端IP地址
            string userHostAddress = null;
            if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                userHostAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString().Split(',')[0].Trim();
            }
            //否则直接读取REMOTE_ADDR获取客户端IP地址
            if (string.IsNullOrEmpty(userHostAddress))
            {
                userHostAddress = Request.ServerVariables["REMOTE_ADDR"];
            }
            //前两者均失败，则利用Request.UserHostAddress属性获取IP地址，但此时无法确定该IP是客户端IP还是代理IP
            if (string.IsNullOrEmpty(userHostAddress))
            {
                userHostAddress = Request.UserHostAddress;
            }
            //最后判断获取是否成功，并检查IP地址的格式（检查其格式非常重要）
            if (!string.IsNullOrEmpty(userHostAddress) && System.Text.RegularExpressions.Regex.IsMatch(userHostAddress, @"^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$"))
            {
                return userHostAddress;
            }
            return "127.0.0.1";
        }

        /// <summary>
        /// List权限列表转Map
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private Dictionary<string, string> GetPermissionMap(List<Resource> list)
        {
            Dictionary<string, string> di = new Dictionary<string, string>();
            if (list != null && list.Count > 0)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    Resource r = list[i];
                    if (!ValidateUtils.CheckNull(r.curl))
                    {
                        if (!di.ContainsKey(r.curl))
                        {
                            di.Add(r.curl, r.id);
                        }
                    }
                }
            }
            return di;
        }


        public JsonResult AutoCurrencyToOCC()
        {
            ResponseData response = new ResponseData("fail");
            try
            {
                if (Request.Url.Host.IndexOf("occmny.io") != -1)//正式网址才请求POC转帐
                {
                    irBLL.AutoCurrencyToOCC();
                    response.msg = "成功";
                    response.Success();
                }
            }
            catch (ValidateException va) { response.msg = va.Message + " 失败"; }
            catch (Exception ex) { response.msg = "失败，请联系管理员！" + ex.Message; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CurrencyToOcc(string type, string date)
        {
            ResponseData response = new ResponseData("fail");
            try
            {
                var url = isOccTest == "1" ? occTestUrl : occUrl;
                var isLocalhost = isOccTest == "1" ? 1 : Request.Url.Host.IndexOf("occmny.io");
                if (isLocalhost != -1)
                {
                   // var path = isOccTest == "1" ? Server.MapPath("/Content/RequestLog") : "";
                   // if (!System.IO.Directory.Exists(path)) System.IO.Directory.CreateDirectory(path);
                   // path = path == "" ? "" : path + "/" + DateTime.Now.ToString("_yyyyMMddhhmmss");
                    irBLL.CurrencyToOcc(occSignCode, url, type, date);
                    response.msg = "成功";
                    response.Success();
                }
            }
            catch (ValidateException va) { response.msg = va.Message + " 失败"; }
            catch (Exception ex) { response.msg = "失败，请联系管理员！" + ex.Message; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 下发回调
        /// </summary>
        /// <param name="data"></param>
        /// <param name="sign"></param>
        /// <returns></returns>
        public JsonResult CallbackOcc(string data, string sign)
        {
            ResponseData response = new ResponseData("fail");
            try
            {
                var mySign = DESEncrypt.md5(data + occSignCode);
                if (mySign != sign) { response.msg = "数据签名验证失败"; }
                irBLL.CallbackOcc(data);
                response.msg = "成功";
                response.Success();

            }
            catch (ValidateException va) { response.msg = va.Message + " 失败"; }
            catch (Exception ex) { response.msg = "失败，请联系管理员！" + ex.Message; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 定时下发
        /// </summary>
        /// <returns></returns>
        public JsonResult AutoCurrencyToOCCHASH()
        {
            ResponseData response = new ResponseData("fail");
            try
            {
                if (Request.Url.Host.IndexOf("occmny.io") != -1)//正式网址才请求POC转帐
                {
                    irBLL.AutoCurrencyToOCCHASH();//回调HASH
                    response.msg = "成功";
                    response.Success();
                }
            }
            catch (ValidateException va) { response.msg = va.Message + " 失败"; }
            catch (Exception ex) { response.msg = "失败，请联系管理员！" + ex.Message; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 下发回调，取hash
        /// </summary>
        /// <param name="orderID"></param>
        /// <param name="sign"></param>
        /// <returns></returns>
        public JsonResult CallbackPoc(string orderID, string sign)
        {
            ResponseData response = new ResponseData("fail");
            try
            {
                string secretKey = SEND_CB_KEY;
                string veryfy = DESEncrypt.md5("poccecool" + orderID.ToString() + secretKey);
                //if (sign != veryfy) { throw new ValidateException("sign-验证失败"); }
                //当前登录用户
                Member mb = memberBLL.GetModelByUserId("system");
                mb.ipAddress = "127.1.1.1";
                irBLL.OCCgateway("online", orderID);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception ex) { response.msg = "审核失败，请联系管理员！" + ex.Message; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        //绑定推荐人
        public JsonResult CallbackBindRe(string OrderId, string fromAddress, string toAddress, string HASH, string occValue, string getTime, string sign)
        {
            ResponseData response = new ResponseData("fail");
            try
            {
                if (OrderId == null) OrderId = "no";
                if (occValue == null) occValue = "0";
                string secretKey_A19018 = "A1901800xYWg8L";//来自各子系统密钥xYWg8L17pSVaTRkEyIqCAGFH

                string veryfy_A19018 = DESEncrypt.md5("poc" + HASH.ToString() + secretKey_A19018);

                if (sign != veryfy_A19018 && sign != "A1901800xYWg8LTest") { throw new ValidateException(" sign-验证失败"); }
                if (sign == "A1901800xYWg8LTest") fromAddress = fromAddress + "_test";
                try
                {
                    double.Parse(occValue);
                }
                catch { throw new ValidateException(occValue + " 非数值"); }
                try
                {
                    DateTime.Parse(getTime);
                }
                catch { throw new ValidateException(getTime + " 非日期"); }
                //核对orderID是否存在
                if (SqlChecker.CheckKeyWord(OrderId) != "") { throw new ValidateException(OrderId + " 非法含注入SQL"); }
                if (fromAddress.Trim() == "") { throw new ValidateException("fromAddress 钱包地址不能为空"); }
                if (toAddress.Trim() == "") { throw new ValidateException("toAddress 钱包地址不能为空"); }
                if (OrderId.Trim() == "") { throw new ValidateException(" OrderId不能为空"); }
                //会员是否有注册，没有就注册.
                Member reMem = memberBLL.GetModelByUserId(toAddress);
                if (reMem == null)
                {
                    Member pocMem = new Member();
                    pocMem.userId = toAddress;
                    pocMem.addTime = DateTime.Now;
                    reMem = memberBLL.SaveMemberNoVerify(pocMem);
                }
                Member myMem = memberBLL.GetModelByUserId(fromAddress);
                if (myMem == null)
                {
                    Member pocMem = new Member();
                    pocMem.userId = fromAddress;
                    pocMem.addTime = DateTime.Now;
                    pocMem.reId = reMem.id.Value;
                    pocMem.reName = reMem.userId;
                    pocMem.reuserName = reMem.userName;
                    myMem = memberBLL.SaveMemberNoVerify(pocMem);
                }
                else
                {
                    if (myMem.reName != "system")
                        throw new ValidateException(" 推荐关系是不能改的");
                    //改动上线
                    ReMemberEdit reedit = new ReMemberEdit();
                    reedit.uid = myMem.id.Value;
                    reedit.userId = myMem.userId;
                    reedit.userName = myMem.userName;
                    reedit.oldReName = myMem.reName;
                    reedit.newReName = reMem.userId;
                    reedit.addTime = DateTime.Now;
                    reedit.createId = 1;
                    reedit.createUser = "system";
                    rmeBLL.SaveReMemberEdit(reedit, myMem);
                }
                MTransfer mt = new MTransfer();
                mt.addTime = DateTime.Now;
                mt.auditTime = DateTime.Parse(getTime);
                mt.auditUid = 1;
                mt.auditUser = "system";
                mt.epoints = double.Parse(occValue);
                mt.fee = 0;
                mt.flag = 0;
                mt.fromUid = myMem.id.Value;
                mt.fromUserId = myMem.userId;
                mt.fromUserName = myMem.userName;
                mt.HASH = HASH;
                mt.toUid = reMem.id.Value;
                mt.toUserId = reMem.userId;
                mt.toUserName = reMem.userName;
                mt.typeId = -1;
                transferBLL.SaveByIdentity(mt);
                response.msg = "绑定推荐人成功";
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message + " 失败"; }
            catch (Exception ex) { response.msg = "失败，请联系管理员！" + ex.Message; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        //投资回调
        public JsonResult CallbackInvest(string OrderId, string fromAddress, string HASH, string ethValue, string getTime, string sign)
        {
            ResponseData response = new ResponseData("fail");

            InvestBackRecord ibr = new InvestBackRecord();
            ibr.addTime = DateTime.Now;
            ibr.flag = 0;
            ibr.invokerIp = "";
            ibr.cont = "{'orderId':'" + OrderId + "','fromAddress':'" + fromAddress + "','HASH':'" + HASH + "','ethValue':'" + ethValue + "','getTime':'" + getTime + "','sign':'" + sign + "'}";
            try
            {
                if (isOccTest != "1")
                {
                    if (ValidateUtils.CheckNull(OrderId)) OrderId = "no";
                    string secretKey_A19018 = INVEST_CB_KEY;//来自各子系统密钥xYWg8L17pSVaTRkEyIqCAGFH
                    string veryfy_A19018 = DESEncrypt.md5("poc" + HASH.ToString() + secretKey_A19018);
                    if (sign != veryfy_A19018) { throw new ValidateException(" sign-验证失败"); }
                    //核对orderID是否存在
                    if (SqlChecker.CheckKeyWord(OrderId) != "") { throw new ValidateException(OrderId + " 非法含注入SQL"); }
                    if (ValidateUtils.CheckNull(HASH)) { throw new ValidateException("回调HASH为空"); }
                    if (ValidateUtils.CheckNull(fromAddress)) { throw new ValidateException("fromAddress 钱包地址不能为空"); }
                }
                InvestRecord ir = new InvestRecord();
                ir.OrderId = OrderId;
                ir.fromAddress = fromAddress;
                ir.HASH = HASH;
                ir.ethValue = double.Parse(ethValue);
                ir.addTime = DateTime.Now;
                ir.getTime = DateTime.Parse(getTime);
                ir.status = "已确认";
                irBLL.SaveInvestRecord(ir);
                ibr.flag = 2;
                ibr.endTime = DateTime.Now;
                response.msg = "到帐回调成功";
                response.Success();
            }
            catch (ValidateException va)
            {
                response.msg = va.Message + " 失败";
                response.Fail();
                ibr.flag = 1;
            }
            catch (Exception ex)
            {
                response.msg = "失败，请联系管理员！" + ex.Message;
                response.Fail();
                ibr.flag = 1;
            }
            finally
            {
                ibrBLL.SaveModel(ibr);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        //接口测试
        public JsonResult OCCgateway(string orderID, string sign)
        {
            ResponseData response = new ResponseData("fail");
            try
            {

                string secretKey_A19018 = "A1901800xYWg8L";//来自各子系统密钥xYWg8L17pSVaTRkEyIqCAGFH  //OGVfmx49DaTrAy8R27EhizuL

                string veryfy_A19018 = DESEncrypt.md5("poc" + orderID.ToString() + secretKey_A19018);


                if (sign != veryfy_A19018 && sign != "A1901800xYWg8LTest") { throw new ValidateException(" sign-验证失败"); }
                string poc_back = irBLL.OCCgateway("test", orderID);//不调用了
                response.msg = poc_back;
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message + " 失败"; }
            catch (Exception ex) { response.msg = "失败，请联系管理员！" + ex.Message; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 进入首页
        /// </summary>
        /// <returns></returns>
        public ActionResult Index(string id)
        {
            BaseSet set = setBLL.GetModel();
            ViewData["WebCode"] = set.WebCode;
            return View();
        }

        /// <summary>
        /// 获取随机字符串
        /// </summary>
        /// <returns></returns>
        public JsonResult GetCode()
        {
            ResponseData response = new ResponseData("fail");
            try
            {
                string code = Guid.NewGuid().ToString().Replace("-", "");
                Session["signCode"] = code;
                response.msg = code;
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message + " 失败"; }
            catch (Exception ex) { response.msg = "失败，请联系管理员！" + ex.Message; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        private String callMessage(string code,string tpFlag,string sign,string userId,string address)
        {
            String s = "调用url:https://poc.ce.cool/walletapi/verifySign/" + tpFlag + "<br/>";
            s = s + "随机码：" + code + "<br/>";
            s = s + "签名字符：" + sign + "<br/>";
            s = s + "接口返回result.address：" + address + "<br/>";
            s = s + "前端传入钱包地址：" + userId + "<br/>";
            return s;
        }

        /// <summary>
        /// 获取登录信息
        /// </summary>
        /// <returns></returns>
        public JsonResult LoginEd()
        {
            ResponseData response = new ResponseData();
            response.Success();
            response.msg = Session["user_id"] == null ? "N" : Session["user_id"].ToString();
            return Json(response, JsonRequestBehavior.AllowGet);
        } 

        /// <summary>
        /// 是否已注册
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public JsonResult ExistsReg(string userId, string sign, string tpFlag)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                if (userId == null) { throw new ValidateException("钱包地址为空"); }
                if (sign == null) { throw new ValidateException("签名为空"); }
                else
                {
                    if (Session["signCode"] == null || Session["signCode"].ToString() == "")
                    {
                        throw new ValidateException("随机串为空，请刷新页面");
                    }
                    string address = JhInterface.AddressSign(Session["signCode"].ToString(), sign,tpFlag);

                    response.other = callMessage(Session["signCode"].ToString(), tpFlag, sign, userId, address);

                    if (address == null || address.ToUpper() != userId.ToUpper()) { throw new ValidateException("钱包地址验证错误!" + address); }
                    else
                    {
                        Member reMem = memberBLL.GetModelByUserId(userId);
                        if (reMem != null && reMem.id != null)
                        {
                            //获取用户前台权限
                            List<Resource> list = resourceBLL.GetListByUserAndParent(reMem.id.Value, "101");
                            Dictionary<string, string> di = GetPermissionMap(list);
                            reMem.permission = di;
                            Session["MemberUser"] = reMem;
                            response.Success();
                            response.msg = "ok";
                            Session["signCode"] = null;
                            Session["user_id"] = userId;
                        }
                        else
                        {
                            response.Success();
                            response.msg = "no";
                        }
                    }
                }
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception e) { response.msg = e.Message; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        //public JsonResult ExistsReg2(string userId, string sign)
        //{
        //    ResponseDtoData response = new ResponseDtoData("fail");
        //    try
        //    {
        //        if (userId == null) { throw new ValidateException("钱包地址为空"); }
        //        else
        //        {

        //            Member reMem = memberBLL.GetModelByUserId(userId);
        //            if (reMem != null && reMem.id != null)
        //            {
        //                //获取用户前台权限
        //                List<Resource> list = resourceBLL.GetListByUserAndParent(reMem.id.Value, "101");
        //                Dictionary<string, string> di = GetPermissionMap(list);
        //                reMem.permission = di;
        //                Session["MemberUser"] = reMem;
        //                response.Success();
        //                response.msg = "ok";
        //                Session["signCode"] = null;
        //            }
        //            else
        //            {
        //                response.Success();
        //                response.msg = "no";
        //            }
        //        }
        //    }
        //    catch (ValidateException ex) { response.msg = ex.Message; }
        //    catch (Exception e) { response.msg = e.Message; }
        //    return Json(response, JsonRequestBehavior.AllowGet);
        //}


        /// <summary>
        /// 注册会员并绑定推荐人
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="reUserName"></param>
        /// <returns></returns>
        public JsonResult BindReUser(string userId, string reUserName, string sign, string tpFlag)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                if (userId == null) { throw new ValidateException("钱包地址为空"); }
                if (sign == null) { throw new ValidateException("签名为空"); }
                else
                {
                    if (Session["signCode"] == null || Session["signCode"].ToString() == "")
                    {
                        throw new ValidateException("随机串为空，请刷新页面");
                    }
                    string address = JhInterface.AddressSign(Session["signCode"].ToString(), sign, tpFlag);
                    response.other = callMessage(Session["signCode"].ToString(), tpFlag, sign, userId, address);
                    Session["signCode"] = null;
                    if (address == null || address.ToUpper() != userId.ToUpper()) { throw new ValidateException("钱包地址验证错误!" + address); }
                    else
                    {
                        Member m = new Member();
                        m.userId = userId;
                        m.reName = reUserName;
                        memberBLL.SaveMember(m);
                        //获取用户前台权限
                        List<Resource> list = resourceBLL.GetListByUserAndParent(m.id.Value, "101");
                        Dictionary<string, string> di = GetPermissionMap(list);
                        m.permission = di;
                        Session["MemberUser"] = m;
                        Session["user_id"] = userId;
                        response.Success();
                    }
                }
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception e) { response.msg = e.Message; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}
