﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Config 的摘要说明
/// </summary>
public class ConfigHandler : Handler
{
    public ConfigHandler(HttpContextBase context) : base(context) { }

    public override Dictionary<string, string> Process()
    {
        return WriteJson(Config.Items);
    }
}