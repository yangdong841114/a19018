﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;


/// <summary>
/// Handler 的摘要说明
/// </summary>
public abstract class Handler
{
    public Handler(HttpContextBase context)
	{
        this.Request = context.Request;
        this.Response = context.Response;
        this.Context = context;
        this.Server = context.Server;
	}

    public abstract Dictionary<string, string> Process();

    protected Dictionary<string, string> WriteJson(object response)
    {
        string jsonpCallback = Request["callback"],
            json = JsonConvert.SerializeObject(response);
        Dictionary<string, string> di = new Dictionary<string, string>();
        if (String.IsNullOrWhiteSpace(jsonpCallback))
        {
            di.Add("xtype", "text/plain");
            di.Add("json", json);
            return di;
            //Response.AddHeader("Content-Type", "text/plain");
            //Response.Write(json);
        }
        else
        {
            di.Add("xtype", "application/javascript");
            di.Add("json", String.Format("{0}({1});", jsonpCallback, json));
            return di;
            //return String.Format("{0}({1});", jsonpCallback, json);
            //Response.AddHeader("Content-Type", "application/javascript");
            //Response.Write(String.Format("{0}({1});", jsonpCallback, json));
        }
        //Response.End();
    }

    //protected string WriteJson(object response)
    //{
    //    string jsonpCallback = Request["callback"],
    //        json = JsonConvert.SerializeObject(response);
    //    Dictionary<string, string> di = new Dictionary<string, string>();
    //    if (String.IsNullOrWhiteSpace(jsonpCallback))
    //    {
    //        di.Add("xtype", "text/plain");
    //        di.Add("json", json);
    //        return id;
    //        //Response.AddHeader("Content-Type", "text/plain");
    //        //Response.Write(json);
    //    }
    //    else 
    //    {
    //        di.Add("xtype", "application/javascript");
    //        di.Add("json", String.Format("{0}({1});", jsonpCallback, json));
    //        return di;
    //        //return String.Format("{0}({1});", jsonpCallback, json);
    //        //Response.AddHeader("Content-Type", "application/javascript");
    //        //Response.Write(String.Format("{0}({1});", jsonpCallback, json));
    //    }
    //    Response.End();
    //}

    public HttpRequestBase Request { get; private set; }
    public HttpResponseBase Response { get; private set; }
    public HttpContextBase Context { get; private set; }
    public HttpServerUtilityBase Server { get; private set; }
}