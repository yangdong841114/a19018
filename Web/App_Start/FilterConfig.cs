﻿using System.Web;
using System.Web.Mvc;

namespace Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());

            //自实现权限过滤器
            filters.Add(new Filters.MyActionFilterAttribute());
        }
    }
}