
define(['text!UService.html', 'jquery', 'j_easyui', 'zui'], function (UService, $) {

    var controller = function (id) {

        appView.html(UService);


        utils.AjaxPostNotLoadding("UService/InitView", {}, function (result) {
            if (result.status == "success") {
                var dto = result.result;
                $("#qName1").html(dto.qName1);
                $("#qName2").html(dto.qName2);
                $("#qName3").html(dto.qName3);
                $("#qName4").html(dto.qName4);

                $("#qq1").attr("href", "http:\//wpa.qq.com/msgrd?v=3&uin=" + dto.qq1 + "&site=qq&menu=yes");
                $("#qq2").attr("href", "http:\//wpa.qq.com/msgrd?v=3&uin=" + dto.qq2 + "&site=qq&menu=yes");
                $("#qq3").attr("href", "http:\//wpa.qq.com/msgrd?v=3&uin=" + dto.qq3 + "&site=qq&menu=yes");
                $("#qq4").attr("href", "http:\//wpa.qq.com/msgrd?v=3&uin=" + dto.qq4 + "&site=qq&menu=yes");

            } else {
                utils.showErrMsg(result.msg);
            }
        })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});