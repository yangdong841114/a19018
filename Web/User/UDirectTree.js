
define(['text!UDirectTree.html', 'jquery', 'j_easyui', 'zui'], function (UDirectTree, $) {

    var controller = function (name) {
       
        //ztree设置
        var treesetting = {
            data: {
                simpleData: {
                    enable: true,
                    idKey: "id", 	              // id编号字段名
                    pIdKey: "reId",               // 父id编号字段名
                    rootPId: null
                },
                key: {
                    name: "dtreeName"    //显示名称字段名
                }
            },
            view: {
                showLine: false,            //不显示连接线
                dblClickExpand: false       //禁用双击展开
            },
            callback: {
                onClick: function (e, treeId, node) {
                    if (node.isParent) {
                        resourceTree.expandNode(node);
                    }
                }
            }
        };

        var resourceTree = undefined; //ztree对象

        //初始化加载数据
        utils.AjaxPostNotLoadding("UDirectTree/GetDefaultDirectTree", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(UDirectTree);
                reInitTree(result.list);

                //查询按钮
                $("#queryBtn").on("click", function () {
                    var param = $("#QueryForm").serializeObject();
                    utils.AjaxPost("UDirectTree/GetDirectTreeByUserId", param, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            //生成树
                            reInitTree(result.list);
                        }
                    });
                })

                //我的直推图
                $("#selfBtn").on("click", function () {
                    utils.AjaxPost("UDirectTree/GetDefaultDirectTree", {}, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            //生成树
                            reInitTree(result.list);
                        }
                    });
                })
            }

        });



        //重新生成树
        reInitTree = function (data) {
            //先销毁树
            if (resourceTree) {
                resourceTree.destroy();
            }
            //生成树
            resourceTree = $.fn.zTree.init($("#resourceTree"), treesetting, data);

            //查找根节点并自动展开
            var rootNodes = resourceTree.getNodesByFilter(function (node) {
                if (node.reId == null || node.reId == 0) {
                    return true;
                }
            });
            if (rootNodes && rootNodes.length > 0) {
                for (var i = 0; i < rootNodes.length; i++) {
                    resourceTree.expandNode(rootNodes[i], true, false, true);
                }
            }
        }


        controller.onRouteChange = function () {
        };
    };

    return controller;
});