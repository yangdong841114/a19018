
define(['text!UMemberInfo.html', 'jquery', 'j_easyui', 'zui'], function (UMemberInfo, $) {

    var controller = function (memberId) {
       
        var pmap = null;

        //设置表单默认数据
        setDefaultFormValue = function (dto) {
            initProvince(dto);
            $("#id").val(dto.id);
            $(".form-control").each(function (index, ele) {
                if (dto[this.id]) {
                    if (this.id == "uLevel") {
                        this.value = cacheMap["ulevel"][dto[this.id + ""]];
                    }
                    else {
                        $(this).val(dto[this.id]);
                    }

                }
            });
            $("#treePlace").val(dto["treePlace"] == 0 ? "左区" : "右区");
        }

        //初始化省、市、区下拉框
        initProvince = function (dto) {
            $("#city").empty();
            $("#city").append("<option value='0'>--请选择--</option>");
            $("#area").empty();
            $("#area").append("<option value='0' selected='true'>--请选择--</option>");
            $("#province").empty();
            $("#province").append("<option value='0'>--请选择--</option>");
            if (areaData && areaData.length > 0) {
                for (var i = 0; i < areaData.length; i++) {
                    var pro = areaData[i];
                    $("#province").append("<option value='" + pro.value + "' dataId='"+pro.id+"' >" + pro.value + "</option>");
                    if (dto.province == pro.value) {
                        var childs = pro.childs;
                        if (childs && childs.length > 0) {
                            for (var j = 0; j < childs.length; j++) {
                                var child = childs[j];
                                $("#city").append("<option value='" + child.value + "' dataId='" + child.id + "'>" + child.value + "</option>");
                                if (dto.city == child.value) {
                                    var grandchilds = child.childs;
                                    if (grandchilds && grandchilds.length > 0) {
                                        for (var k = 0; k < grandchilds.length; k++) {
                                            var grandchild = grandchilds[k];
                                            $("#area").append("<option value='" + grandchild.value + "'>" + grandchild.value + "</option>");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        };

        var data = {};
        if (memberId && memberId > 0) { data["memberId"] = memberId; }

        var inputs = undefined;

        //加载会员信息
        utils.AjaxPostNotLoadding("UMemberInfo/GetModel", data, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(UMemberInfo);

                var dto = result.result;

                //$("#bankName").empty();
                //$("#bankName").append("<option value='0'>--请选择--</option>");
                //if (cacheList["UserBank"] && cacheList["UserBank"].length > 0) {
                //    var list = cacheList["UserBank"];
                //    for (var i = 0; i < list.length; i++) {
                //        $("#bankName").append("<option value='" + list[i].name + "'>" + list[i].name + "</option>");
                //    }
                //}

                //初始化银行帐号下拉框
                $("#bankName").empty();
                $("#bankName").append("<option value='0'>--请选择--</option>");
                if (cacheList["UserBank"] && cacheList["UserBank"].length > 0) {
                    var list = cacheList["UserBank"];
                    for (var i = 0; i < list.length; i++) {
                        $("#bankName").append("<option value='" + list[i].name + "'>" + list[i].name + "</option>");
                    }
                }
                
                //省改变事件
                $("#province").bind("change", function (a, b, c) {
                    //获取自定义属性的值
                    var dataId = $(this).find("option:selected").attr("dataId");
                    $("#city").empty();
                    $("#city").append("<option value='0'>--请选择--</option>");
                    $("#area").empty();
                    $("#area").append("<option value='0'>--请选择--</option>");
                    if (areaMap[dataId] && areaMap[dataId].length > 0) {
                        for (var i = 0; i < areaMap[dataId].length; i++) {
                            var child = areaMap[dataId][i];
                            $("#city").append("<option value='" + child.value + "' dataId='" + child.id + "'>" + child.value + "</option>");
                        }
                    }
                });

                //市改变事件
                $("#city").bind("change", function (a, b, c) {
                    //获取自定义属性的值
                    var dataId = $(this).find("option:selected").attr("dataId");
                    $("#area").empty();
                    $("#area").append("<option value='0'>--请选择--</option>");
                    if (areaMap[dataId] && areaMap[dataId].length > 0) {
                        for (var i = 0; i < areaMap[dataId].length; i++) {
                            var child = areaMap[dataId][i];
                            $("#area").append("<option value='" + child.value + "' dataId='" + child.id + "'>" + child.value + "</option>");
                        }
                    }
                })

                //初始表单默认值
                setDefaultFormValue(dto);

                //绑定获取焦点事件
                inputs = $("#memberForm").serializeObject();
                $.each(inputs, function (name, val) {
                    var current = $("#" + name);
                    var parent = current.parent();
                    var before = current.parent().parent().children().eq(0);
                    current.on("focus", function (event) {
                        parent.removeClass("has-error");
                        utils.destoryPopover(current);
                    });
                });

                //保存
                $("#saveBtn").on('click', function () {
                    var fs = $("#memberForm").serializeObject();
                    var checked = true;

                    //非空校验
                    $.each(fs, function (name, val) {
                        var current = $("#" + name);
                        var parent = current.parent();
                        var before = current.parent().parent().children().eq(0);
                        var emptyMsg = current.attr("emptyMsg");
                        if (before.hasClass("required")) {
                            if (val == 0) {
                                checked = false;
                                parent.addClass("has-error");
                                utils.showPopover(current, emptyMsg, "popover-danger");
                            }
                            if (name == 'bankCard') {
                                var card = val;
                                var reg = /^(\d{16,19})$/g;
                                if (!reg.test(card)) {
                                    utils.showErrMsg("银行卡号位数必须为16-19位！");
                                    jdom.focus();
                                    checked = false;
                                    return false;
                                }
                            }
                        }
                        if (parent.hasClass("has-error")) {
                            checked = false;
                        }
                    });

                    if (checked) {
                        utils.AjaxPost("UMemberInfo/UpdateMember", fs, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("操作成功！");
                                ////清空表单
                                //resetForm();
                            }
                        });
                    }
                })

            }
        });


        controller.onRouteChange = function () {
            //清除所有弹出层
            $.each(inputs, function (name, val) {
                var current = $("#" + name);
                var parent = current.parent();
                parent.removeClass("has-error");
                utils.destoryPopover(current);
            });
        };
    };

    return controller;
});