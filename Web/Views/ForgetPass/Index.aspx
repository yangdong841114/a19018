﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html>
<html lang="zh">
<head runat="server">
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><%:ViewData["sitename"]%></title>
    <link href="/Content/css/zui2.css" rel="stylesheet" type="text/css" />
    <link href="/Content/css/style.css" rel="stylesheet" type="text/css" />
    <script src="/Content/js/jquery-3.2.1.min.js" id="main"></script>
    <script type="text/javascript" src="/Content/js/zui.js"></script>
    <script type="text/javascript" src="/Content/js/jquery.flexslider-min.js"></script>
</head>
<body>
    <div class="header">
        <div class="pull-left">
            <img class="lf" src="/Content/images/logo.png" />
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="mainnav">
    </div>
    <div class="">
        <div class="center-block tipsbox">

            <div class="tipscontbox">
                <div class="tipsiconbox">
                    <i class="icon icon-user"></i>
                </div>
                <div class="form-group" style="padding-top: 8px;">
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon" >登录帐号</span>
                            <input type="text" class="form-control" id="acoungId" placeholder="请录入找回的登录帐号">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <button type="button" id="zfBtn" class="btn btn-warning ">确认</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="ifooter">
        <div class="">
            <p><%:ViewData["copyright"]%></p>

            <div class="clear"></div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $("#acoungId").on("focus", function () {
                $("#acoungId").removeClass("inputError");
                if ($("#acoungId").popover) {
                    $("#acoungId").popover("destroy");
                }
            })

            //回车键绑定
            document.onkeydown = function (event) {
                var e = event || window.event || arguments.callee.caller.arguments[0];
                if (e && e.keyCode == 13) {
                    $('#zfBtn').trigger("click");
                }
            };

            $("#zfBtn").on("click", function () {
                if ($("#acoungId").val() == 0) {
                    $("#acoungId").addClass("inputError");
                } else {
                    $.ajax({
                        url: "/ForgetPass/CheckUser",
                        type: "POST",
                        data: "id=" + $("#acoungId").val(),
                        success: function (result) {
                            if (result.status == "fail") {
                                var options = {
                                    container: 'body',  //在body层显示
                                    tipClass: 'popover-danger',      
                                    trigger: 'manual',  //需手动显示
                                    template: '<div class="popover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content" style="color:red"></div></div>',
                                    content: result.msg        //显示文本
                                };
                                $("#acoungId").addClass("inputError");
                                $("#acoungId").popover(options).popover("show");   //构建
                            } else {
                                location.href = "/ForgetPass/CheckCode";
                            }
                        }
                    });
                }
            })
        });
    </script>
</body>
</html>
