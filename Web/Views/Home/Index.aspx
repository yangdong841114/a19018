﻿﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OCC全链社区</title>
    <link type="text/css" href="/Content/APP/css/pstyle.css" rel="stylesheet">
    <link rel="stylesheet" href="/Content/APP/css/font-awesome.min.css">
    <link href="/Content/css/zui.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/Content/APP/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="/Content/APP/js/language.js"></script>
    <script type="text/javascript" src="/Content/js/zui.js"></script>
</head>
<body class="qdbody">

<div class="mainbox">

<div class="qidongment">
	<div class="lslogan">
		<h3>
			基于社区应用，链接社区用户<br />实现社区通证化 OCC为社区应用而生
		</h3>
	</div>
</div>

<div class="goindex"><a href="javascript:;" id="goindex">立即体验</a></div>
<div class="waitopen">
	<span class="closewait"><img src="/Content/images/btncloseg.png"></span>
	<div class="clear"></div>
	<h4>绑定老邻居</h4>
	<h5>您尚未绑定老邻居，请输入老邻居ID</h5>
	<div class="tjrtext">
		<span>老邻居ID</span>
		<input type="text" class="txttjr" id="rename" name="rename" placeholder="请填写老邻居ID">		
	</div>
	<p><a href="javascript:void();" class="waitbtn" id="submitBtn">确认加入社区</a></p>	
</div>
<div class="mesbackbg"></div>

</div>
<script type="text/javascript" src="/Content/APP/js/maincont.js?t=0.03"></script>
     <div style="display: none;">
        <script type="text/javascript">
          <%: @Html.Raw(ViewData["WebCode"])%>
            cnzz_protocol = cnzz_protocol.replace(/&quot;/g, '"');
        </script>
     </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        //显示错误消息
        showErrorMsg = function (msg) {
            var msgbox = new $.zui.Messager('提示消息：' + msg, {
                type: 'danger',
                icon: 'warning-sign',
                placement: 'center',
                parent: 'body',
                close: true
            });
            msgbox.show();
        }

        transMsg = function(msg){
            var dd = {"fromAddress":msg};
            $.ajax({
                url: "http://125.65.42.68/call/test",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(dd),
                success: function (result) {
                }
            });
        }

        var account,signHex,intervalId,tpFlag = "1";
        //注册会员并绑定推荐人
        $("#submitBtn").bind("click",function () {
            if($("#rename").val() == 0){ showErrorMsg("请填写老邻居ID！");}
            else{
                $.ajax({
                    url: "/Home/BindReUser",
                    type: "POST",
                    data: "userId="+account+"&reUserName="+$("#rename").val()+"&sign="+signHex+"&tpFlag="+tpFlag,
                    success: function (result) {
                        //transMsg(result.other);
                        if (result.status == "fail") {
                            showErrorMsg(result.msg);
                        } else {
                            //注册成功并跳转
                            $(".waitopen").removeClass("waitopens");
                            $(".mesbackbg").removeClass("mesbackbgs");
                            location.href = "/wap/User/index.html";
                        }
                    }
                });
            }
        });

        //立即体验按钮
        //1.检测用户的地址在我们系统是否有注册
        //2.有注册，后台生成登录信息跳转地址
        //3.未注册，弹出填写推荐人框，注册
        submitFc = function(sign){
            $("#goindex").click(function(){
                if(!account || account == null){
                    showErrorMsg("获取账户失败！")
                }else{
                    $.ajax({
                        url: "/Home/ExistsReg",
                        type: "POST",
                        data: "userId="+account+"&sign="+sign+"&tpFlag="+tpFlag,
                        success: function (result) {
                            //transMsg(result.other);
                            if (result.status == "fail") {
                                showErrorMsg(result.msg);
                            } else {
                                if(result.msg =="ok" ){
                                    //已注册，后台生成登录信息，直接跳转
                                    location.href = "/wap/User/index.html?t="+(new Date()).getTime();
                                }else{
                                    //未注册，弹出填写推荐人框
                                    $(".waitopen").toggleClass("waitopens");
                                    $(".mesbackbg").toggleClass("mesbackbgs");
                                }
                            }
                        }
                    });
                }
            });
        }

        //地址签名
        signAddress = function(){
            $.ajax({
                url: "/Home/GetCode",
                type: "POST",
                success: function (result) {
                    if (result.status == "fail") {
                        showErrorMsg(result.msg);
                    } else {
                        var code = result.msg;
                        window.web3.eth.sign(account,code,function(err,result){
                            if(err && err.message){
                                showErrorMsg(err.message);
                            }else{
                                signHex = result;
                                submitFc(result);
                            }
                        });
                    }
                }
            });
        }


        //加载用户的钱包账号
        async function loadWeb3 (){
            if (window.ethereum) {
                window.web3 = new Web3(window.ethereum);
                try {
                    // Request account access if needed
                    await ethereum.enable();
                    account = web3.eth.defaultAccount;
                    //account = accounts;
                    signAddress();
                    // Acccounts now exposed
                } catch (error) {
                    // User denied account access...
                }
            }
                // Legacy dapp browsers...
            else if (window.web3) {
                window.web3 = new Web3(web3.currentProvider);
                // Acccounts always exposed
                account = web3.eth.defaultAccount;
                console.log(defaultAccount); 
                signAddress();
                //web3.eth.sendTransaction({/* ... */});
            }
        }

        //$.ajax({
        //    url: "/Home/LoginEd",
        //    type: "POST",
        //    success: function (result) {
        //        if (result.status == "fail") {
        //            showErrorMsg(result.msg);
        //        } else {
        //            if("N" == result.msg){

                        //判断是imToken浏览器还是非imToken
                        if(!!window.imToken ||　(window.ethereum && window.ethereum.isImToken)){
                            //imToken环境
                            loadWeb3();
                        }else {
                            //轮询检测POC钱包环境是否准备好
                            intervalId = setInterval(function () {
                                if(window.mpw){
                                    tpFlag = "0";
                                    //准备就绪后清理定时器
                                    clearInterval(intervalId);
                                    //调用window.mpw api
                                    window.mpw.getWalletAddress(function(err,res){
                                        if(err != "mask close"){
                                            if(err){ showErrorMsg(err);}
                                            else{
                                                account = res; //获取账号
                                                //获取随机码
                                                $.ajax({
                                                    url: "/Home/GetCode",
                                                    type: "POST",
                                                    success: function (result) {
                                                        if (result.status == "fail") {
                                                            showErrorMsg(result.msg);
                                                        } else {
                                                            var code = result.msg;
                                        
                                                            //进行签名
                                                            window.mpw.signMessage(account,code,function(err,res){
                                                                if(err != "mask close"){
                                                                    if(err){
                                                                        showErrorMsg(err);
                                                                    }else{
                                                                        signHex = res;
                                                                        submitFc(res);
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    });
                                }
                            }, 1000);
                        }

        //            }else{
        //                location.href = "/wap/User/index.html?t="+(new Date()).getTime();
        //            }
        //        }
        //    }
        //});


        
        
        
        //else{
        //    //非imToken环境,非POC钱包环境
        //    showErrorMsg("请在imToken浏览器中打开！")
        //    $("#goindex").click(function(){
        //        showErrorMsg("请在imToken浏览器中打开！");
        //    });

        //}

        

    });
</script>

</html>
