﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%--<html lang="zh">--%>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>登录</title>
    <link type="text/css" href="/Content/APP/User/css/pstyle.css" rel="stylesheet">
    <link rel="stylesheet" href="/Content/APP/css/font-awesome.min.css">
    <link href="/Content/css/zui.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/Content/APP/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="/Content/js/zui.js"></script>
</head>
<body class="loginbg">
   
    <div class="clear"></div>

    <div class="loginenter">
        <dl>
            <dt>
                <img src="/Content/APP/User/images/iconuser.png" /></dt>
            <dd>
                <a class="fa fa-2x fa-times-circle logingb" style="display: none;"></a>
                <input type="text" class="entertxt" id="userId" placeholder="请输入用户名" />
            </dd>
        </dl>
        <dl>
            <dt>
                <img src="/Content/APP/User/images/iconpassword.png" /></dt>
            <dd><a class="fa fa-2x fa-times-circle logingb" style="display: none;"></a>
                <input type="password" class="entertxt" id="password" placeholder="请登录密码" />
            </dd>
        </dl>
        <div class="loginset">
            <label>
                <input type="checkbox" value="remember" id="remember" />记住我</label>
            <a href="/ForgetPass/Index">忘记密码？</a>
            <div class="clear"></div>
        </div>
        <div class="lbtnbox">
            <button class="bigbtn" onclick="login()">登录</button>
             <div id="eth" style="color:white;">环境检测显示</div>
            <div  style="text-align:center; margin-top:10px;">
           
        </div>
        </div>
        
    </div>
     <div  style="display:none;">
    <script type="text/javascript">
      <%: @Html.Raw(ViewData["WebCode"])%>
        cnzz_protocol = cnzz_protocol.replace(/&quot;/g, '"');
    </script>
          </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        $(".logingb").click(function () {
            $(this).next().val('');
            $(this).hide();
        });
        $("input[class='entertxt']").keyup(function () {
            //$(this).prev().show();
            if ($(this).val().length > 0) {
                $(this).prev().show();
            } else {
                $(this).prev().hide();
            }
        });



         



    

    //显示错误消息
    showErrorMsg = function (msg) {
        var msgbox = new $.zui.Messager('提示消息：' + msg, {
            type: 'danger',
            icon: 'warning-sign',
            placement: 'center',
            parent: 'body',
            close: true
        });
        msgbox.show();
    }


    var account;
    var is_login="no";

    if(window.ethereum.isImToken || (!!window.imToken))
    {

        //异步
        async function getData (){
            try {
            // 将异步操作写成同步的写法
            const accounts  = await ethereum.enable();//等待
            //得到进入下面
            $("#eth").html($("#eth").html() + "ethereum.enable环境成立" + JSON.stringify(accounts));
            $("#eth").html($("#eth").html() + "accounts object环境成立" + accounts);
            account=accounts;
            if(account!=""&&is_login=="no")
            {
                is_login="yes";
                location.href = "/Home/AppIndex/" + account;
             }
            }
            catch (error) { }
        };
     




        $("#eth").html("isImToken环境成立");
   

        if (typeof window.ethereum !== 'undefined')
            $("#eth").html($("#eth").html()+" window.ethereum环境成立"); 
        if (typeof window.web3 !== 'undefined') $("#eth").html($("#eth").html() + " window.web3环境成立");

        try {
            window.web3 = new Web3(ethereum);
            $("#eth").html($("#eth").html() + " window.web3 = new Web3(ethereum);环境成立");
        } catch (error) { $("#eth").html($("#eth").html() + " window.web3 = new Web3(ethereum);失败"); }


        getData();

        try {
            if (typeof ethereum.selectedAddress !== 'undefined') $("#eth").html($("#eth").html() + " ethereum.selectedAddress环境成立" + ethereum.selectedAddress);
            account=ethereum.selectedAddress;
            if(account!=""&&is_login=="no")
            {
                is_login="yes";
                location.href = "/Home/AppIndex/" + account;
            }
        }
        catch (error) { }

        try {
            account = web3.eth.defaultAccount;
            $("#eth").html($("#eth").html() + "web3.eth.defaultAccount环境成立" + account);
            if(account!=""&&is_login=="no")
            {
                is_login="yes";
                location.href = "/Home/AppIndex/" + account;
            }
        }
        catch (error) { }


    //if (window.ethereum) {
    //    window.web3 = new Web3(ethereum);
    //    $("#eth").html("ethereum环境成立");
    //    try {
    //         ethereum.enable();
    //        $("#eth").html("ethereum.enable环境成立");
    //    } catch (error) {
    //        // User denied account access...
    //    }
    //    account = web3.eth.defaultAccount;
    //    location.href = "/Home/AppIndex/" + account;
    //    $("#eth").html(account);
    //}
    //else if (window.web3) {
    //    window.web3 = new Web3(web3.currentProvider);
    //    $("#eth").html("window.web3环境成立");
    //    account = web3.eth.defaultAccount;
    //    $("#eth").html(account);
    //    location.href = "/Home/AppIndex/" + account;
      
    //}
    //else { $("#eth").html("读取不到eth.accounts请手动登录"); }
    
  


    }


      


    //登录校验
    login = function () {
        if ($("#userId").val() == 0) {
            showErrorMsg("用户名不能为空");
        } else if ($("#password").val() == 0) {
            showErrorMsg("密码不能为空");
        }
        else {
            var data = "userId=" + $("#userId").val() + "&password=" + $("#password").val();
            if (document.getElementById("remember").checked) {
                data += "&remenber=1";
            } else {
                data += "&remenber=0";
            }
            $.ajax({
                url: "/Common/MemberLoginByPhone",
                type: "POST",
                data: data,
                success: function (result) {
                    if (result.status == "fail") {
                        showErrorMsg(result.msg);
                    } else {
                        location.href = "/wap/User/index.html";
                    }
                }
            });
            //$("#loginForm").submit();
        }
    }

    });
</script>

</html>
