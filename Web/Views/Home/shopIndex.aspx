﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>游戏</title>
        <link type="text/css" href="/Content/APP/User/css/pstyle.css" rel="stylesheet">
       <link rel="stylesheet" href="/Content/APP/User/css/font-awesome.min.css">
<script type="text/javascript" src="/Content/APP/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/Content/APP/js/language.js"></script>

       
</head>
<body class="qdbody">

<div class="mainbox">

<div class="qidongment">
	<div class="lslogan">
		<%--<img src="/Content/APP/User/images/logo.png">--%>
		<h3>
			基于社区应用，链接社区用户<br />实现社区通证化 OCC为社区应用而生
		</h3>
	</div>
</div>
<div class="goindex" id="goindex"><a href="javascript:void(0)">立即体验</a></div>

<div class="waitopen">
	<span class="closewait"><img src="/Content/APP/User/images/btncloseg.png"></span>
	<div class="clear"></div>
	<h4>绑定老邻居</h4>
	<h5>您尚未绑定老邻居，请输入老邻居ID</h5>
	<div class="tjrtext">
		<span>老邻居ID</span>
		<input type="text" class="txttjr" placeholder="请填写老邻居ID">		
	</div>
	<p><a href="javascript:;" class="waitbtn">确认加入社区</a></p>	
</div>
<div class="mesbackbg"></div>

</div>
<script type="text/javascript" src="/Content/APP/js/maincont.js"></script>

</body>
</html>

<script>
    var account = '';
    var is_login = "no";
    alert("begin");
    alert("window.ethereum.isImToken:" + window.ethereum.isImToken);
    alert("window.imToken:"+window.imToken);
    if(window.ethereum.isImToken || (!!window.imToken))
    {
        //异步
        async function getData (){
            try {
                // 将异步操作写成同步的写法
                const accounts  = await ethereum.enable();//等待
                //得到进入下面
                account=accounts;
                if(account!=""&&is_login=="no")
                {
                    alert(account);
                    menuFix();
                    is_login="yes";                    
                }
            }
            catch (error) { }
        };


        getData();

        try {           
            account=ethereum.selectedAddress;
            if(account!=""&&is_login=="no")
            {
                is_login="yes";
                //location.href = "/Home/AppIndex/" + account;
            }
        }
        catch (error) { }

        try {
            account = web3.eth.defaultAccount;
            if(account!=""&&is_login=="no")
            {
                is_login="yes";
                //location.href = "/Home/AppIndex/" + account;
            }
        }
        catch (error) { }


        
    }



    //显示错误消息
    this.showErrMsg = function (msg) {
        var msgbox = new $.zui.Messager('提示消息：' + msg, {
            type: 'danger',
            icon: 'warning-sign',
            placement: 'center',
            parent: 'body',
            close: true
        });
        msgbox.show();
    }

    //显示成功消息
    this.showSuccessMsg = function (msg) {
        var msgbox = new $.zui.Messager('提示消息：' + msg, {
            type: 'success',
            icon: 'check',
            placement: 'center',
            html: '<font style="">' + msg + '</font>',
            parent: 'body',
            close: true
        });
        msgbox.show();
    } 
 </script>
