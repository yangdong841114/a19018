
'use strict';

(function (win) {
    //配置baseUrl
    //var baseUrl = document.getElementById('main').getAttribute('data-baseurl');

    /*
     * 文件依赖
     */
    var config = {
        //baseUrl: baseUrl,           //依赖相对路径
        map: {
            '*': {
                'css': '../Content/js/css.min'
            }
        },
        paths: {                    //如果某个前缀的依赖不是按照baseUrl拼接这么简单，就需要在这里指出
            director: '../Content/js/director.min',
            jquery: '../Content/js/jquery-3.2.1.min',
            underscore: '../Content/js/underscore',
            zui: '../Content/js/zui.min',
            datetimepicker: '../Content/js/datetimepicker.min',
            j_easyui: '../Content/easyui/jquery.easyui.min',
            ztree: '../Content/ztree/js/jquery.ztree.all.min',
            text: '../Content/js/text'             //用于requirejs导入html类型的依赖
        },
        shim: {                     //引入没有使用requirejs模块写法的类库。
            underscore: {
                exports: '_'
            },
            jquery: {
                exports: '$'
            },
            director: {
                exports: 'Router'
            },
            ztree: {
                exports: 'ztree',
                deps: ['jquery',
                    'css!../Content/ztree/css/zTreeStyle/zTreeStyle.css'
                ]
            },
            j_easyui: {
                exports: 'j_easyui',
                deps: ['jquery',
                    'css!../Content/easyui/themes/material/easyui.css',
                    'css!../Content/easyui/themes/icon.css',
                ]
            },
            zui: {
                exports: 'zui',
                deps: ['jquery', 'css!../Content/css/zui.css']
            },
            datetimepicker: {
                exports: 'datetimepicker',
                deps: ['jquery', 'zui', 'css!../Content/css/datetimepicker.min.css']
            },
        }
    };

    require.config(config);
    require(['jquery', 'router', '../Content/js/CommonUtil', 'j_easyui', 'ztree', 'zui'], function ($, router, util) {
        win.appView = $('#center');      //用于各个模块控制视图变化
        win.$ = $;                          //暴露必要的全局变量，没必要拘泥于requirejs的强制模块化
        win._ = _;
        //win.editDialog = $("#editModal");
        win.utils = util;
        win.cacheMap = null;
        win.cacheList = null;

        router.configure({
            //未匹配到URL
            notfound: function () { alert("您无权限访问");},
            before: function () {
                win.appView.empty();
                win.appView.html("<table width=\"100%\"><tr>"+
                "<td align=\"center\" style=\"padding-top:15%;\"><i class=\"icon icon-spin icon-spinner\" style=\"font-size:150px;\"></i><br/>"+
                    "<h3>加载中......</h3></td></tr></table>");
            },
        })

        //ztree设置
        var setting = {
            data: {
                simpleData: {
                    enable : true,
                    idKey : "id", 	              // id编号字段名
                    pIdKey: "parentResourceId",   // 父id编号字段名
                    rootPId : null
                },
                key: {
                    name: "resourceName"    //显示名称字段名
                }
            },
            view: {
                showLine: false,            //不显示连接线
                dblClickExpand :false       //禁用双击展开
            },
            callback: {
                onClick: function (e, treeId, treeNode) {
                    if (treeNode.isParent) {
                        zTree.expandNode(treeNode);
                    } else {
                        var url = (treeNode.curl + "").replace("Admin/", "");
                        location.href = "#" + url;
                    }
                }
            }
        };

        var zTree = null;
        var zNodes = [];

        $.ajax({
            url: "ManageWeb/GetBaseData",
            type: "POST",
            success: function (result) {
                win.cacheList = result; //list形式缓存
                //map形式缓存
                if (result != null) {
                    win.cacheMap = {};
                    for (name in result) {
                        win.cacheMap[name] = {};
                        var list = result[name];
                        if (list != null && list.length > 0) {
                            for (var i = 0; i < list.length; i++) {
                                var obj = list[i];
                                win.cacheMap[name][obj.id] = obj.name;
                            }
                        }
                    }
                }
                //初始化菜单start
                $.ajax({
                    url: "ManageWeb/GetMenu",
                    type: "POST",
                    data: "parentId=100",
                    success: function (result) {
                        if (result.status == "success") {
                            //zNodes = result.list;
                            for (var i = 0; i < result.list.length; i++) {
                                var node = result.list[i];
                                if (node.isMenu == "N") {
                                    var url = (node.curl + "").replace("Admin/", "");
                                    //注册路由
                                    router.toUrl(url+"/?([^\/]*)/?([^\/]*)", url + ".js");
                                }
                                if (node.isShow == 0) {
                                    zNodes.push(node);
                                }
                            }
                            zTree = $.fn.zTree.init($("#menuTree"), setting, zNodes);
                        }
                    }
                });
                //初始化菜单end
            }
        });

    });


})(window);
