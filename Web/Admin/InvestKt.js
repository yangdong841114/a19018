
define(['text!InvestKt.html', 'jquery', 'j_easyui', 'datetimepicker'], function (InvestKt, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "退款记录");
        appView.html(InvestKt);

      

        $("#ethOrtcs").empty();
        $("#ethOrtcs").append("<option value='全部'>--全部--</option>");
        $("#ethOrtcs").append("<option value='ETH'>ETH退出</option>");
        $("#ethOrtcs").append("<option value='TCS'>OCC退出</option>");

       

        //初始化按钮
        $(".easyui-linkbutton").each(function (i, ipt) {
            $(ipt).linkbutton({});
        });


        //初始化表格
        var grid = utils.newGrid("dg", {
            singleSelect: false, //禁用单选
            frozenColumns: [[{ field: 'userId', title: '钱包地址', width: '200' },
                {
                    field: '_operate', title: '操作', width: '80', align: 'center', formatter: function (val, row, index) {
                        if (row.userName != '合计：') {
                            var btnstr = "";
                            if (row.status == "待审核")
                                btnstr += '<a href="javascript:void(0);" dataId="' + row.id + '" class="XFETHgridFildEdit" >确认退款</a>&nbsp;&nbsp;';
                            return btnstr;
                        } else {
                            return '';
                        }
                    }
                }
            ]],
            columns: [[
             { field: 'ck', title: '文本', checkbox: true, },
             { field: 'userName', title: '会员ID', width: '100', align: 'right' },
             { field: 'uLevel', title: '会员级别', width: '80' },
             { field: 'regaddTime', title: '注册日期', width: '130' },
             { field: 'ethValueTz', title: '总投资ETH', width: '80' },
             { field: 'ethValueSy', title: '总收益ETH', width: '80' },
             { field: 'ethOrtcs', title: '退款类型', width: '100' },
             { field: 'je', title: '金额', width: '80' },
             { field: 'status', title: '会员状态', width: '80' },
             { field: 'addTime', title: '退款日期', width: '130' },
             { field: 'getTime', title: '到帐日期', width: '130' },
             { field: 'isSend', title: '下发状态', width: '80' },
             { field: 'KtLog', title: '接口日志', width: '200' },
          
           

             { field: 'HASH', title: 'HASH', width: '80' },
             { field: 'auditUser', title: '操作人', width: '100' }
           
            ]],
           
            url: "MemberPassed/GetListPageInvestKt"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                    data.rows[i]["regaddTime"] = utils.changeDateFormat(data.rows[i]["regaddTime"]);
                    data.rows[i]["getTime"] = utils.changeDateFormat(data.rows[i]["getTime"]);
                    data.rows[i].uLevel = cacheMap["ulevel"][data.rows[i].uLevel];
                    if (data.rows[i].isSend == -1) data.rows[i].isSend = "未审核";
                    if (data.rows[i].isSend == 0) data.rows[i].isSend = "未下发";
                    if (data.rows[i].isSend == 1) data.rows[i].isSend = "已下发";
                }
                //data.footer[0].userName = '合计：';
            }
            return data;
        }, function () {
            //行编辑按钮
            $(".gridFildEdit").each(function (i, dom) {
                dom.onclick = function () {
                    var memberId = $(dom).attr("memberId");
                    location.href = "#MemberInfo/" + memberId;
                    return false;
                }
            });

            //行确认按钮
            $(".XFETHgridFildEdit").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    utils.confirm("确定审核下发ETH吗？", function () {
                        utils.AjaxPost("Currency/AuditXFETH_KT", { id: dataId }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("操作成功");
                                queryGrid();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    })
                }
            });

           
           
           
        })

        


        //删除错误样式
        $("#msgContent").on("focus", function () {
            $("#msgContent").removeClass("inputError");
        })

        //导出excel
        $("#ExportExcel").on("click", function () {
            location.href = "MemberPassed/ExportExcelInvestKt?userName=" + $("#userName").val() + "&userId=" + $("#userId").val() + "&ethOrtcs=" + $("#ethOrtcs").val();
        })
        
      
        //查询grid
        queryGrid = function () {
            var objs = $("#MemberPassingQueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {
            //销毁模态窗口
         
        };
    };

    return controller;
});