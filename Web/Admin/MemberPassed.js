
define(['text!MemberPassed.html', 'jquery', 'j_easyui', 'datetimepicker'], function (MemberPassed, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "已开通会员");
        appView.html(MemberPassed);

        //初始化下拉框
        $("#status").empty();
        $("#status").append("<option value='全部'>--全部--</option>");
        $("#status").append("<option value='未参与'>未参与</option>");
        $("#status").append("<option value='共建中'>共建中</option>");
        $("#status").append("<option value='已停止'>已停止</option>");
        $("#status").append("<option value='冻结'>冻结</option>");
        $("#status").append("<option value='ETH退出'>ETH退出</option>");
        $("#status").append("<option value='OCC退出'>OCC退出</option>");

        $("#uLevel").empty();
        $("#uLevel").append("<option value=''>--全部--</option>");
        if (cacheList["ulevel"] && cacheList["ulevel"].length > 0) {
            for (var i = 0; i < cacheList["ulevel"].length; i++) {
                $("#uLevel").append("<option value='" + cacheList["ulevel"][i].id + "'>" + cacheList["ulevel"][i].name + "</option>");
            }
        }

       

        //初始化按钮
        $(".easyui-linkbutton").each(function (i, ipt) {
            $(ipt).linkbutton({});
        });

       

        

       

        //冻结会员
        disabledMember = function () {
            lockMember(1);
        }
        //启用会员
        enabledMember = function () {
            lockMember(0);
        }
        //重置密码
        redPwdMember = function () {
            lockMember(2);
        }

        //启用或冻结会员实现
        lockMember = function (isLock) {
            //获取选中的行（数组）
            var rows = grid.datagrid("getSelections");
            var msg = "";
            if(isLock == 0)
                msg = "启用";
            if (isLock == 1)
                msg = "冻结";
            if (isLock == 2)
                msg = "重置密码";
            if (rows == null || rows.length == 0) { utils.showErrMsg("请选择需要" + msg + "的会员！"); }
            else {
                utils.confirm("您确定要" + msg + "选中的会员吗？", function () {
                    var data = { isLock: isLock };
                    for (var i = 0; i < rows.length; i++) {
                        data["ids[" + i + "]"] = rows[i].id;
                    }
                    utils.AjaxPost("MemberPassed/LockMember", data, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showSuccessMsg(result.msg);
                            //重刷grid
                            queryGrid();
                        }
                    });
                });
            }
        }

        //初始化表格
        var grid = utils.newGrid("dg", {
            singleSelect: false, //禁用单选
            rownumbers: false,
            frozenColumns: [[{ field: 'userId', title: '钱包地址', width: '200' },
             {
                 field: '_operate', title: '操作', width: '160', align: 'center', formatter: function (val, row, index) {
                     return '<a href="javascript:void(0);" memberId="' + row.id + '" class="gridFildTo" >进入前台</a>&nbsp;&nbsp;';
                 }
             }
            ]],
            columns: [[
             { field: 'ck', title: '文本', checkbox: true, },
             { field: 'userName', title: '会员ID', width: '100', align: 'right' },
             { field: 'uLevel', title: '会员级别', width: '80' },
             { field: 'addTime', title: '注册日期', width: '130' },
             { field: 'status', title: '会员状态', width: '80' },
             { field: 'isLock', title: '是否冻结', width: '70' },
             { field: 'reuserName', title: '推荐人ID', width: '80' },
             { field: 'reName', title: '推荐人钱包', width: '200' }
           
            ]],
            
            toolbar: [{
                text: "启用",
                iconCls: 'icon-man',
                handler: enabledMember
            }, '-', {
                text: "冻结",
                iconCls: 'icon-lock',
                handler: disabledMember
            }, '-', {
                text: "重置密码",
                iconCls: 'icon-redo',
                handler: redPwdMember
            }
            ],
            url: "MemberPassed/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                    data.rows[i].uLevel = cacheMap["ulevel"][data.rows[i].uLevel];
                    var rr = cacheMap["rLevel"][data.rows[i].rLevel];
                    data.rows[i].rLevel = rr ? rr : "无";
                    data.rows[i].empty = data.rows[i].empty == 0 ? "否" : "是";
                    data.rows[i].isLock = data.rows[i].isLock == 0 ? "否" : "是";
                    data.rows[i].isFt = data.rows[i].isFt == 0 ? "否" : "是";
                }
            }
            return data;
        }, function () {
            //行编辑按钮
            $(".gridFildEdit").each(function (i, dom) {
                dom.onclick = function () {
                    var memberId = $(dom).attr("memberId");
                    location.href = "#MemberInfo/" + memberId;
                    return false;
                }
            });
            //行进入前台按钮
            $(".gridFildTo").each(function (i, dom) {
                dom.onclick = function () {
                    var memberId = $(dom).attr("memberId");
                    utils.confirm("确定用该帐号进入前台吗？", function () {
                        utils.AjaxPost("MemberPassed/ToForward", { id: memberId }, function (result) {
                            if (result.status == "success") {
                                location.href = "/wap/User/index.html";
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    })
                }
            });
           
        })

        


        //删除错误样式
        $("#msgContent").on("focus", function () {
            $("#msgContent").removeClass("inputError");
        })


        
      
        //查询grid
        queryGrid = function () {
            var objs = $("#MemberPassingQueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {
            //销毁模态窗口
         
        };
    };

    return controller;
});