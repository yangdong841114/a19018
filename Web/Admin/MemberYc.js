
define(['text!MemberYc.html', 'jquery', 'j_easyui', 'datetimepicker'], function (MemberYc, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "异常帐户");
        appView.html(MemberYc);

      

        $("#uLevel").empty();
        $("#uLevel").append("<option value=''>--全部--</option>");
        if (cacheList["ulevel"] && cacheList["ulevel"].length > 0) {
            for (var i = 0; i < cacheList["ulevel"].length; i++) {
                $("#uLevel").append("<option value='" + cacheList["ulevel"][i].id + "'>" + cacheList["ulevel"][i].name + "</option>");
            }
        }

       

        //初始化按钮
        $(".easyui-linkbutton").each(function (i, ipt) {
            $(ipt).linkbutton({});
        });

        //初始化表格
        var grid = utils.newGrid("dg", {
            singleSelect: false, //禁用单选
            rownumbers: false,
            frozenColumns: [[{ field: 'userId', title: '钱包地址', width: '200' }
            ]],
            columns: [[
             { field: 'ck', title: '文本', checkbox: true, },
             { field: 'userName', title: '会员ID', width: '100', align: 'right' },
             { field: 'uLevel', title: '会员级别', width: '80' },
             { field: 'addTime', title: '注册日期', width: '130' },
             { field: 'status', title: '会员状态', width: '80' },

             { field: 'ethValueYx', title: '总投资ETH', width: '80' },
             { field: 'SumCurrencyFd', title: '收益封顶', width: '80' },
             { field: 'SumCurrencyETH', title: '总收益ETH', width: '80' },
             { field: 'SumCurrencyETHCfd', title: '超封顶ETH', width: '80' },
             { field: 'SumCurrencyXfETH', title: '总下发ETH', width: '80' },
             { field: 'SumCurrencyXfETHCg', title: '多发ETH', width: '80' },
             { field: 'SumCurrencyTCS', title: '总收益OCC', width: '80' },
             { field: 'SumCurrencyXfTCS', title: '总下发OCC', width: '80' },

             { field: 'reuserName', title: '推荐人ID', width: '80' },
             { field: 'reName', title: '推荐人钱包', width: '200' }
           
            ]],
           
            url: "MemberPassed/GetListPageYc"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                    data.rows[i].uLevel = cacheMap["ulevel"][data.rows[i].uLevel];
                    var rr = cacheMap["rLevel"][data.rows[i].rLevel];
                    data.rows[i].rLevel = rr ? rr : "无";
                    data.rows[i].empty = data.rows[i].empty == 0 ? "否" : "是";
                    data.rows[i].isLock = data.rows[i].isLock == 0 ? "否" : "是";
                    data.rows[i].isFt = data.rows[i].isFt == 0 ? "否" : "是";
                }
            }
            return data;
        }, function () {
            //行编辑按钮
            $(".gridFildEdit").each(function (i, dom) {
                dom.onclick = function () {
                    var memberId = $(dom).attr("memberId");
                    location.href = "#MemberInfo/" + memberId;
                    return false;
                }
            });
            //行进入前台按钮
            $(".gridFildTo").each(function (i, dom) {
                dom.onclick = function () {
                    var memberId = $(dom).attr("memberId");
                    utils.confirm("确定用该帐号进入前台吗？", function () {
                        utils.AjaxPost("MemberPassed/ToForward", { id: memberId }, function (result) {
                            if (result.status == "success") {
                                location.href = "/wap/User/index.html";
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    })
                }
            });
           
        })

        


        //删除错误样式
        $("#msgContent").on("focus", function () {
            $("#msgContent").removeClass("inputError");
        })


        
      
        //查询grid
        queryGrid = function () {
            var objs = $("#MemberPassingQueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {
            //销毁模态窗口
         
        };
    };

    return controller;
});