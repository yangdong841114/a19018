
define(['text!CurrencyXf.html', 'jquery', 'j_easyui', 'datetimepicker'], function (CurrencyXf, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "下发记录");
        appView.html(CurrencyXf);

        //初始化日期选择框
        $(".form-date").datetimepicker(
            {
                language: "zh-CN",
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
                format: "yyyy-mm-dd"
            });
        CurrencyToOcc = function (type, id) {
            var xfType = type == 0 ? "OCC" : "ETH";
            var date = $("#currency_" + id).attr("data");
            utils.AjaxPostNotLoadding("/Home/CurrencyToOcc", { type: xfType, date: date }, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    utils.showSuccessMsg(result.msg);
                }
            });
        }
        //初始化表格
        var grid = utils.newGrid("dg", {
            showFooter: true,
            frozenColumns: [[
                {
                    field: '_operate', title: '操作', width: '70', align: 'center', formatter: function (val, row, index) {
                        if (row.addDate != '合计：') {
                            return '&nbsp;<a href="javascript:void(0);" addDate="' + row.addDate + '" class="gridFildTo" >下发明细</a>&nbsp;';
                        } else {
                            return '';
                        }
                    }


                }]],
            columns: [[
                { field: 'addDate', title: '结算日期', width: '100' },
                { field: 'XfzsETH', title: 'ETH总数', width: '100' },
                { field: 'XfzsYxfETH', title: '已下发ETH', width: '100' },
                { field: 'XfzsDxfETH', title: '待下发ETH', width: '100' },

                { field: 'XfzsTCS', title: 'OCC总数', width: '100' },
                { field: 'XfzsYxfTCS', title: '已下发OCC', width: '100' },
                { field: 'XfzsDxfTCS', title: '待下发OCC', width: '100' },
                { field: 'xfeth', title: '下发ETH', width: '100' },
                { field: 'xfocc', title: '下发OCC', width: '100' }

            ]],
            onDblClickRow: function (index, data) {
                location.href = '#CurrencyUserXf/' + data.addDate;
            },
            url: "Currency/GetAllSumCurrencyXf"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addDate"] = utils.changeDateFormat(data.rows[i]["addDate"], 'date');
                    data.rows[i]["XfzsETH"] = data.rows[i]["XfzsETH"].toFixed(8);
                    data.rows[i]["XfzsYxfETH"] = data.rows[i]["XfzsYxfETH"].toFixed(8);
                    data.rows[i]["XfzsDxfETH"] = data.rows[i]["XfzsDxfETH"].toFixed(8);
                    data.rows[i]["XfzsTCS"] = data.rows[i]["XfzsTCS"].toFixed(8);
                    data.rows[i]["XfzsYxfTCS"] = data.rows[i]["XfzsYxfTCS"].toFixed(8);
                    data.rows[i]["XfzsDxfTCS"] = data.rows[i]["XfzsDxfTCS"].toFixed(8);
                    data.rows[i]["xfeth"] = data.rows[i]["isEth"] == 0 ? "<a id=currency_" + i + " data='" + data.rows[i]["addDate"] + "' onclick='CurrencyToOcc(1," + i + ")'>下发ETH</a>" : "<a>已下发</a>";
                    data.rows[i]["xfocc"] = data.rows[i]["isOcc"] == 0 ? "<a id=currency_" + i + " data='" + data.rows[i]["addDate"] + "' onclick='CurrencyToOcc(0," + i + ")'>下发OCC</a>" : "<a>已下发</a>";
                }
                data.footer[0].addDate = '合计：';
            }
            return data;
        }, function () {

            //行查看明细按钮
            $(".gridFildTo").each(function (i, dom) {
                dom.onclick = function () {
                    var addDate = $(dom).attr("addDate");
                    location.href = '#CurrencyUserXf/' + addDate;
                }
            });
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {

        };
    };

    return controller;
});