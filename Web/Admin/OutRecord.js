
define(['text!OutRecord.html', 'jquery', 'j_easyui', 'datetimepicker'], function (OutRecord, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "出局记录");
        appView.html(OutRecord);


        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

        //初始化表格
        var grid = utils.newGrid("dg", {
            showFooter: true,
            rownumbers: false,
            frozenColumns: [[{ field: 'userId', title: '钱包地址', width: '100' }]],
            columns: [[
                { field: 'userName', title: '会员ID', width: '100' },
                { field: 'uLevel', title: '出局前会员级别', width: '150' },
                { field: 'addTime', title: '出局日期', width: '120' },
                { field: 'ethValueTz_Wcj', title: '本周期投资ETH金额', width: '200' },
                { field: 'SumCurrencyWcj', title: '本周期总应发收益', width: '200' }
            ]],
            url: "MemberPassed/GetListPageOutRecord"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                    data.rows[i].uLevel = cacheMap["ulevel"][data.rows[i].uLevel];
                }
                data.footer[0].ethValueTz_Wcj = data.footer[0].ethValueTz_Wcj.toFixed(4);
                data.footer[0].SumCurrencyWcj = data.footer[0].SumCurrencyWcj.toFixed(4);
            }
            return data;
        }, function () {

            //行确认按钮
            $(".gridFildEdit").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    window.open('https://etherscan.io/tx/' + dataId);
                }
            });
            
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

       

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});