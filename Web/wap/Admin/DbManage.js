
define(['text!DbManage.html', 'jquery'], function (DbManage, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("数据管理")
        appView.html(DbManage);

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //清空数据库按钮
        $("#clearBtn").on("click", function () {
            $("#prompTitle").html("确定清空数据库吗？");
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/DbManage/ClearDb", {}, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        utils.showSuccessMsg("清除成功");
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        })

        //备份数据库按钮
        $("#backUpBtn").on("click", function () {
            $("#prompTitle").html("确定备份数据库吗？");
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/DbManage/BakDb", {}, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        utils.showSuccessMsg("备份成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        })

        //恢复数据库
        backDb = function (fname) {
            $("#prompTitle").html("恢复后现有数据将丢失，确定要恢复备份吗？");
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/DbManage/reBackDb", { fileName: fname }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        utils.showSuccessMsg("恢复成功");
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        //删除备份
        deleteBk = function (dataId) {
            $("#prompTitle").html("确定删除该备份吗？");
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/DbManage/Delete", { id: dataId }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        utils.showSuccessMsg("删除成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        //下载数据库
        downloadBk = function (fname) {
            location.href = "/Admin/DbManage/DownLoadFile/" + fname;
        }
        downloadBkUrl = function (fname) {
            location.href = "/Admin/bakup/" + fname;
        }

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#DbManagedatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/DbManage/GetBackModelListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);

                            var dto = rows[i];
                            html += '<li>' +
                                    '<div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addTime + '</time>';
                            html += '<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<div class="btnbox"><ul class="tga3">' +
                            '<li><button class="seditbtn" onclick=\'backDb("' + dto.fname + '")\'>恢复</button></li>' +
                           '<li><button class="sdelbtn" onclick=\'downloadBk("' + dto.fname + '")\'>函数下载</button></li>' +
                             '<li><button class="sdelbtn" onclick=\'downloadBkUrl("' + dto.fname + '")\'>路径下载</button></li>' +
                            '<li><button class="smallbtn" onclick="deleteBk(\'' + dto.id + '\')">删除</button></li>' +
                            '</ul></div>' +
                            '<dl><dt>备份文件</dt><dd>' + dto.fname + '</dd></dl><dl><dt>备份时间</dt><dd>' + dto.addTime + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#DbManageitemList").append(html);
                    }, function () {
                        $("#DbManageitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#DbManageitemList").empty();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});