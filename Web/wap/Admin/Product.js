
define(['text!Product.html', 'jquery'], function (Product, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("商品管理")
        appView.html(Product);

        var dto = null;
        var editList = {};

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        var shelveList = [{ id: 0, value: "全部" }, { id: 0, value: "已下架" }, { id: 0, value: "已上架" }];
        //是否上架选择
        utils.InitMobileSelect('#isShelveName', '是否上架', shelveList, null, [0], null, function (indexArr, data) {
            $("#isShelveName").val(data[0].name);
            $("#isShelve").val(data[0].id);
        });

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);

        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        //上架商品
        shelveProduct = function (id) {
            utils.AjaxPost("/Admin/Product/Shelve", { id: id }, function (result) {
                if (result.status == "success") {
                    utils.showSuccessMsg("上架成功");
                    searchMethod();
                } else {
                    utils.showErrMsg(result.msg);
                }
            });
        }

        //下架商品
        cancelShelveProduct = function (id) {
            utils.AjaxPost("/Admin/Product/CancelShelve", { id: id }, function (result) {
                if (result.status == "success") {
                    utils.showSuccessMsg("下架成功");
                    searchMethod();
                } else {
                    utils.showErrMsg(result.msg);
                }
            });
        }

        //删除按钮
        deleteRecord = function (id) {
            $("#sureBtn").unbind();
            //确认删除
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/Product/Delete", { id: id }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        utils.showSuccessMsg("删除成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            })
            utils.showOrHiddenPromp();
        }

        //预览图片
        $("#imgFile").bind("change", function () {
            var url = URL.createObjectURL($(this)[0].files[0]);
            document.getElementById("showImg").style.backgroundImage = 'url(' + url + ')';
        })

        //发布商品
        $("#deployProduct").bind("click", function () {
            dto = null;
            $("#id").val("");
            $("#imgUrl").val("");
            $("#productCode").val("");
            $("#productName").val("");
            $("#price").val("");
            $("#fxPrice").val("");
            document.getElementById("showImg").style.backgroundImage = 'url(-testimg/testd1.jpg)';
            utils.setEditorHtml(editor, "")
            $("#mainDiv").css("display", "none");
            $("#deployDiv").css("display", "block");
        });

        //初始化编辑器
        var editor = new Quill("#editor", {
            modules: {
                toolbar: utils.getEditorToolbar()
            },
            theme: 'snow'
        });

        //保存发布商品
        $("#saveProductBtn").bind("click", function () {
            //金额校验
            var g = /^\d+(\.{0,1}\d+){0,1}$/;
            //非空校验
            if ($("#productCode").val() == 0) {
                utils.showErrMsg("商品编码不能为空");
            } else if ($("#productName").val() == 0) {
                utils.showErrMsg("商品名称不能为空");
            } else if ($("#imgFile").val() == 0 && !dto) { //新增必须上传图片，编辑时可以不用上传
                utils.showErrMsg("请选择上传的图片");
            } else if (!g.test($("#price").val())) {
                utils.showErrMsg("报单价金额格式错误");
            } else if (!g.test($("#fxPrice").val())) {
                utils.showErrMsg("复消价金额格式错误");
            } else if (editor.getText() == 0) {
                utils.showErrMsg("请输入商品内容");
            } else {
                var formdata = new FormData();
                if (dto) {
                    formdata.append("id", $("#id").val());
                    formdata.append("imgUrl", $("#imgUrl").val());
                }
                formdata.append("productName", $("#productName").val());
                formdata.append("productCode", $("#productCode").val());
                formdata.append("price", $("#price").val());
                formdata.append("fxPrice", $("#fxPrice").val());
                formdata.append("cont", utils.getEditorHtml(editor));
                formdata.append("imgFile", $("#imgFile")[0].files[0]);

                utils.AjaxPostForFormData("/Admin/Product/SaveOrUpdate", formdata, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        $("#mainDiv").css("display", "block");
                        $("#deployDiv").css("display", "none");
                        utils.showSuccessMsg("保存成功！");
                        searchMethod();
                    }
                });
            }
        })

        //关闭发布商品
        $("#closeDeployBtn").bind("click", function () {
            $("#mainDiv").css("display", "block");
            $("#deployDiv").css("display", "none");
        })

        //编辑商品
        editRecord = function (id) {
            dto = editList[id];
            $("#id").val(id);
            $("#imgUrl").val(dto.imgUrl);
            $("#productCode").val(dto.productCode);
            $("#productName").val(dto.productName);
            $("#price").val(dto.price);
            $("#fxPrice").val(dto.fxPrice);
            document.getElementById("showImg").style.backgroundImage = 'url(' + dto.imgUrl + ')';
            utils.setEditorHtml(editor, dto.cont)
            $("#mainDiv").css("display", "none");
            $("#deployDiv").css("display", "block");

        }

        utils.CancelBtnBind();

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#Productdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/Product/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            rows[i]["status"] = rows[i].isShelve == 1 ? "已下架" : "已上架";

                            var dto = rows[i];
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)" style=""><time>' + dto.productCode + '</time>';
                            if (dto.status == "已上架") {
                                html += '<span class="ship">' + dto.status + '</span>';
                            } else {
                                html += '<span class="noship">' + dto.status + '</span>';
                            }
                            html += '&nbsp;<span class="sum">' + dto.productName + '</span><i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<div class="btnbox"><ul class="tga3">';
                            if (dto.isShelve == 1) {
                                html += '<li><button class="smallbtn" onclick="shelveProduct(\'' + dto.id + '\')">上架</button></li>';
                            } else {
                                html += '<li><button class="sdelbtn" onclick="cancelShelveProduct(\'' + dto.id + '\')">下架</button></li>';
                            }
                            html += '<li><button class="sdelbtn" onclick=\'deleteRecord(' + dto.id + ')\'>删除</button></li>' +
                                 '<li><button class="sdelbtn" onclick=\'editRecord(' + dto.id + ')\'>编辑</button></li>' +
                            '</ul></div>' +
                            '<dl><dt>商品编码</dt><dd>' + dto.productCode + '</dd></dl><dl><dt>商品名称</dt><dd>' + dto.productName + '</dd></dl>' +
                            '<dl><dt>商品图片</dt><dd><img data-toggle="lightbox" src="' + dto.imgUrl + '" data-image="' + dto.imgUrl + '" class="img-thumbnail" alt="" width="100"></dd></dl>' +
                            '<dl><dt>报单价</dt><dd>' + dto.price + '</dd></dl><dl><dt>复消价</dt><dd>' + dto.fxPrice + '</dd></dl>' +
                            '<dl><dt>是否上架</dt><dd>' + dto.status + '</dd></dl><dl><dt>发布日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                            '</div></li>';
                            editList[dto.id] = dto;
                        }
                        $("#ProductitemList").append(html);
                    }, function () {
                        $("#ProductitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#ProductitemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            param["isShelve"] = $("#isShelve").val();
            param["productName"] = $("#productName2").val();
            param["productCode"] = $("#productCode2").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })


        controller.onRouteChange = function () {

        };
    };

    return controller;
});