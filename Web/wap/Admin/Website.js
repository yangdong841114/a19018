
define(['text!Website.html', 'jquery'], function (Website, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("前台设置")
        

        utils.AjaxPostNotLoadding("/Admin/Website/InitView", {}, function (result) {
            if (result.status=="fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(Website);

                var dto = result.result;
                $("#msg").val(dto.msg);
                if (dto.isclose == 1) {
                    $("#isclose").attr("checked", true);
                }


                //保存
                $("#saveBtn").on('click', function () {
                    var checked = $("#isclose")[0].checked;     //复选框
                    var val = $("#msg").val();
                    if (checked==false && val == 0) {
                        utils.showErrMsg("请输入关闭网站的提示信息");
                    } else {
                        var data = { id: 1, isclose: (checked ? 1 : 2), msg: (val == 0 ? "" : val) };
                        utils.AjaxPost("/Admin/Website/Save", data, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("保存成功！");
                            }
                        });
                    }
                    
                })
            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});