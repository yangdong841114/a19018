
define(['text!EpSale.html', 'jquery'], function (EpSale, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("挂卖记录")
        appView.html(EpSale);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        var statusDto = { 0: "挂卖中", 1: "部分售出", 2: "全部售出", 3: "已完成", 4: "已取消" };
        var statusList = [{ id: -1, value: "全部" }, { id: 0, value: "挂卖中" }, { id: 1, value: "部分售出" }, { id: 2, value: "全部售出" }, { id: 3, value: "已完成" }, { id: 4, value: "已取消" }, ]

        //初始化下拉框
        utils.InitMobileSelect('#flagName', '选择状态', statusList, null, [0], null, function (indexArr, data) {
            $("#flagName").val(data[0].value);
            $("#flag").val(data[0].id);
        });

        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);

        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //进入明细页面
        toDetailPage = function (id) {
            location.href = "#EpDetail/" + id;
        }

        //取消挂卖
        cancelRecord = function (id) {
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/EpSale/SaveCancel", { id: id }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showSuccessMsg("操作成功");
                        searchMethod();

                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#EpSaledatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/EpSale/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            rows[i]["status"] = statusDto[rows[i]["flag"]];

                            var dto = rows[i];
                            html += '<li>' +
                                    '<div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addTime + '</time><span class="sum">' + dto.userId + '</span>';
                            if (dto.status == "已完成") {
                                html += '<span class="status ship">' + dto.status + '</span>';
                            } else {
                                html += '<span class="status noship">' + dto.status + '</span>';
                            }
                            html += '<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<div class="btnbox"><ul class="tga2">' + 
                            '<li><button class="seditbtn" onclick=\'cancelRecord(' + dto.id + ')\'>取消挂卖</button></li>' +
                            '<li><button class="sdelbtn" onclick=\'toDetailPage(' + dto.id + ')\'>交易明细</button></li>' +
                            '</ul></div>' +
                            '<dl><dt>挂卖单号</dt><dd>' + dto.number + '</dd></dl><dl><dt>卖家编号</dt><dd>' + dto.userId + '</dd></dl>' +
                            '<dl><dt>手机号</dt><dd>' + dto.phone + '</dd></dl><dl><dt>QQ</dt><dd>' + dto.qq + '</dd></dl>' +
                            '<dl><dt>挂卖数量</dt><dd>' + dto.saleNum + '</dd></dl><dl><dt>待售数量</dt><dd>' + dto.waitNum + '</dd></dl>' +
                            '<dl><dt>已售数量</dt><dd>' + dto.scNum + '</dd></dl><dl><dt>挂卖日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                            '<dl><dt>状态</dt><dd>' + dto.status + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#EpSaleitemList").append(html);
                    }, function () {
                        $("#EpSaleitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#EpSaleitemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            param["userId"] = $("#userId").val();
            param["flag"] = $("#flag").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});