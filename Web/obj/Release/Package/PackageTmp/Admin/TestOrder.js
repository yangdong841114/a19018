
define(['text!TestOrder.html', 'jquery', 'j_easyui', 'zui'], function (TestOrder, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "测试数据录入");
        appView.html(TestOrder);


       
        
    

       

        //保存start
        $("#saveBtnCallbackBindReForm").on('click', function () {
            
            if ($("#userId").val() == 0) {
                utils.showErrMsg("请录入您的钱包地址");
            }
            else if ($("#userId").val() == 0) {
                utils.showErrMsg("请录入推荐人的钱包地址");
            }
            else{
                var data = { userId: $("#userId").val(), reUserName: $("#reName").val() };
                utils.AjaxPost("/Home/BindReUser", data, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showSuccessMsg("操作成功");
                    }
                });
            }
            
        }); ////保存end


        //保存start
        $("#saveBtnCallbackInvestForm").on('click', function () {
            var fs = $("#CallbackInvestForm").serializeObject();

            utils.AjaxPost("/Home/CallbackInvest", fs, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    utils.showSuccessMsg(result.msg);
                }
            });

        }); ////保存end


        //保存start
        $("#saveBtnTCSgatewayForm").on('click', function () {
            var fs = $("#TCSgatewayForm").serializeObject();

            utils.AjaxPost("/Home/TCSgateway", fs, function (result) {
                $("#TCSgatewayDiv").html(result.msg);
            });

        }); ////保存end
       
        

        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#regContent').dialog("destroy");
        };
    };

    return controller;
});