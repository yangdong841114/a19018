
define(['text!CurrencyUser.html', 'jquery', 'j_easyui', 'datetimepicker'], function (CurrencyUser, $) {

    var controller = function (addDate) {
        //设置标题
        $("#center").panel("setTitle", "明细记录");
        appView.html(CurrencyUser);

        if (addDate) {
            $("#startTime").val(addDate);
            $("#endTime").val(addDate);
            $("#reback").on("click", function () {
                history.go(-1);
            });
        } else {
            $("#reback")[0].style.display = 'none';
            //初始化日期选择框
            $(".form-date").datetimepicker(
            {
                language: "zh-CN",
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
                format: "yyyy-mm-dd"
            });
        }


        var isxfETHList = [{ id: "0", name: "未下发" }, { id: "1", name: "待确认" },
                        { id: "3", name: "已下发" }];

        //初始化查询区select
        $("#isxfETH").empty();
        $("#isxfETH").append("<option value='-1'>--全部--</option>");
        if (isxfETHList && isxfETHList.length > 0) {
            for (var i = 0; i < isxfETHList.length; i++) {
                $("#isxfETH").append("<option value='" + isxfETHList[i].id + "'>" + isxfETHList[i].name + "</option>");
            }
        }

        //初始化表格
        var grid = utils.newGrid("dg", {
            showFooter: true,
            rownumbers: false,
            frozenColumns: [[{ field: 'userId', title: '钱包地址', width: '100' },
                {
                    field: '_operate', title: '操作', width: '150', align: 'center', formatter: function (val, row, index) {
                        if (row.userName != '合计：') {
                            var btnstr = "";
                            if (row.isxfETH == "未审核")
                            btnstr+= '<a href="javascript:void(0);" dataId="' + row.id + '" class="XFETHgridFildEdit" >下发ETH</a>&nbsp;&nbsp;' ;
                            if (row.isxfTCS == "未审核")
                                btnstr += '<a href="javascript:void(0);" dataId="' + row.id + '" class="XFTCSgridFildEdit" >下发OCC</a>&nbsp;&nbsp;';
                            return btnstr;
                        } else {
                            return '';
                        }
                    }
                }]],
            columns: [[
                { field: 'addDate', title: '生成日期', width: '100' },
                { field: 'userName', title: '会员ID', width: '100' },
                { field: 'epointsETH', title: 'ETH数量', width: '100' },
                { field: 'isxfETH', title: 'ETH状态', width: '100' },
                { field: 'XfLogETH', title: 'ETH下发日志', width: '200' },
                { field: 'epointsETHToTCS', title: 'OCC数量', width: '100' },
                { field: 'isxfTCS', title: 'OCC状态', width: '100' },
                { field: 'XfLogTCS', title: 'OCC下发日志', width: '200' },
                {
                    field: 'HASHETH', title: 'HASHETH', width: '300', align: 'center', formatter: function (val, row, index) {

                        return '<a href="javascript:void(0);" dataId="' + row.HASHETH + '" class="gridFildEdit" >' + row.HASHETH + '</a>&nbsp;&nbsp;';

                    }
                }
            ]],
            
            url: "Currency/GetDetailListPageXf?startTime=" + $("#startTime").val() + "&endTime=" + $("#endTime").val()
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addDate"] = utils.changeDateFormat(data.rows[i]["addDate"], 'date');
                    data.rows[i]["epointsETH"] = data.rows[i]["epointsETH"].toFixed(8);
                    data.rows[i]["epointsETHToTCS"] = data.rows[i]["epointsETHToTCS"].toFixed(8);
                    if(data.rows[i].isxfETH == 2) data.rows[i].isxfETH="未审核";
                    if (data.rows[i].isxfETH == 0) data.rows[i].isxfETH = "未下发";
                    if (data.rows[i].isxfETH == 1) data.rows[i].isxfETH = "待确认";
                    if (data.rows[i].isxfETH == 3) data.rows[i].isxfETH = "已下发";

                    if (data.rows[i].isxfTCS == 2) data.rows[i].isxfTCS = "未审核";
                    if (data.rows[i].isxfTCS == 0) data.rows[i].isxfTCS = "未下发";
                    if (data.rows[i].isxfTCS == 1) data.rows[i].isxfTCS = "待确认";
                    if (data.rows[i].isxfTCS == 3) data.rows[i].isxfTCS = "已下发";
                }
                data.footer[0].userName = '合计：';
                data.footer[0].epointsETH = data.footer[0].epointsETH.toFixed(8);
                data.footer[0].epointsETHToTCS = data.footer[0].epointsETHToTCS.toFixed(8);
            }
            return data;
        }, function () {
            //行确认按钮
            $(".gridFildEdit").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    window.open('https://etherscan.io/tx/' + dataId);
                }
            });

            //行确认按钮
            $(".XFETHgridFildEdit").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    utils.confirm("确定审核下发ETH吗？", function () {
                        utils.AjaxPost("Currency/AuditXFETH", { id: dataId }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("操作成功");
                                queryGrid();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    })
                }
            });

            //行确认按钮
            $(".XFTCSgridFildEdit").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    utils.confirm("确定审核下发OCC吗？", function () {
                        utils.AjaxPost("Currency/AuditXFOCC", { id: dataId }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("操作成功");
                                queryGrid();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    })
                }
            });



        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        $("#ExportExcel").on("click", function () {
            location.href = "Currency/ExportBonusDetailExcelXf?userName=" + $("#userName").val() + "&userId=" + $("#userId").val() + "&startTime=" + $("#startTime").val() + "&endTime=" + $("#endTime").val();
        })

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});