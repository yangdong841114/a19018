
define(['text!InvestWx.html', 'jquery', 'j_easyui', 'datetimepicker'], function (InvestWx, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "无效投资");
        appView.html(InvestWx);


        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

        //初始化表格
        var grid = utils.newGrid("dg", {
            showFooter: true,
            rownumbers: false,
            frozenColumns: [[{ field: 'OrderId', title: 'OrderId', width: '100' }]],
            columns: [[
                { field: 'userName', title: '会员ID', width: '100' },
                { field: 'fromAddress', title: '钱包地址', width: '200' },

                { field: 'ethValue', title: '投资金额ETH', width: '120' },
                { field: 'ethValueTz', title: '投资本金ETH', width: '120' },
                { field: 'epointsETHToTCS', title: '销毁OCC数量', width: '120' },
                { field: 'status', title: '状态', width: '100' },
                { field: 'addTime', title: '投资日期', width: '130' },
               {
                   field: 'HASH', title: 'HASH', width: '200', align: 'center', formatter: function (val, row, index) {

                       return '<a href="javascript:void(0);" dataId="' + row.HASH + '" class="gridFildEdit" >' + row.HASH + '</a>&nbsp;&nbsp;';

                   }
               },
               { field: 'XhLog', title: '备注', width: '150' },
            ]],
            url: "MemberPassed/GetListPageInvestWx"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                }
                data.footer[0]["ethValueTz"] = data.footer[0].ethValueTz.toFixed(2);
            }
            return data;
        }, function () {

            //行确认按钮
            $(".gridFildEdit").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    window.open('https://etherscan.io/tx/' + dataId);
                }
            });
            
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //导出excel
        $("#ExportExcel").on("click", function () {
            location.href = "MemberPassed/ExportExcelInvestWx?userName=" + $("#userName").val() + "&userId=" + $("#userId").val()
        })

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});