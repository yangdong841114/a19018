
define(['text!InvestSj.html', 'jquery', 'j_easyui', 'datetimepicker'], function (InvestSj, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "财务审计");
        appView.html(InvestSj);


        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

        //初始化表格
        var grid = utils.newGrid("dg", {
            showFooter: true,
            rownumbers: false,
            frozenColumns: [[{ field: 'userId', title: '钱包地址', width: '100' }]],
            columns: [[
                { field: 'userName', title: '会员ID', width: '100' },
                { field: 'sjTime', title: '审计日期', width: '100' },

                { field: 'srETH', title: '收入ETH总数', width: '120' },
                { field: 'xfETH', title: '下发ETH总数', width: '120' },
                { field: 'szceETH', title: '收支差额ETH', width: '120' },
                { field: 'tzbjETH', title: '投资本金ETH', width: '100' },
                 { field: 'xfTCS', title: '下发OCC总数', width: '100' },
                    { field: 'xhTCS', title: '销毁OCC总数', width: '100' },


                { field: 'addTime', title: '投资日期', width: '130' }
            ]],
            url: "MemberPassed/GetListPageInvestSj"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                    data.rows[i]["sjTime"] = utils.changeDateFormat(data.rows[i]["sjTime"]);
                }
                data.footer[0].srETH =data.footer[0].srETH.toFixed(4);
                data.footer[0].xfETH = data.footer[0].xfETH.toFixed(4);
                data.footer[0].szceETH = data.footer[0].szceETH.toFixed(4);
                data.footer[0].tzbjETH = data.footer[0].tzbjETH.toFixed(4);
                data.footer[0].xfTCS = data.footer[0].xfTCS.toFixed(4);
                data.footer[0].xhTCS = data.footer[0].xhTCS.toFixed(4);
            }
            return data;
        }, function () {

            //行确认按钮
            $(".gridFildEdit").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    window.open('https://etherscan.io/tx/' + dataId);
                }
            });
            
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //导出excel
        $("#ExportExcel").on("click", function () {
            location.href = "MemberPassed/ExportExcelInvestSj?userName=" + $("#userName").val() + "&userId=" + $("#userId").val()
        })

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});