
define(['text!FamilyTree.html', 'jquery', 'j_easyui', 'css!../Content/css/styleAdmin.css'], function (FamilyTree, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "系谱图");
        
        bindTree = function (msg, other) {
            $("#treeDiv").html(msg);
            $("#other").html("");
            if (other && other.length > 0) {
                $("#other").html("上线链路：" + other);
            }
            $(".forward_to").each(function () {
                var dom = $(this);
                dom.on("click", function () {
                    var userId = dom.attr("userId");
                    var data = { userId: userId, ceng: 3 };
                    utils.AjaxPost("FamilyTree/GetFamilyTreeByCondition", data, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            $("#userId").val("");
                            $("#ceng").val("3");
                            bindTree(result.msg, result.other);
                        }
                    });
                })
            });
        }

        //初始化加载数据
        utils.AjaxPostNotLoadding("FamilyTree/GetFamilyTree", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(FamilyTree);
                bindTree(result.msg, result.other);

                //查询按钮
                $("#queryBtn").on("click", function () {
                    var param = $("#QueryForm").serializeObject();
                    utils.AjaxPost("FamilyTree/GetFamilyTreeByCondition", param, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            bindTree(result.msg, result.other);
                        }
                    });
                })
            }

        });

        controller.onRouteChange = function () {
            //销毁模态窗口
        };
    };

    return controller;
});