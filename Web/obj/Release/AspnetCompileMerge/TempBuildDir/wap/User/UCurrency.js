
define(['text!UCurrency.html', 'jquery'], function (UCurrency, $) {

    var controller = function (name) {
        var me = this;
        //設置標題
      
        appView.html(UCurrency);

        if (language == "ft") {
            $("#title").html("獎金明細");
            $("#startTime").attr('placeholder', '開始日期');
            $("#endTime").attr('placeholder', '結束日期');
            $("#searchBtn").text("搜索");
            $("#clearQueryBtn").text("清除");
            $("#a_mx").html("收益明細");
            $("#a_hz").html("收益合計");
        }

        if (language == "en") {
            $("#title").html("Bonus");
            $("#startTime").attr('placeholder', 'startTime');
            $("#endTime").attr('placeholder', 'endTime');
            $("#searchBtn").text("search");
            $("#clearQueryBtn").text("clear");
            $("#a_mx").html("Detail");
            $("#a_hz").html("Total");
        }

        if (language == "hw") {
            $("#title").html("獎金明細");
            $("#startTime").attr('placeholder', '開始日期');
            $("#endTime").attr('placeholder', '結束日期');
            $("#searchBtn").text("搜索");
            $("#clearQueryBtn").text("清除");
            $("#a_mx").html("收益明細");
            $("#a_hz").html("收益合計");
        }

        //清空查詢條件按鈕
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //點擊選項卡切換
        $('.maintag ul li').click(function () {
            var $this = $(this);
            var $t = $this.index();
            $('.maintag ul li').removeClass();
            $this.addClass('active');
            var $contentbox = $('.contentbox');
            $contentbox.css('display', 'none');
            $contentbox.eq($t).css('display', 'block');
        });

        //查詢參數
        this.param = utils.getPageData();

        //初始化日期選擇框
        utils.initCalendar(["startTime", "endTime"]);

        //設置高度
        //utils.setHeight();

        //獲取匯總的默認數據
        setTotalDefault = function () {
            return '<li><div class="lead"><h5>所有獎項合計</h5><span>0</span></div>' +
                                  '<dl><dt>日分紅</dt><dd>0</dd></dl><dl><dt>業績獎勵</dt><dd>0</dd></dl>' +
                                  '<dl><dt>加權分紅</dt><dd>0</dd></dl><dl><dt>應發</dt><dd>0</dd></dl>' +
                                  '<dl><dt>轉OCC</dt><dd>0</dd></dl><dl><dt>OCC數量</dt><dd>0</dd></dl>' +
                                  '<dl><dt>實發</dt><dd>0</dd></dl><dl><dt>手續費</dt><dd>0</dd></dl>' +
                                  '<dl><dt>到帳ETH</dt><dd>0</dd></dl>' +
                                  '</div></li>';
        }

        //獎金詳情
        toCurrencyDetail = function (addDate) {
            location.href = "#UCurrencyUserDetail/" + addDate;
        }

        var dropload = $('#UCurrencyDatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData('/User/UCurrency/GetByUserSumCurrency', param, me,
                    function (rows, footers) {
                        var footer = footers[0];
                        var totleHtml = "";
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addDate"] = utils.changeDateFormat(rows[i]["addDate"], 'date');
                            rows[i]["cat1"] = rows[i]["cat1"].toFixed(8);
                            rows[i]["cat2"] = rows[i]["cat2"].toFixed(8);
                            rows[i]["cat3"] = rows[i]["cat3"].toFixed(8);
                            rows[i]["cat4"] = rows[i]["cat4"].toFixed(8);
                            rows[i]["cat5"] = rows[i]["cat5"].toFixed(2);
                            rows[i]["cat6"] = rows[i]["cat6"].toFixed(2);
                            rows[i]["cat7"] = rows[i]["cat7"].toFixed(2);
                            rows[i]["yf"] = rows[i]["yf"].toFixed(8);
                            rows[i]["fee1"] = rows[i]["fee1"].toFixed(8);
                            rows[i]["fee2"] = rows[i]["fee2"].toFixed(2);
                            rows[i]["fee3"] = rows[i]["fee3"].toFixed(2);
                            rows[i]["sf"] = rows[i]["sf"].toFixed(8);
                            rows[i]["epointsETHToTCS"] = rows[i]["epointsETHToTCS"].toFixed(8);
                            rows[i]["epointsTCS"] = rows[i]["epointsTCS"].toFixed(8);
                            rows[i]["getETH"] = rows[i]["getETH"].toFixed(8);
                            var dto = rows[i];
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addDate + '</time><span class="sum">+' + dto.yf + '</span>' +
                                  '實發：<span class="sum">¥' + dto.sf + '</span><i class="fa fa-angle-right"></i></div>' +
                                  '<div class="allinfo"><div class="btnbox"><button class="seditbtn" onclick=\'toCurrencyDetail("' + dto.addDate + '")\'>獎金明細</button></div>' +
                                  '<dl><dt>結算日期</dt><dd>' + dto.addDate + '</dd></dl>' +
                                  '<dl><dt>日分紅</dt><dd>' + dto.cat1 + '</dd></dl><dl><dt>業績獎勵</dt><dd>' + dto.cat2 + '</dd></dl>' +
                                  '<dl><dt>加權分紅</dt><dd>' + dto.cat3 + '</dd></dl><dl><dt>應發</dt><dd>' + dto.yf + '</dd></dl>' +
                                  '<dl><dt>轉OCC</dt><dd>' + dto.epointsETHToTCS + '</dd></dl><dl><dt>OCC數量</dt><dd>' + dto.epointsTCS + '</dd></dl>' +
                                  '<dl><dt>實發</dt><dd>' + dto.sf + '</dd></dl><dl><dt>手續費</dt><dd>' + dto.fee1 + '</dd></dl>' +
                                  '<dl><dt>到帳ETH</dt><dd>' + dto.getETH + '</dd></dl>' +
                                  '</div></li>';
                        }
                        $("#UCurrencyItemList").append(html);
                        totleHtml += '<li><div class="lead"><h5>所有獎項合計</h5><span>' + footer.yf + '</span></div>' +
                                  '<dl><dt>日分紅</dt><dd>' + footer.cat1 + '</dd></dl><dl><dt>業績獎勵</dt><dd>' + footer.cat2 + '</dd></dl>' +
                                  '<dl><dt>加權分紅</dt><dd>' + footer.cat3 + '</dd></dl><dl><dt>應發</dt><dd>' + footer.yf + '</dd></dl>' +
                                  '<dl><dt>轉OCC</dt><dd>' + footer.epointsETHToTCS + '</dd></dl><dl><dt>OCC數量</dt><dd>' + footer.epointsTCS + '</dd></dl>' +
                                  '<dl><dt>實發</dt><dd>' + footer.sf + '</dd></dl><dl><dt>手續費</dt><dd>' + footer.fee1 + '</dd></dl>' +
                                  '<dl><dt>到帳ETH</dt><dd>' + footer.getETH + '</dd></dl>' +
                                  '</div></li>';
                        $("#footerUl").html(totleHtml);

                    }, function () {
                        $("#footerUl").html(setTotalDefault());
                        $("#UCurrencyItemList").append('<p class="dropload-noData">暫無數據</p>');
                    });
            }
        });

        //查詢按鈕
        $("#searchBtn").on("click", function () {
            param.page = 1;
            $("#UCurrencyItemList").empty();
            $("#footerUl").html(setTotalDefault());
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        })

        controller.onRouteChange = function () {
            dropload = null;
            delete dropload;
        };
    };

    return controller;
});