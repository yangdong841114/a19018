﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html>
<html lang="zh">
<head runat="server">
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><%:ViewData["sitename"]%></title>
    <link href="/Content/css/zui.min.css" rel="stylesheet" type="text/css" />
    <link href="/Content/css/style.css" rel="stylesheet" type="text/css" />
    <script src="/Content/js/jquery-3.2.1.min.js" id="main"></script>
    <script type="text/javascript" src="/Content/js/zui.min.js"></script>
    <script type="text/javascript" src="/Content/js/jquery.flexslider-min.js"></script>
</head>
<body>
    <div class="header">
        <div class="pull-left">
            <img class="lf" src="/Content/images/logo.png" />
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="mainnav">
    </div>
    <div class="">
        <div class="center-block tipsbox">

            <div class="tipscontbox">
                <div class="tipsiconbox">
                    <i class="icon icon-info-sign"></i>
                </div>

                <h3><%:ViewData["msg"]%></h3>

            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="ifooter">
        <div class="">
            <p><%:ViewData["copyright"]%></p>

            <div class="clear"></div>
        </div>
    </div>

</body>
</html>
