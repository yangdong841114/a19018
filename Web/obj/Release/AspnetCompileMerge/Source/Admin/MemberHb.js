
define(['text!MemberHb.html', 'jquery', 'j_easyui', 'datetimepicker'], function (MemberHb, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "回本差额查询");
        appView.html(MemberHb);



        $("#uLevel").empty();
        $("#uLevel").append("<option value=''>--全部--</option>");
        if (cacheList["ulevel"] && cacheList["ulevel"].length > 0) {
            for (var i = 0; i < cacheList["ulevel"].length; i++) {
                $("#uLevel").append("<option value='" + cacheList["ulevel"][i].id + "'>" + cacheList["ulevel"][i].name + "</option>");
            }
        }



        //初始化按钮
        $(".easyui-linkbutton").each(function (i, ipt) {
            $(ipt).linkbutton({});
        });









        //初始化表格
        var grid = utils.newGrid("dg", {
            singleSelect: false, //禁用单选
            rownumbers: false,
            frozenColumns: [[{ field: 'userId', title: '钱包地址', width: '200' },
            {
                field: '_operate', title: '操作', width: '150', align: 'center', formatter: function (val, row, index) {
                    if (row.SumHBETH > 0 && row.status == "共建中") {
                        var btnstr = "";

                        btnstr += '<a href="javascript:void(0);" dataId="' + row.id + '" class="XFETHgridFildEdit" >退差额ETH</a>&nbsp;&nbsp;';

                        btnstr += '<a href="javascript:void(0);" dataId="' + row.id + '" class="XFTCSgridFildEdit" >退差额OCC</a>&nbsp;&nbsp;';
                        return btnstr;
                    } else {
                        return '';
                    }
                }
            }
            ]],
            columns: [[
                { field: 'ck', title: '文本', checkbox: true, },
                { field: 'userName', title: '会员ID', width: '100', align: 'right' },
                { field: 'uLevel', title: '会员级别', width: '80' },
                { field: 'addTime', title: '注册日期', width: '130' },
                { field: 'status', title: '会员状态', width: '80' },

                { field: 'ethValueTz_Wcj', title: '总投资ETH', width: '80' },
                { field: 'SumCurrencyETHWcj', title: '总收益ETH', width: '80' },

                { field: 'SumHBETH', title: '回本ETH', width: '80' },
                { field: 'SumHBTCS', title: '回本OCC', width: '80' },


                { field: 'reuserName', title: '推荐人ID', width: '80' },
                { field: 'reName', title: '推荐人钱包', width: '200' }

            ]],

            url: "MemberPassed/GetListPageHb"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                    data.rows[i].uLevel = cacheMap["ulevel"][data.rows[i].uLevel];
                }
            }
            return data;
        }, function () {
            //行编辑按钮
            $(".gridFildEdit").each(function (i, dom) {
                dom.onclick = function () {
                    var memberId = $(dom).attr("memberId");
                    location.href = "#MemberInfo/" + memberId;
                    return false;
                }
            });

            //行确认按钮
            $(".XFETHgridFildEdit").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    utils.confirm("退差额ETH吗？", function () {
                        utils.AjaxPost("MemberPassed/TceETH", { id: dataId }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("操作成功");
                                queryGrid();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    })
                }
            });

            //行确认按钮
            $(".XFTCSgridFildEdit").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    utils.confirm("退差额OCC吗？", function () {
                        utils.AjaxPost("MemberPassed/TceTCS", { id: dataId }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("操作成功");
                                queryGrid();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    })
                }
            });


        })




        //删除错误样式
        $("#msgContent").on("focus", function () {
            $("#msgContent").removeClass("inputError");
        })

        //导出excel
        $("#ExportExcel").on("click", function () {
            location.href = "MemberPassed/ExportExcelMemberHb?userName=" + $("#userName").val() + "&userId=" + $("#userId").val()
        })


        //查询grid
        queryGrid = function () {
            var objs = $("#MemberPassingQueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {
            //销毁模态窗口

        };
    };

    return controller;
});