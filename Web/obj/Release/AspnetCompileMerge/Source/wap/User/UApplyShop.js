
define(['text!UApplyShop.html', 'jquery'], function (UApplyShop, $) {

    var controller = function (name) {
        
        $("#title").html("取消投資");
        var dto = undefined;

       

        //加载默认密码
        utils.AjaxPostNotLoadding("/User/UApplyShop/GetDefaultMsg", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(UApplyShop);
                dto = result.result;
                $("#ethValueTz").html(dto.ethValueTz);
                $("#SumCurrency").html(dto.SumCurrency);
                $("#SumHBETH").html(dto.SumHBETH);
             
                //隐藏提示框
                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();
                });

                //提交申请按钮
                $("#saveBtn").on('click', function () {
                    utils.showOrHiddenPromp();
                });

                //确认提交按钮
                $("#sureBtn").on('click', function () {
                    utils.AjaxPost("/User/UApplyShop/ApplyInvestRecordKt", {}, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showOrHiddenPromp();
                                utils.showSuccessMsg("申请成功，请等待管理员审核！");
                            }
                        });
                });


            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});