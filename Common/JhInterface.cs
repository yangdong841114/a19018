﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Xfrog.Net;

namespace Common
{
    /// <summary>
    /// 聚合数据接口
    /// </summary>
    public static class JhInterface
    {

        public static readonly string appkey = ConfigurationManager.AppSettings["JH_APPKEY"];
        public static readonly string ipappkey = ConfigurationManager.AppSettings["JH_IP_APPID"];
        public static readonly string openId = ConfigurationManager.AppSettings["JH_OPENID"];
        public static readonly string logKey = ConfigurationManager.AppSettings["JH_LOG_APPKEY"];

        public static readonly string SEND_KEY = ConfigurationManager.AppSettings["SEND_KEY"];
        public static readonly string SEND_URL = ConfigurationManager.AppSettings["SEND_URL"];

        /// <summary>
        /// 手机充值
        /// </summary>
        /// <param name="phone">电话</param>
        /// <param name="money">充值金额</param>
        /// <param name="orderId">订单号</param>
        /// <returns></returns>
        public static string Recharge(string phone, string money, string orderId)
        {

            string url7 = "http://op.juhe.cn/ofpay/mobile/onlineorder";

            var parameters7 = new Dictionary<string, string>();
            parameters7.Add("phoneno", phone); //手机号码
            parameters7.Add("cardnum", money); //充值金额,目前可选：5、10、20、30、50、100、300
            parameters7.Add("orderid", orderId); //商家订单号，8-32位字母数字组合
            parameters7.Add("key", appkey);//你申请的key
            string sign = openId + appkey + phone + money + orderId;
            sign = DESEncrypt.md5(sign);
            parameters7.Add("sign", sign); //校验值，md5(OpenID+key+phoneno+cardnum+orderid)，OpenID在个人中心查询

            string result7 = HttpSend.sendPost(url7, parameters7, "get");

            JsonObject newObj7 = new JsonObject(result7);
            String errorCode7 = newObj7["error_code"].Value;

            if (errorCode7 == "0")
            {
                return "success";
            }
            else
            {
                throw new ValidateException(newObj7["error_code"].Value + ":" + newObj7["reason"].Value);
            }
        }

        /// <summary>
        /// 获取物流商信息
        /// </summary>
        /// <returns></returns>
        public static List<Detail> GetLogisticList()
        {
            string url7 = "http://v.juhe.cn/exp/com";

            var parameters7 = new Dictionary<string, string>();
            parameters7.Add("key", logKey);//你申请的key

            string result7 = HttpSend.sendPost(url7, parameters7, "get");

            LogResult m = Newtonsoft.Json.JsonConvert.DeserializeObject<LogResult>(result7);

            if (m.error_code == "0")
            {
                return m.result;
            }
            else
            {
                throw new ValidateException(m.reason);
            }
        }

        /// <summary>
        /// 查询物流信息
        /// </summary>
        /// <param name="wlno">物流商编码</param>
        /// <param name="logNo">物流单号</param>
        /// <returns></returns>
        public static LogisticResult GetLogisticMsg(string wlno, string logNo)
        {
            string url7 = "http://v.juhe.cn/exp/index";

            var parameters7 = new Dictionary<string, string>();
            parameters7.Add("key", logKey);//你申请的key
            parameters7.Add("com", wlno);
            parameters7.Add("no", logNo);
            parameters7.Add("dtype", "json");

            string result7 = HttpSend.sendPost(url7, parameters7, "get");

            LogisticJson m = Newtonsoft.Json.JsonConvert.DeserializeObject<LogisticJson>(result7);

            if (m.error_code == "0")
            {
                return m.result;
            }
            else
            {
                throw new ValidateException(m.reason);
            }
        }

        /// <summary>
        /// 调用兑现网关
        /// </summary>
        /// <param name="orderID"></param>
        /// <param name="xmbh"></param>
        /// <param name="sign"></param>
        /// <param name="address"></param>
        /// <param name="value"></param>
        /// <param name="dxtype"></param>
        /// <param name="fee"></param>
        /// <returns></returns>
        public static string AddressSign(string code, string sign, string tpFlag)
        {

            string url7 = "https://poc.ce.cool/walletapi/verifySign/" + tpFlag;
            var parmJson = new Dictionary<string, string>();
            parmJson.Add("message", code);
            parmJson.Add("hex", sign);
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(parmJson);
            string s = DESEncrypt.md5(json + "SMkOFNZJnsgjufL0EGCpIy1Q");

            var parameters7 = new Dictionary<string, string>();
            parameters7.Add("data", json);
            parameters7.Add("sign", s);
            string result7 = HttpSend.sendPost(url7, parameters7, "post");
            SignDto dto = null;
            if (result7.IndexOf("0000") != -1)
            {
                dto = Newtonsoft.Json.JsonConvert.DeserializeObject<SignDto>(result7);
                if ("0000" != dto.code)
                {
                    throw new ValidateException(dto.result.msg);
                }
            }
            else
            {
                throw new ValidateException(result7);
            }

            return dto.result.address;
        }
        public static string pocGateway(System.Data.DataTable model, string occSignCode, string urls)
        {
            var data = Newtonsoft.Json.JsonConvert.SerializeObject(model);
            var sign = DESEncrypt.md5(data + occSignCode);
            var parameters = new Dictionary<string, string>();
            parameters.Add("data", data);
            parameters.Add("sign", sign);
            var result = HttpSend.sendPost(urls, parameters, "post");
            return result;
        }
        /// <summary>
        /// 调用兑现网关
        /// </summary>
        /// <param name="orderID"></param>
        /// <param name="xmbh"></param>
        /// <param name="sign"></param>
        /// <param name="address"></param>
        /// <param name="value"></param>
        /// <param name="dxtype"></param>
        /// <param name="fee"></param>
        /// <returns></returns>
        public static string pocGateway(string orderID, string xmbh, string sign, string address, string value, string dxtype, string fee)
        {

            string url7 = SEND_URL;
            string secretKey_A19018 = SEND_KEY;
            string veryfy_A19018 = DESEncrypt.md5("poc" + orderID.ToString() + secretKey_A19018);
            var parameters7 = new Dictionary<string, string>();
            parameters7.Add("orderID", orderID);
            parameters7.Add("xmbh", xmbh);
            parameters7.Add("address", address);
            parameters7.Add("value", value);
            parameters7.Add("fee", fee);
            parameters7.Add("dxtype", dxtype);
            parameters7.Add("sign", veryfy_A19018); //校验值
            string result7 = "";
            result7 = HttpSend.sendPost(url7, parameters7, "post");
            return result7;
        }

    }

    public partial class SignDto
    {
        public string code { get; set; }

        public SignMsg result { get; set; }
    }

    public partial class SignMsg
    {
        public string msg { get; set; }
        public string id { get; set; }
        public string address { get; set; }
    }

    public partial class Detail
    {
        public string no { get; set; }

        public string com { get; set; }
    }


    public partial class LogResult
    {
        public string error_code { get; set; }

        public string resultcode { get; set; }

        public string reason { get; set; }

        public List<Detail> result { get; set; }
    }
}
