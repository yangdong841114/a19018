﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class ValidateUtils
    {
        /// <summary>
        /// 检查字符串是否为空
        /// </summary>
        /// <param name="str">对象字符串</param>
        /// <returns>true:为空，false:非空</returns>
        public static bool CheckNull(string str)
        {
            if (str == null || str == "" || str.Trim().Length == 0) { return true; }
            return false;
        }

        /// <summary>
        /// 检查int?是否为null,0
        /// </summary>
        /// <param name="str">对象int?</param>
        /// <returns>true:0或null，false:非0</returns>
        public static bool CheckIntZero(int? i)
        {
            return i == null ? true : (i.Value == 0 ? true : false);
        }

        /// <summary>
        /// 检查double?是否为null,0
        /// </summary>
        /// <param name="str">对象double?</param>
        /// <returns>true:0或null，false:非0</returns>
        public static bool CheckDoubleZero(double? i)
        {
            return i == null ? true : (i.Value == 0 ? true : false);
        }
    }
}
