﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    /// <summary>
    /// 物流查询结果
    /// </summary>
    [Serializable]
    public class LogisticJson : DtoData
    {
        public string resultcode { get; set; }

        //原因
        public string reason { get; set; }

        //错误编码
        public string error_code { get; set; }

        public LogisticResult result { get; set; }
    }
}
